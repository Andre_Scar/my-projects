

export interface ICommon {
    _id?: string
}

export interface ICommonURL {
    id: string;
}

export interface IPaginationQuery {
    pageSize: number;
    page: number;
    filter: any;
}

export interface IPageResult<T> {
    count: number;
    page: number;
    pageSize: number;
    results: T;
}

export interface ILoggedUser {
    _id: string;
    name: string;
}

export interface IReason {
    message: string;
}