import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import ProfileForm from '../containers/ProfileForm/ProfileForm.container';


export const PROFILE_PATH = '/profile';


const ProfileRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={PROFILE_PATH} component={ProfileForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default ProfileRoutes;