import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IProfile, IProfileUpdate } from '../types/Profile.types';


export const AUTH_END_POINT: string = `profile/v1`;


export class ApiProfile {

    static Update = (profile: IProfileUpdate): Promise<AxiosResponse<IProfileUpdate>> => {
        return api.put(`${AUTH_END_POINT}`, profile);
    }

    static FindById = (): Promise<AxiosResponse<IProfile>> => {
        return api.get(`${AUTH_END_POINT}`);
    }

}