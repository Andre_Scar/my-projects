import React, { useEffect, useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ProfileForm.styles';
import { IProfileFormProps, IProfileFormSchema } from './ProfileForm.types';
import ProfileFormGeneral from './components/ProfileFormGeneral/ProfileFormGeneral';
import { Form, FormModes } from '../../../../components/Form';
import { getProfileFormSchema } from './ProfileForm.schema';
import { Button, Container } from 'react-bootstrap';
import FormHeaderLayout from '../../../../components/FormHeaderLayout/FormHeaderLayout';
import CardIndicative from './components/CardIndicative/CardIndicative';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { AxiosResponse } from 'axios';
import { IProfile, IProfileUpdate } from '../../types/Profile.types';
import { toast } from 'react-toastify';
import { ApiProfile } from '../../api/Profile.api';
import { useHistory } from 'react-router-dom';
import { PROJECTS_PATH } from '../../../projects/routes/Projects.routes';
import { AuthService } from '../../../../services/auth/auth.service';
import Loading from '../../../../components/Loading/Loading';
import { IReason } from '../../../../types/Common.types';

const ProfileForm: React.FC<IProfileFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const formId = 'ProfileFormGeneral';

    const [initialValues , setInitialValues] = useState<IProfileFormSchema>({
        name: '',
        email: '',
    });

    const [countMyProjects, setCountMyProjects] = useState<number>(0);
    const [countSharedWithMe, setCountSharedWithMe] = useState<number>(0);

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        loadProfile();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadProfile = () => {
        setLoading(true);

        const successCallback = (response: AxiosResponse<IProfile>) => {

            const { data } = response;

            const newInitialValues: IProfileFormSchema = {
                name: data.name,
                email: data.email
            };

            setCountMyProjects(data.countMyProjects || 0);
            setCountSharedWithMe(data.countSharedWithMe || 0);

            setInitialValues(newInitialValues);
        }

        const failureCallback = () => {
            toast.error('Erro ao carregar perfil do usuário.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProfile.FindById()
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onSubmit = (values: IProfileFormSchema) => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IProfileUpdate>) => {

            const { data } = response;
            
            AuthService.SetUser(data._id || '', data.name);
            
            toast.success('Sucesso ao atualizar perfil.');
            history.push(PROJECTS_PATH);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao atualizar perfil.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProfile.Update({
            name: values.name
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);

    }

    const onCancel = () => {
        history.push(PROJECTS_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={"Perfil"}>
                <Button variant="secondary" onClick={onCancel}>
                    Cancelar
                </Button>
                <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>
            </FormHeaderLayout>
            
            <Form 
                id={formId} 
                mode={FormModes.EDIT} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getProfileFormSchema}>

                <ProfileFormGeneral/>
            </Form>
        
            <Container fluid>
                <Row>
                    <Col xs={12} sm={12} md={4}>
                        <CardIndicative label={'Meus projetos'} value={countMyProjects}/>
                    </Col>
                    <Col xs={12} sm={12} md={4}>
                        <CardIndicative label={'Compartilhados comigo'} value={countSharedWithMe}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ProfileForm);