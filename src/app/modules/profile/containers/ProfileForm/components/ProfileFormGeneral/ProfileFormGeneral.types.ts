import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProfileFormGeneral.styles";

export interface IProfileFormGeneralProps extends WithStylesProps<typeof getStyles> { }
