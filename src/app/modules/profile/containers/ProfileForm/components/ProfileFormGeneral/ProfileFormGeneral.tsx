import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { getStyles } from './ProfileFormGeneral.styles';
import { IProfileFormGeneralProps } from './ProfileFormGeneral.types';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { FormField, FormFieldTypes } from '../../../../../../components/Form';

const ProfileFormGeneral: React.FC<IProfileFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8} className={classes.readonly}>
                        <FormField name="email" label="Email" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ProfileFormGeneral);