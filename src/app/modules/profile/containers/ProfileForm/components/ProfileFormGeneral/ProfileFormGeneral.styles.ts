export const getStyles = {
    root: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },

    readonly: {
        pointerEvents: 'none',
        cursor: 'not-allowed',
    }
};