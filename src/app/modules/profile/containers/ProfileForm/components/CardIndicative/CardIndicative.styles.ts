export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'column',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16);',
        padding: '16px',
        marginTop: '16px',
        marginBottom: '16px',
        color: '#6D6D6D'
    },

    label: {
        fontWeight: 'bold',
        fontSize: '24px',
        justifyContent: 'center',
        textAlign: 'center',
    },

    value: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: '62px',
        fontWeight: 'bold',
        textAlign: 'center',
    }
};