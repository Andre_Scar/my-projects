import { WithStylesProps } from "react-jss";
import { getStyles } from "./CardIndicative.styles";

export interface ICardIndicativeProps extends WithStylesProps<typeof getStyles> {
    label: string;
    value: number;
}