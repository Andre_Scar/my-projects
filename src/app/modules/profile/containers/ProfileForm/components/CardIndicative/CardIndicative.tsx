import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './CardIndicative.styles';
import { ICardIndicativeProps } from './CardIndicative.types';

const CardIndicative: React.FC<ICardIndicativeProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        label,
        value
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root}>
        <div className={classes.label}>
            {label}
        </div>
        <div className={classes.value}>
            {value}
        </div>
    </div>
}

export default withStyles(getStyles)(CardIndicative);