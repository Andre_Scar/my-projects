import * as Yup from 'yup';
import { IProfileFormSchema } from './ProfileForm.types';

export const getProfileFormSchema = () => {
    return Yup.object().shape<IProfileFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        email: Yup
            .string()
            .notRequired()
            .nullable()
    });
}