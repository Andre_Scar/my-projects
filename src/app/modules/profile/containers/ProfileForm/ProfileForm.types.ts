import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProfileForm.styles";

export interface IProfileFormProps extends WithStylesProps<typeof getStyles> { }


export interface IProfileFormSchema {
    name: string;
    email?: string | null;
}