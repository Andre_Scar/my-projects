import { ICommon } from "../../../types/Common.types";


export interface IProfile {
    name: string;
    email: string;
    countMyProjects?: number;
    countSharedWithMe?: number;
}

export interface IProfileUpdate extends ICommon {
    name: string;
}

