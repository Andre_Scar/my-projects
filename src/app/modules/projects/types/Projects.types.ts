import { ICommon } from "../../../types/Common.types";
import { Permission } from "../../members/types/Members.types";


export interface IProjectList {
    _id: string;
    name: string;
    description?: string;
    isFavorite: boolean;
    permission: Permission;
    createdByName: string;
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
}

export interface IProjectCreateOrUpdate extends ICommon {
    name: string;
    description?: string | null;
    isFavorite: boolean;
}

export interface IChangeIsFavoriteModel {
    projectId: string;
    isFavorite: boolean;
}

export interface IProjectsIngressResult {
    projectId: string;
    name: string;
    permission: Permission;
}

export interface IProjectsIngressURL {
    code: string;
}