import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IPageResult, IPaginationQuery } from '../../../types/Common.types';
import { IChangeIsFavoriteModel, IProjectCreateOrUpdate, IProjectList } from '../types/Projects.types';


export const AUTH_END_POINT: string = `project/v1`;


export class ApiProject {

    static Create = (project: IProjectCreateOrUpdate) => {
        return api.post(`${AUTH_END_POINT}`, project);
    }

    static Update = (id: string, project: IProjectCreateOrUpdate) => {
        return api.put(`${AUTH_END_POINT}/${id}`, project);
    }

    static FindById = (id: string): Promise<AxiosResponse<IProjectCreateOrUpdate>> => {
        return api.get(`${AUTH_END_POINT}/${id}`);
    }

    static List = (query: IPaginationQuery): Promise<AxiosResponse<IPageResult<IProjectList[]>>> => {
        return api.post(`${AUTH_END_POINT}/list`, query);
    }

    static ChangeIsFavorite = (changeIsFavoriteModel: IChangeIsFavoriteModel) => {
        return api.post(`${AUTH_END_POINT}/change-is-favorite`, changeIsFavoriteModel);
    }

    static Delete = (id: string) => {
        return api.delete(`${AUTH_END_POINT}/${id}`);
    }
}