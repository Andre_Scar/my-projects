import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import ProjectsList from '../containers/ProjectsList/ProjectsList.container';
import ProjectsForm from '../containers/ProjectsForm/ProjectsForm.container';
import ProjectsIngress from '../containers/ProjectsIngress/ProjectsIngress';

export const PROJECTS_PATH = '/projects';
export const NEW_PROJECT_PATH = '/projects/new';
export const EDIT_PROJECT_PATH = '/projects/edit';
export const VIEW_PROJECT_PATH = '/projects/view';
export const INGRESS_PROJECT_PATH = '/projects/ingress'

const ProjectsRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={NEW_PROJECT_PATH} component={ProjectsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_PROJECT_PATH}/:id`} component={ProjectsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_PROJECT_PATH}/:id`} component={ProjectsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={PROJECTS_PATH} component={ProjectsList} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${INGRESS_PROJECT_PATH}/:code`} component={ProjectsIngress} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default ProjectsRoutes;