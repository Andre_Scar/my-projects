import { WithStylesProps } from "react-jss";
import { getStyles } from "./Footer.styles";

export interface IFooterProps extends WithStylesProps<typeof getStyles> { 
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
}