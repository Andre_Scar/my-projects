export const getStyles = {

    root: { 
        width: '100%',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16);',
        padding: '16px',
        marginTop: '16px',
        marginBottom: '16px',
        position: 'relative',
    },

    body: {
        height: '100px',
        maxHeight: '100px',
        minHeight: '100px',
        overflow: 'auto',
        marginTop: '8px',
        marginBottom: '8px',
    },

    loadingContant: {
        position: 'absolute',
        width: 'calc(100% - 32px)',
        height: 'calc(100% - 32px)',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFFA6',
        zIndex: 1,
    }
};