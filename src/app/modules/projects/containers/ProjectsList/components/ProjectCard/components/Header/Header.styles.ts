export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'column',
    },

    activeFavorite: {
        color: '#F0AD4E',
    },

    title: {
        fontWeight: 'bold',
        fontSize: '18px',
        cursor: 'pointer',
    },

    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: '8px',
    },

    subHeader: {
        fontSize: '14px'
    },

    actions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',

        '& > div': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            width: '24px',
            marginLeft: '8px',
            cursor: 'pointer',
        }
    }
};