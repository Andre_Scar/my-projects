import React from 'react';
import withStyles from 'react-jss';
import Loading from '../../../../../../../../components/Loading/Loading';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import { getStyles } from './ProjectCard.styles';
import { IProjectCardProps } from './ProjectCard.types';

const ProjectCard: React.FC<IProjectCardProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        title,
        isFavorite,
        createdBy,
        description,
        countActivities,
        countDone,
        countDoing,
        countVerify,
        loading,
        onClick,
        onChangeIsFavorite,
        dropdownItems
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loadingContant}>
            <Loading/>
        </div>
    }
    //#endregion

    return (
        <div className={classes.root}>
            
            {renderLoading()}

            <Header 
                title={title} 
                isFavorite={isFavorite} 
                createdBy={createdBy} 
                dropdownItems={dropdownItems}
                onClick={onClick} 
                onChangeIsFavorite={onChangeIsFavorite}/>

            <div className={classes.body}>
                {description || '--'}
            </div>

            <Footer countActivities={countActivities} countDone={countDone} countDoing={countDoing} countVerify={countVerify}/>

        </div>
    );
}

export default withStyles(getStyles)(ProjectCard);