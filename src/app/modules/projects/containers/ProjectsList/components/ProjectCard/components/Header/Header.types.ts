import { WithStylesProps } from "react-jss";
import { IDropdownItem } from "../../../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types";
import { getStyles } from "./Header.styles";

export interface IHeaderProps extends WithStylesProps<typeof getStyles> { 
    title?: string;
    isFavorite?: boolean;
    createdBy: string;
    onClick: () => void;
    onChangeIsFavorite: (currentIsFavorite: boolean) => void;
    dropdownItems?: IDropdownItem[];
}