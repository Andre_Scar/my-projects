import React from 'react';
import withStyles from 'react-jss';
import ProgressBarActivitiesStatus from '../../../../../../../../components/ProgressBarActivitiesStatus/ProgressBarActivitiesStatus';
import { getStyles } from './Footer.styles';
import { IFooterProps } from './Footer.types';

const Footer: React.FC<IFooterProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        countActivities,
        countDone,
        countDoing,
        countVerify
    } = props;

    const percentageDoing = (countDoing / countActivities) * 100;
    const percentageDone = (countDone / countActivities) * 100;
    const percentageVerify = (countVerify / countActivities) * 100;
    const percentageToDo = ((countActivities - countVerify - countDone - countDoing) / countActivities) * 100;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root}>
        <ProgressBarActivitiesStatus 
            percentageDoing={percentageDoing} 
            percentageDone={percentageDone} 
            percentageVerify={percentageVerify} 
            percentageToDo={percentageToDo}/>
    </div>
}

export default withStyles(getStyles)(Footer);