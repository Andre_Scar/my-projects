import { WithStylesProps } from "react-jss";
import { IDropdownItem } from "../../../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types";
import { getStyles } from "./ProjectCard.styles";

export interface IProjectCardProps extends WithStylesProps<typeof getStyles> {
    id: string;
    title: string;
    isFavorite?: boolean;
    createdBy: string;
    description?: string | null;
    countActivities: number;
    countDone: number;
    countDoing: number;
    countVerify: number;
    loading?: boolean;
    onClick: () => void;
    onChangeIsFavorite: (currentIsFavorite: boolean) => void;
    dropdownItems: IDropdownItem[];
}