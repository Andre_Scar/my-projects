import React, { useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Header.styles';
import { IHeaderProps } from './Header.types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";
import { faStar as faStarRegular } from "@fortawesome/free-regular-svg-icons";
import { DropdownIcon } from '../../../../../../../../components/DropdownIcon';

const Header: React.FC<IHeaderProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        title,
        isFavorite,
        createdBy,
        onClick,
        onChangeIsFavorite,
        dropdownItems
    } = props;

    const [favorite, setFavorite] = useState<boolean>(isFavorite || false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const changeFavorite = () => {
        const currentIsFavorite = !favorite;

        setFavorite(currentIsFavorite);
        onChangeIsFavorite(currentIsFavorite);
    }
    //#endregion

    //#region RENDER METHODS
    const renderFavorite = () => {
        
        if(favorite) {
            return <FontAwesomeIcon onClick={changeFavorite} className={classes.activeFavorite} icon={faStarSolid} />
        }

        return <FontAwesomeIcon onClick={changeFavorite} icon={faStarRegular} />
    }

    const renderOptions = () => {
        return <DropdownIcon items={dropdownItems || []}/>;
    }
    //#endregion

    return <div className={classes.root}>
        <div className={classes.header}>
            <div className={classes.title} onClick={onClick}>
                {title}
            </div>
            <div className={classes.actions}>
                <div>
                    {renderFavorite()}
                </div>
                <div>
                    {renderOptions()}
                </div>
            </div>
        </div>
        <div className={classes.subHeader}>
            Criado por: {createdBy}
        </div>
    </div>
}

export default withStyles(getStyles)(Header);