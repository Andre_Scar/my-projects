import { AxiosResponse } from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import Loading from '../../../../../../components/Loading/Loading';
import { ApiMember } from '../../../../../members/api/Members.api';
import { INGRESS_PROJECT_PATH } from '../../../../routes/Projects.routes';
import { getStyles } from './ProjectShare.styles';
import { IProjectShareProps } from './ProjectShare.types';

const ProjectShare: React.FC<IProjectShareProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        projectId,
        show,
        handleClose
    } = props;

    const inputEl = useRef<HTMLInputElement>(null);

    const [link, setLink] = useState<string>('');

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(show) {
            loadURL();
        }
    }, [show]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadURL = () => {
        setLoading(true);

        const successCallback = (response: AxiosResponse<string>) => {
            setLink(`${process.env.REACT_APP_CLIENT_URL}${INGRESS_PROJECT_PATH}/${response.data}`);
        }

        const failureCallback = () => {
            toast.error('Erro ao gerar link de compartilhamento.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiMember.GenerateLink(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const copyURL = () => {
        if(inputEl === null || inputEl.current === null) {
            return toast.error('Erro ao copiar link.');
        }

        inputEl.current.select();
        document.execCommand('copy');

        toast.success('Sucesso ao copiar link.');
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}>
            <Loading hiddenLabel={true} width={65} height={65}/>
        </div>;
    }
    //#endregion

    return <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Compartilhar</Modal.Title>
        </Modal.Header>
        <Modal.Body className={classes.body}> 
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Link de compartilhamento</Form.Label>
                <Form.Control ref={inputEl} type="text" value={link} readOnly={true}/>
            </Form.Group>
            {renderLoading()}
        </Modal.Body>
        <Modal.Footer>
            <Button variant="primary" onClick={copyURL} disabled={loading}>
                Copiar Link
            </Button>
            <Button variant="secondary" onClick={handleClose}>
                Fechar
            </Button>
        </Modal.Footer>
    </Modal>
}

export default withStyles(getStyles)(ProjectShare);