export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    icon: {
        marginRight: '16px',
        height: '100%'
    },
    
    name: {
        fontWeight: '400',
        userSelect: 'none',
    },

    body: {
        display: 'flex',
        flexDirection: 'column',
    },

    loading: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: 'calc(100% - 32px)',
        height: 'calc(100% - 32px)',
        background: '#FFFFFFE6',
        zIndex: 1,
    }
};