import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectShare.styles";

export interface IProjectShareProps extends WithStylesProps<typeof getStyles> {
    projectId: string;
    show?: boolean;
    handleClose: () => void;
}