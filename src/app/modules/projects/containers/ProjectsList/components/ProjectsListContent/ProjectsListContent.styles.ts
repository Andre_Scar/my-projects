export const getStyles = {

    root: { 
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },

    tabContent: {
        display: 'flex !important',
        flexDirection: 'column',
        paddingLeft: '40px',
        paddingRight: '40px',
        marginBottom: '32px',
        position: 'relative',
    },

    tapOptions: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingLeft: '40px',
        paddingRight: '40px',
        marginBottom: '24px',
        marginTop: '24px',
    },

    tapOptionsLeft: {
        width: '30%',
    },

    tapOptionsRight: {
        width: '30%',
        display: 'flex',
        justifyContent: 'flex-end',
    },

    loading: {
        position: 'absolute',
        width: '100%',
        height: 'calc(100% - 129px)',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFFE6',
        zIndex: 1,
    },

    noDataFound: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        paddingLeft: '40px',
        paddingRight: '40px',
    },

    showMoreContent : {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: '16px',
        marginBottom: '32px',
    },

    '@media (max-width: 450px)': { 

        tapOptions: {
            flexWrap: 'wrap',
            flexDirection: 'column-reverse'
        },

        tapOptionsLeft: {
            width: '100%',
        },
    
        tapOptionsRight: {
            marginBottom: '32px',
            width: '100%',
        },

    }
};