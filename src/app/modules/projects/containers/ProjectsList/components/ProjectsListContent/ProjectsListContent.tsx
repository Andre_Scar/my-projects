import { AxiosResponse } from 'axios';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConfirmDelete from '../../../../../../components/ConfirmDelete/ConfirmDelete';
import { IConfirmDelete } from '../../../../../../components/ConfirmDelete/ConfirmDelete.types';
import { clearTimeDebounde, debounce } from '../../../../../../components/Debounce/Debounce';
import Link from '../../../../../../components/Link/Link';
import Loading from '../../../../../../components/Loading/Loading';
import NoDataFound from '../../../../../../components/NoDataFound/NoDataFound';
import Search from '../../../../../../components/Search/Search';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { IPageResult, IPaginationQuery, IReason } from '../../../../../../types/Common.types';
import { DASHBOARD_PATH } from '../../../../../dashboard/routes/Dashboard.routes';
import { Permission } from '../../../../../members/types/Members.types';
import { ApiProject } from '../../../../api/Projects.api';
import { EDIT_PROJECT_PATH, NEW_PROJECT_PATH, VIEW_PROJECT_PATH } from '../../../../routes/Projects.routes';
import { IProjectList } from '../../../../types/Projects.types';
import { ProjectCard } from '../ProjectCard';
import ProjectShare from '../ProjectShare/ProjectShare';
import { getStyles } from './ProjectsListContent.styles';
import { IProjectsListContentProps } from './ProjectsListContent.types';


const ProjectsListContent: React.FC<IProjectsListContentProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        filter,
        hiddenNewProjectButton,
        currentKey,
        tabKey
    } = props;

    let history = useHistory();

    const PAGE_SIZE = 10;
    const [page, setPage] = useState<number>(0);
    const [ countProjects, setCountProjects ] = useState<number>(0);

    const [projects, setProjects] = useState<IProjectList[] | null>(null);
    
    const [loading, setLoading] = useState<boolean>(false);
    const [loadingProjectCard, setLoadingProjectCard] = useState<string | null>(null);

    const [openProjecShare, setOpenProjecShare] = useState<boolean>(false);
    const [projecIdShare, setProjecIdShare] = useState<string>('');

    const [confirmDelete, setConfirmDelete] = useState<IConfirmDelete | null>(null);

    const searchElement = useRef<HTMLInputElement>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        init();
    }, [currentKey]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const init = () => {

        if(searchElement && searchElement.current) {
            searchElement.current.value = ''
        }
        
        setProjects([]);
        
        if(tabKey === currentKey) {
            setPage(0);
            listProjects(0);
        }
    }

    const listProjects = (currentPage: number) => {
        setLoading(true);
        
        const query: IPaginationQuery = {
            page: currentPage,
            pageSize: PAGE_SIZE,
            filter: filter
        }

        const successCallback = (response: AxiosResponse<IPageResult<IProjectList[]>>) => {
            setProjects(projects && currentPage !== 0 ? projects.concat(response.data.results) : response.data.results);
            setCountProjects(response.data.count);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao listar projeto.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProject.List(query)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const showMore = () => {
        const currentPage = page + 1;
        setPage(currentPage);

        listProjects(currentPage);
    }

    const openProject = (id: string, name: string, permission: Permission) => {

        ProjectService.SetCurrentProject({id, name, permission});

        history.push(DASHBOARD_PATH);
    }

    const newProject = () => {
        history.push(NEW_PROJECT_PATH);
    }

    const changeIsFavorite = (id: string, isFavorite: boolean) => {
        setLoadingProjectCard(id);

        const successCallback = () => {
            toast.success('Sucesso ao alterar favorito.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao alterar favorito.');
        }

        const finallyCallback = () => {
            setLoadingProjectCard(null);
        }

        ApiProject.ChangeIsFavorite({
            projectId: id,
            isFavorite: isFavorite
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const deleteProject = (id: string) => {
        setLoadingProjectCard(id);
        closeConfirmDelete();

        const successCallback = () => {
            setPage(0);
            listProjects(0);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar projeto.');
        }

        const finallyCallback = () => {
            setLoadingProjectCard(null);

            toast.success('Sucesso ao deletar projeto.');
        }

        ApiProject.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const openConfirmDelete = (id: string, name: string) => {
        setConfirmDelete({id, name});
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    const generateDropdownItems = (id: string, name: string, permission: Permission) => {
        if(permission === Permission.DEFAULT) {
            return [
                {
                    key: 'open',
                    label: 'Abrir',
                    onClick: () => {openProject(id, name, permission)}
                },
                {
                    key: 'view',
                    label: 'Visualizar',
                    onClick: () => {history.push(`${VIEW_PROJECT_PATH}/${id}`)}
                }
            ];
        }

        return [
            {
                key: 'share',
                label: 'Compartilhar',
                onClick: () => {
                    setProjecIdShare(id);
                    setOpenProjecShare(true);
                }
            },
            {
                key: 'open',
                label: 'Abrir',
                onClick: () => {openProject(id, name, permission)}
            },
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => {history.push(`${VIEW_PROJECT_PATH}/${id}`)}
            },
            {
                key: 'edit',
                label: 'Editar',
                onClick: () => {history.push(`${EDIT_PROJECT_PATH}/${id}`)}
            },
            {
                key: 'delete',
                label: 'Deletar',
                onClick: () => { openConfirmDelete(id, name) }
            }
        ]
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        delete filter.name;

        if(event.target.value !== '' && event.target.value) {
            filter.name = {'$regex' : event.target.value, '$options' : 'i'};
        }

        clearTimeDebounde();
        debounce(() => listProjects(0), 500)();
    }
    //#endregion

    //#region RENDER METHODS
    const renderNewProjectButton = () => {
        if(hiddenNewProjectButton === true) {
            return;
        }

        return <Button variant="primary" onClick={newProject}>Novo Projeto</Button>;
    }

    const renderTapOptions = () => {
        return (
            <div className={classes.tapOptions}>
                <div className={classes.tapOptionsLeft}>
                    <Search inputRef={searchElement} placeholder="Busca por nome" onChange={onChangeSearch} disabled={loading}/>
                </div>
                <div className={classes.tapOptionsRight}>
                    {renderNewProjectButton()}
                </div>
            </div>
        );
    }

    const renderMyProjects = () => {

        if(projects === null || projects.length === 0) {
            return;
        }

        return projects.map(project => {
            return <Col key={project._id}>
                <ProjectCard 
                    key={project._id} 
                    id={project._id}
                    title={project.name} 
                    isFavorite={project.isFavorite} 
                    onClick={() => openProject(project._id, project.name, project.permission)}
                    createdBy={project.createdByName}
                    description={project.description}
                    countActivities={project.countActivities}
                    countDone={project.countDone}
                    countDoing={project.countDoing}
                    countVerify={project.countVerify}
                    loading={loadingProjectCard === project._id}
                    onChangeIsFavorite={(currentIsFavorite: boolean) => changeIsFavorite(project._id, currentIsFavorite)}
                    dropdownItems={generateDropdownItems(project._id, project.name, project.permission)}/>
            </Col>
        });
        
    }

    const renderShowMore = () => {
        if(projects === null || projects.length === 0 || projects.length === countProjects) {
            return;
        }

        return <div className={classes.showMoreContent}>
            <Link onClick={showMore}>Mostrar mais</Link>
        </div>
    }

    const renderTabContent = () => {
        if(projects === null || projects.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound/></div>
        }

        return <div className={classes.tabContent}>
            <Row xs={1} md={3} lg={4}>
                {renderMyProjects()}
            </Row>
            {renderShowMore()}
        </div>
    }

    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}><Loading/></div>;
    }
    //#endregion

    return <div className={classes.root}>
        {renderLoading()}
        {renderTapOptions()}
        {renderTabContent()}
        <ProjectShare show={openProjecShare} projectId={projecIdShare} handleClose={() => setOpenProjecShare(false)}/>
        <ConfirmDelete 
            show={confirmDelete !== null}
            recordName={confirmDelete != null ? confirmDelete.name : ''} 
            recordId={confirmDelete!= null ? confirmDelete.id : ''} 
            handleClose={closeConfirmDelete} 
            handleConfirm={deleteProject}/>
    </div>
}

export default withStyles(getStyles)(ProjectsListContent);