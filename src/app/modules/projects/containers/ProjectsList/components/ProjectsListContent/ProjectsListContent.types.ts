import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectsListContent.styles";

export interface IProjectsListContentProps extends WithStylesProps<typeof getStyles> {
    filter: any;
    hiddenNewProjectButton?: boolean;
    currentKey: string;
    tabKey: string;
}