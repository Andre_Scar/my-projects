import React, { useEffect, useState } from 'react';
import withStyles from 'react-jss';
import { Tab, Tabs } from '../../../../components/Tabs';
import { AuthService } from '../../../../services/auth/auth.service';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import ProjectsListContent from './components/ProjectsListContent/ProjectsListContent';
import { getStyles } from './ProjectsList.styles';
import { IProjectsListProps } from './ProjectsList.types';

const ProjectsList: React.FC<IProjectsListProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    const [key, setKey] = useState('my');
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        init();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const init = () => {
        ProjectService.ClearCurrentProject();
    }

    const onSelect = (k: string | null) => {
        setKey(k || 'my');
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion
    
    return (
        <div className={classes.root}>
            <Tabs transition={false} activeKey={key} onSelect={onSelect}>

                <Tab eventKey="my" title="Meus projetos">
                    <ProjectsListContent filter={ 
                        {
                            createdBy: AuthService.GetUser()?._id
                        } 
                    }
                    currentKey={key}
                    tabKey={'my'}/>
                </Tab>

                <Tab eventKey="shared" title="Compartilhados comigo">
                    <ProjectsListContent filter={
                        {
                            createdBy: {
                                '$ne': AuthService.GetUser()?._id
                            }, 
                            users: { 
                                '$elemMatch': { 
                                    user: AuthService.GetUser()?._id 
                                } 
                            }
                        }
                    } 
                    hiddenNewProjectButton={true}
                    currentKey={key}
                    tabKey={'shared'}/>
                </Tab>

                <Tab eventKey="favorite" title="Favoritos">
                    <ProjectsListContent filter={
                        { 
                            users: { 
                                '$elemMatch': 
                                { 
                                    user: AuthService.GetUser()?._id, 
                                    isFavorite: true 
                                } 
                            } 
                        }
                    } 
                    hiddenNewProjectButton={true}
                    currentKey={key}
                    tabKey={'favorite'}/>
                </Tab>

            </Tabs>
        </div>
    );
}

export default withStyles(getStyles)(ProjectsList);