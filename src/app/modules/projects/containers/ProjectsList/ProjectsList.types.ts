import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectsList.styles";

export interface IProjectsListProps extends WithStylesProps<typeof getStyles> { }