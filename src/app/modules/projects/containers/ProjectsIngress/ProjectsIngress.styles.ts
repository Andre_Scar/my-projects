export const getStyles = {
    root: { 
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },

    message: {
        marginBottom: '32px',
        marginTop: '32px',
    },

    actions: {
        '& > button': {
            margin: '0px 16px 24px 16px',
        }
    },

    loading: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: 'calc(100% - 64px)',
        height: 'calc(100% - 56px)',
        background: '#FFFFFFE6',
        zIndex: 1,
    }
};