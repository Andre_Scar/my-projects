import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectsIngress.styles";

export interface IProjectsIngressProps extends WithStylesProps<typeof getStyles> { }