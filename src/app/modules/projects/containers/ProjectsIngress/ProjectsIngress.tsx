import React, { useEffect, useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ProjectsIngress.styles';
import { IProjectsIngressProps } from './ProjectsIngress.types';
import { Button } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { PROJECTS_PATH } from '../../routes/Projects.routes';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { DASHBOARD_PATH } from '../../../dashboard/routes/Dashboard.routes';
import { IProjectsIngressResult, IProjectsIngressURL } from '../../types/Projects.types';
import { ApiMember } from '../../../members/api/Members.api';
import Loading from '../../../../components/Loading/Loading';
import LottieIcon from '../../../../components/LottieIcon/LottieIcon';
import { LottieIconType } from '../../../../components/LottieIcon/LottieIcon.types';
import { IReason } from '../../../../types/Common.types';
import { AxiosResponse } from 'axios';


const ProjectsIngress: React.FC<IProjectsIngressProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();
    let { code } = useParams<IProjectsIngressURL>();

    const [message, setMessage] = useState<string>('Ingressando no projeto. Aguarde uns instantes...');
    const [result, setResult] = useState<IProjectsIngressResult | null>(null);

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        projectIngress();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const projectIngress = () => {
        setLoading(true);

        const successCallback = (response: AxiosResponse<IProjectsIngressResult>) => {
            setResult(response.data);

            setMessage(`Sucesso ao ingressar no projeto.`);
        }

        const failureCallback = (reason: IReason) => {
            setResult(null);
            setMessage(`${reason.message}.`);
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiMember.Ingress(code)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const goToListProjects = () => {
        history.push(PROJECTS_PATH);
    }

    const openProject = (result: IProjectsIngressResult | null) => {

        if(result === null) {
            return;
        }

        ProjectService.SetCurrentProject({id: result.projectId, name: result.name, permission: result.permission});

        history.push(DASHBOARD_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLottieIcon = () => {
        if(loading === true) {
            return <Loading hiddenLabel={true}/>
        }

        return <LottieIcon 
            type={result == null ? LottieIconType.ATTENTION : LottieIconType.OK} 
            height={150}
            width={150}>
        </LottieIcon>
    }
    //#endregion

    return <div className={classes.root}>
        <div>
            {renderLottieIcon()}
        </div>
        <div className={classes.message}>{message}</div>
        <div className={classes.actions}>
            <Button variant="secondary" onClick={goToListProjects}>Lista de projetos</Button>
            <Button variant="primary" 
                onClick={() => openProject(result)}
                disabled={result === null || loading}>
                    Abrir projeto
            </Button>
        </div>
    </div>
}

export default withStyles(getStyles)(ProjectsIngress);