import * as Yup from 'yup';
import { IProjectsFormSchema } from './ProjectsForm.types';

export const getProjectsFormSchema = () => {
    return Yup.object().shape<IProjectsFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        description: Yup
            .string()
            .notRequired()
            .nullable(),
        isFavorite: Yup
            .string()
            .required()
    });
}