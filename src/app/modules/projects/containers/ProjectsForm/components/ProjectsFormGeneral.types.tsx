import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectsFormGeneral.styles";

export interface IProjectsFormGeneralProps extends WithStylesProps<typeof getStyles> { }