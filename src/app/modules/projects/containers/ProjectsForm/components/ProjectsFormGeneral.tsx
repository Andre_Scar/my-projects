import React from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { FormField, FormFieldTypes, FormSelect, FormTextarea } from '../../../../../components/Form';
import { getStyles } from './ProjectsFormGeneral.styles';
import { IProjectsFormGeneralProps } from './ProjectsFormGeneral.types';

const ProjectsFormGeneral: React.FC<IProjectsFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormTextarea name="description" label="Descrição" rows={4}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="isFavorite" label="Favorito">
                            <option value={1}>Sim</option>
                            <option value={0}>Não</option>
                        </FormSelect>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ProjectsFormGeneral);