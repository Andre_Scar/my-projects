import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProjectsForm.styles";

export interface IProjectsFormProps extends WithStylesProps<typeof getStyles> { }

export interface IProjectsFormSchema {
    name: string;
    description?: string | null;
    isFavorite: string;
}
