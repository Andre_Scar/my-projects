import { AxiosResponse } from 'axios';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Form, FormModes } from '../../../../components/Form';
import { useFormMode } from '../../../../components/Form/hooks/FormMode';
import FormHeaderLayout from '../../../../components/FormHeaderLayout/FormHeaderLayout';
import Loading from '../../../../components/Loading/Loading';
import { ICommonURL, IReason } from '../../../../types/Common.types';
import { ApiProject } from '../../api/Projects.api';
import { PROJECTS_PATH } from '../../routes/Projects.routes';
import { IProjectCreateOrUpdate } from '../../types/Projects.types';
import ProjectsFormGeneral from './components/ProjectsFormGeneral';
import { getProjectsFormSchema } from './ProjectsForm.schema';
import { getStyles } from './ProjectsForm.styles';
import { IProjectsFormProps, IProjectsFormSchema } from './ProjectsForm.types';

const ProjectsForm: React.FC<IProjectsFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    let { id } = useParams<ICommonURL>();

    const { mode } = useFormMode();

    const formId = 'ProjectFormGeneral';

    const [initialValues , setInitialValues] = useState<IProjectsFormSchema>({
        name: '',
        description: undefined,
        isFavorite: '0'
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.EDIT || mode === FormModes.VIEW) {
            loadProject();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadProject = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IProjectCreateOrUpdate>) => {

            const { data } = response;

            const newInitialValues: IProjectsFormSchema = {
                name: data.name,
                description: data.description,
                isFavorite: data.isFavorite ? '1' : '0'
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar projeto para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProject.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const onSubmit = (values: IProjectsFormSchema, actions: FormikHelpers<any>) => { 

        if(mode === FormModes.NEW) {
            return save(values);
        }
        
        return update(values);
    }

    const onCancel = () => {
        history.push(PROJECTS_PATH);
    }

    const save = (values: IProjectsFormSchema) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar projeto.');
            history.push(PROJECTS_PATH);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao cadastrar projeto.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProject.Create({
            name: values.name,
            description: values.description,
            isFavorite: values.isFavorite === '1'
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: IProjectsFormSchema) => {
        setLoading(true);

        const project = {
            _id: id,
            name: values.name,
            description: values.description,
            isFavorite: values.isFavorite === '1'
        }

        const successCallback = () => {
            toast.success('Sucesso ao editar projeto.');
            history.push(PROJECTS_PATH);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar projeto.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiProject.Update(id, project)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Novo projeto';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar projeto';
        }

        return 'Visualizar projeto';
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getProjectsFormSchema()}>

                <ProjectsFormGeneral/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ProjectsForm);