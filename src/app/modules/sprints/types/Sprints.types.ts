import { ICommon } from "../../../types/Common.types";


export interface ISprintList {
    _id: string;
    name: string;
    createdByName: string;
    startAt: Date;
    endAt: Date;
}

export interface ISprintCreateOrUpdate extends ICommon {
    name: string;
    startAt: Date;
    endAt: Date;
    projectId: string;
}

export interface IExportCSV {
    fileName: string;
    fileData: string;
}