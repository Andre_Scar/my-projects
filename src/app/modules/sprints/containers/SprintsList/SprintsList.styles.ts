export const getStyles = {
    root: {
        padding: '32px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        position: 'relative',
    },

    options: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingLeft: '8px',
        paddingRight: '8px',
        marginBottom: '24px',  
    },

    optionsLeft: {
        width: '30%',
    },

    optionsRight: {
        width: '30%',
        display: 'flex',
        justifyContent: 'flex-end',
    },

    table: {
        // overflow: 'hidden',
    },

    tableOptions: {
        width: '24px',
        height: '24px',
        cursor: 'pointer',
    },

    showMoreContent : {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: '16px',
    },

    loading: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: 'calc(100% - 64px)',
        height: 'calc(100% - 56px)',
        background: '#FFFFFFE6',
        zIndex: 1,
    },

    noDataFound: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },

    scrollTable: {
    },

    '@media (max-width: 450px)': { 

        table: {
            minWidth: '650px',
        },

        scrollTable: {
            overflow: 'auto',
            height: '100%',
        },

        options: {
            flexWrap: 'wrap',
            flexDirection: 'column-reverse'
        },

        optionsLeft: {
            width: '100%',
        },
    
        optionsRight: {
            marginBottom: '32px',
            width: '100%',
        },

    }
};