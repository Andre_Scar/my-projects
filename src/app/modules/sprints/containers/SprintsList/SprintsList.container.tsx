import { AxiosResponse } from 'axios';
import dayjs from 'dayjs';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConfirmDelete from '../../../../components/ConfirmDelete/ConfirmDelete';
import { IConfirmDelete } from '../../../../components/ConfirmDelete/ConfirmDelete.types';
import { clearTimeDebounde, debounce } from '../../../../components/Debounce/Debounce';
import { DropdownIcon } from '../../../../components/DropdownIcon';
import { IDropdownItem } from '../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types';
import Link from '../../../../components/Link/Link';
import Loading from '../../../../components/Loading/Loading';
import NoDataFound from '../../../../components/NoDataFound/NoDataFound';
import Search from '../../../../components/Search/Search';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { DownloadService } from '../../../../services/download/download.service';
import { IPageResult, IPaginationQuery, IReason } from '../../../../types/Common.types';
import { Permission } from '../../../members/types/Members.types';
import { ApiSprint } from '../../api/Sprints.api';
import { EDIT_SPRINT_PATH, NEW_SPRINT_PATH, VIEW_SPRINT_PATH } from '../../routes/Sprints.routes';
import { IExportCSV, ISprintList } from '../../types/Sprints.types';
import { getStyles } from './SprintsList.styles';
import { ISprintsListProps } from './SprintsList.types';

const SprintsList: React.FC<ISprintsListProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const currentProject = ProjectService.GetCurrentProject();

    const PAGE_SIZE = 10;
    const [page, setPage] = useState<number>(0);
    const [ countSprints, setCountSprints ] = useState<number>(0);

    const [sprints, setSprints] = useState<ISprintList[] | null>(null);

    const [loading, setLoading] = useState<boolean>(false);

    const [filterName, setFilterName] = useState<any>();

    const [confirmDelete, setConfirmDelete] = useState<IConfirmDelete | null>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        listSprints(0);
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        listSprints(0);
    }, [filterName]); // eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const listSprints = (currentPage: number) => {
        setLoading(true);

        let filter: any = {
            project: currentProject ? currentProject.id : ''
        }

        if(filterName !== null) {
            filter.name = filterName;
        }
    
        const query: IPaginationQuery = {
            page: currentPage,
            pageSize: PAGE_SIZE,
            filter: filter
        }
    
        const successCallback = (response: AxiosResponse<IPageResult<ISprintList[]>>) => {
            setSprints(sprints && currentPage !== 0 ? sprints.concat(response.data.results) : response.data.results);
            setCountSprints(response.data.count);
        }
    
        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao listar sprints.');
        }
    
        const finallyCallback = () => {
            setLoading(false);
        }
    
        ApiSprint.List(query)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const newSprint = () => {
        history.push(NEW_SPRINT_PATH);
    }

    const exportCSV = (id: string) => {
        setLoading(true);

        const successCallback = (result: AxiosResponse<IExportCSV>) => {
            const { data } = result;
            
            DownloadService.DownloadCSV(data.fileData, data.fileName);

            toast.success('Sucesso ao exportar CSV.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao exportar CSV.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiSprint.ExportCSV(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const showMore = () => {
        const currentPage = page + 1;
        setPage(currentPage);

        listSprints(currentPage);
    }

    const deleteSprint = (id: string) => {
        closeConfirmDelete();
        setLoading(true);

        const successCallback = () => {
            if(sprints === null) {
                return toast.error('Erro ao deletar sprint.');
            }

            const currentSprints = sprints.filter(e => e._id !== id);
            
            setCountSprints(countSprints - 1);
            setSprints(currentSprints);

            toast.success('Sucesso ao deletar sprint.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar sprint.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiSprint.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const openConfirmDelete = (id: string, name: string) => {
        setConfirmDelete({id, name});
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    const generateDropdownItems = (id: string, name: string) => {
        let options: IDropdownItem[] = [
            {
                key: 'exportCSV',
                label: 'Baixar CSV',
                onClick: () => {exportCSV(id)}
            },
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => {history.push(`${VIEW_SPRINT_PATH}/${id}`)}
            }
        ];

        if(currentProject !== null && currentProject.permission === Permission.MANAGER) {
            options = options.concat([
                {
                    key: 'edit',
                    label: 'Editar',
                    onClick: () => {history.push(`${EDIT_SPRINT_PATH}/${id}`)}
                },
                {
                    key: 'delete',
                    label: 'Deletar',
                    onClick: () => { openConfirmDelete(id, name) }
                }
            ]);
        }

        return options;
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let name: any = null;

        if(event.target.value !== '' && event.target.value) {
            name = {'$regex' : event.target.value, '$options' : 'i'};
        }

        clearTimeDebounde();
        debounce(() => setFilterName(name), 500)();
    }
    //#endregion

    //#region RENDER METHODS
    const renderOptions = () => {
        return <div className={classes.options}>
            <div className={classes.optionsLeft}>
                <Search placeholder="Busca por nome" onChange={onChangeSearch} disabled={loading}/>
            </div>
            <div className={classes.optionsRight}>
                <Button variant="primary" 
                    onClick={newSprint} 
                    disabled={currentProject === null || currentProject.permission === Permission.DEFAULT}>
                    Nova Sprint
                </Button>
            </div>
        </div>;
    }

    const renderSprintsRow = () => {
        if(sprints === null || sprints.length === 0) {
            return;
        }

        return sprints.map((sprint, i) => {
            return <tr key={sprint._id}>
                <td>{i + 1}</td>
                <td>{sprint.name}</td>
                <td>{sprint.createdByName}</td>
                <td>{dayjs(sprint.startAt).format('DD/MM/YYYY')}</td>
                <td>{dayjs(sprint.endAt).format('DD/MM/YYYY')}</td>
                <td>
                    <div className={classes.tableOptions}>
                        <DropdownIcon items={generateDropdownItems(sprint._id, sprint.name)}/>
                    </div>
                </td>
            </tr>
        });
    }

    const renderSprintsTable = () => {
        if(sprints === null || sprints.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound/></div>;
        }

        return <div className={classes.scrollTable}>
            <Table className={classes.table}>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Criado por</th>
                        <th>Data de início</th>
                        <th>Data de fim</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {renderSprintsRow()}
                </tbody>
            </Table>
        </div>
    }

    const renderShowMore = () => {
        if(sprints === null || sprints.length === 0 || sprints.length === countSprints || loading === true) {
            return;
        }

        return <div className={classes.showMoreContent}>
            <Link onClick={showMore}>Mostrar mais</Link>
        </div>
    }

    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}><Loading/></div>;
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}
            {renderOptions()}
            {renderSprintsTable()}
            {renderShowMore()}
            <ConfirmDelete 
                show={confirmDelete !== null}
                recordName={confirmDelete != null ? confirmDelete.name : ''} 
                recordId={confirmDelete!= null ? confirmDelete.id : ''} 
                handleClose={closeConfirmDelete} 
                handleConfirm={deleteSprint}/>
        </div>
    );
}

export default withStyles(getStyles)(SprintsList);