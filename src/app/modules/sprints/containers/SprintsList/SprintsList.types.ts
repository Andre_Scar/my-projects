import { WithStylesProps } from "react-jss";
import { getStyles } from "./SprintsList.styles";

export interface ISprintsListProps extends WithStylesProps<typeof getStyles> { }
