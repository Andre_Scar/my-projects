import * as Yup from 'yup';
import { ISprintsFormSchema } from './SprintsForm.types';

export const getSprintsFormSchema = () => {
    return Yup.object().shape<ISprintsFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        startAt: Yup
            .string()
            .required('O campo data de início é obrigatorio'),
        endAt: Yup
            .string()
            .required('O campo data de fim é obrigatorio')
    });
}