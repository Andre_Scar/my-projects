import { WithStylesProps } from "react-jss";
import { getStyles } from "./SprintsFormGeneral.styles";

export interface ISprintsFormGeneralProps extends WithStylesProps<typeof getStyles> { }