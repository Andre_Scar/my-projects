import React from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { FormField, FormFieldTypes } from '../../../../../components/Form';
import { getStyles } from './SprintsFormGeneral.styles';
import { ISprintsFormGeneralProps } from './SprintsFormGeneral.types';

const SprintsFormGeneral: React.FC<ISprintsFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="startAt" label="Data de início" type={FormFieldTypes.DATE}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="endAt" label="Data de fim" type={FormFieldTypes.DATE}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(SprintsFormGeneral);