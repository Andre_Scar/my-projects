import { WithStylesProps } from "react-jss";
import { getStyles } from "./SprintsForm.styles";

export interface ISprintsFormProps extends WithStylesProps<typeof getStyles> { }


export interface ISprintsFormSchema {
    name: string;
    startAt: string;
    endAt: string;
}