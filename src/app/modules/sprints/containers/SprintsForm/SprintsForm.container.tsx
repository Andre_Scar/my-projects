import { AxiosResponse } from 'axios';
import dayjs from 'dayjs';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Form, FormModes } from '../../../../components/Form';
import { useFormMode } from '../../../../components/Form/hooks/FormMode';
import FormHeaderLayout from '../../../../components/FormHeaderLayout/FormHeaderLayout';
import Loading from '../../../../components/Loading/Loading';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { ICommonURL, IReason } from '../../../../types/Common.types';
import { ApiSprint } from '../../api/Sprints.api';
import { SPRINTS_PATH } from '../../routes/Sprints.routes';
import { ISprintCreateOrUpdate } from '../../types/Sprints.types';
import SprintsFormGeneral from './components/SprintsFormGeneral';
import { getSprintsFormSchema } from './SprintsForm.schema';
import { getStyles } from './SprintsForm.styles';
import { ISprintsFormProps, ISprintsFormSchema } from './SprintsForm.types';

const SprintsForm: React.FC<ISprintsFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();
    let { id } = useParams<ICommonURL>();

    const { mode } = useFormMode();
    const formId = 'ProjectFormGeneral';

    const [initialValues , setInitialValues] = useState<ISprintsFormSchema>({
        name: '',
        startAt: dayjs().format('YYYY-MM-DD'),
        endAt: dayjs().add(14, 'day').format('YYYY-MM-DD'),
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.EDIT || mode === FormModes.VIEW) {
            loadSprint();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadSprint = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<ISprintCreateOrUpdate>) => {

            const { data } = response;

            const newInitialValues: ISprintsFormSchema = {
                name: data.name,
                startAt: dayjs(data.startAt).format('YYYY-MM-DD'),
                endAt: dayjs(data.endAt).format('YYYY-MM-DD')
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar sprint para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiSprint.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onSubmit = (values: ISprintsFormSchema, actions: FormikHelpers<any>) => { 

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        if(projectId === '') {
            return toast.error('Não foi possível encontrar o projeto.');
        }

        if(mode === FormModes.NEW) {
            return save(values, projectId);
        }
        
        return update(values, projectId);
    }

    const onCancel = () => {
        history.push(SPRINTS_PATH);
    }

    const save = (values: ISprintsFormSchema, projectId: string) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar sprint.');
            history.push(SPRINTS_PATH);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao cadastrar sprint.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiSprint.Create({
            name: values.name,
            startAt: dayjs(values.startAt).toDate(),
            endAt: dayjs(values.endAt).toDate(),
            projectId: projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: ISprintsFormSchema, projectId: string) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao editar sprint.');
            history.push(SPRINTS_PATH);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar sprint.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiSprint.Update(id, {
            _id: id,
            name: values.name,
            startAt: dayjs(values.startAt).toDate(),
            endAt: dayjs(values.endAt).toDate(),
            projectId: projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Nova sprint';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar sprint';
        }

        return 'Visualizar sprint';
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getSprintsFormSchema()}>

                <SprintsFormGeneral/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(SprintsForm);