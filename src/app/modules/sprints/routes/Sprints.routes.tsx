import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import SprintsList from '../containers/SprintsList/SprintsList.container';
import SprintsForm from '../containers/SprintsForm/SprintsForm.container';


export const SPRINTS_PATH = '/sprints';
export const NEW_SPRINT_PATH = '/sprint/new';
export const EDIT_SPRINT_PATH = '/sprint/edit';
export const VIEW_SPRINT_PATH = '/sprint/view';


const SprintsRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={NEW_SPRINT_PATH} component={SprintsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_SPRINT_PATH}/:id`} component={SprintsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_SPRINT_PATH}/:id`} component={SprintsForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={SPRINTS_PATH} component={SprintsList} layoutType={LayoutType.NAVBAR_AND_MENU} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default SprintsRoutes;