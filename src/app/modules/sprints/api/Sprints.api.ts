import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IPageResult, IPaginationQuery } from '../../../types/Common.types';
import { IExportCSV, ISprintCreateOrUpdate, ISprintList } from '../types/Sprints.types';


export const AUTH_END_POINT: string = `sprint/v1`;


export class ApiSprint {

    static Create = (sprint: ISprintCreateOrUpdate) => {
        return api.post(`${AUTH_END_POINT}`, sprint);
    }

    static Update = (id: string, sprint: ISprintCreateOrUpdate) => {
        return api.put(`${AUTH_END_POINT}/${id}`, sprint);
    }

    static FindById = (id: string): Promise<AxiosResponse<ISprintCreateOrUpdate>> => {
        return api.get(`${AUTH_END_POINT}/${id}`);
    }

    static List = (query: IPaginationQuery): Promise<AxiosResponse<IPageResult<ISprintList[]>>> => {
        return api.post(`${AUTH_END_POINT}/list`, query);
    }

    static Delete = (id: string) => {
        return api.delete(`${AUTH_END_POINT}/${id}`);
    }

    static ExportCSV = (id: string): Promise<AxiosResponse<IExportCSV>> => {
        return api.get(`${AUTH_END_POINT}/export-csv/${id}`);
    }
}