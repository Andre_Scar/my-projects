

export interface IMemberList {
    _id: string;
    name: string;
    email: string;
    permission: Permission;
    isOwner: boolean;
}

export interface IMemberChangePermission {
    memberId: string;
    projectId: string;
    permission: Permission;
}

export enum Permission {
    MANAGER = 'manager',
    DEFAULT = 'default'
}