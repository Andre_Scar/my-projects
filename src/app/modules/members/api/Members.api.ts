import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IProjectsIngressResult } from '../../projects/types/Projects.types';
import { IMemberChangePermission, IMemberList } from '../types/Members.types';


export const MEMBER_END_POINT: string = `member/v1`;


export class ApiMember {

    static Update = (memberChangePermission: IMemberChangePermission): Promise<AxiosResponse<IMemberChangePermission>> => {
        return api.put(`${MEMBER_END_POINT}/${memberChangePermission.memberId}`, memberChangePermission);
    }

    static List = (filter: any): Promise<AxiosResponse<IMemberList[]>> => {
        return api.post(`${MEMBER_END_POINT}/list`, filter);
    }

    static GenerateLink = (projectId: string): Promise<AxiosResponse<string>> => {
        return api.get(`${MEMBER_END_POINT}/generate-link/${projectId}`);
    }

    static Ingress = (code: string): Promise<AxiosResponse<IProjectsIngressResult>> => {
        return api.get(`${MEMBER_END_POINT}/projects-ingress/${code}`);
    }

    static Delete = (id: string, projectId: string) => {
        return api.delete(`${MEMBER_END_POINT}/${id}/${projectId}`);
    }

}