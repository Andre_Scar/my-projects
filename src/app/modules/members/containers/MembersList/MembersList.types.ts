import { WithStylesProps } from "react-jss";
import { getStyles } from "./MembersList.styles";

export interface IMembersListProps extends WithStylesProps<typeof getStyles> { }