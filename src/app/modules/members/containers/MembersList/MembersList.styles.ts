export const getStyles = {
    root: {
        padding: '32px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        position: 'relative',
    },

    title: {
        fontSize: '24px',
        fontWeight: 'bold',
    },

    table: {
        // overflow: 'hidden',
    },

    tableOptions: {
        width: '24px',
        height: '24px',
        cursor: 'pointer',
    },

    headerOptions: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingLeft: '8px',
        paddingRight: '8px',
        marginBottom: '24px',  
    },

    headerOptionsLeft: {
        width: '30%',
    },

    disabled: {
        cursor: 'not-allowed !important'
    },

    tableInput: {
        paddingTop: '0 !important',
        paddingBottom: '0 !important',
    },

    tableInputContant: {
        height: '48.8px',
        display: 'flex',
        alignItems: 'center',
    },

    loading: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: 'calc(100% - 64px)',
        height: 'calc(100% - 56px)',
        background: '#FFFFFFE6',
        zIndex: 1,
    },

    noDataFound: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },

    scrollTable: {
    },

    '@media (max-width: 450px)': { 

        table: {
            minWidth: '650px',
        },

        scrollTable: {
            overflow: 'auto',
            height: '100%',
        },

        headerOptions: {
            flexWrap: 'wrap',
            flexDirection: 'column-reverse'
        },

        headerOptionsLeft: {
            width: '100%',
        }

    }
};