import { AxiosResponse } from 'axios';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Form, Table } from 'react-bootstrap';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import ConfirmDelete from '../../../../components/ConfirmDelete/ConfirmDelete';
import { IConfirmDelete } from '../../../../components/ConfirmDelete/ConfirmDelete.types';
import { clearTimeDebounde, debounce } from '../../../../components/Debounce/Debounce';
import { DropdownIcon } from '../../../../components/DropdownIcon';
import Loading from '../../../../components/Loading/Loading';
import NoDataFound from '../../../../components/NoDataFound/NoDataFound';
import Search from '../../../../components/Search/Search';
import { AuthService } from '../../../../services/auth/auth.service';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { IReason } from '../../../../types/Common.types';
import { ApiMember } from '../../api/Members.api';
import { IMemberChangePermission, IMemberList, Permission } from '../../types/Members.types';
import { getStyles } from './MembersList.styles';
import { IMembersListProps } from './MembersList.types';

const MembersList: React.FC<IMembersListProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    const currentProject = ProjectService.GetCurrentProject();

    const [members, setMembers] = useState<IMemberList[] | null>(null);

    const [loading, setLoading] = useState<boolean>(false);
    const [filterName, setFilterName] = useState<any>();

    const [confirmDelete, setConfirmDelete] = useState<IConfirmDelete | null>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        listMembers();
    }, [filterName]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const listMembers = () => {
        setLoading(true);

        if(currentProject === null){
            setMembers([]);
            setLoading(false);
            return;
        }

        let filter: any = {
            projectId: currentProject.id,
        };
        
        if(filterName !== null) {
            filter.name = filterName;
        }
    
        const successCallback = (response: AxiosResponse<IMemberList[]>) => {
            setMembers(response.data);
        }
    
        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao listar membros.');
        }
    
        const finallyCallback = () => {
            setLoading(false);
        }
    
        ApiMember.List(filter)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const changePermission = (memberId: string, projectId: string, permission: Permission, memberIndex: number) => {
        setLoading(true);

        const successCallback = (response: AxiosResponse<IMemberChangePermission>) => {
            const user = AuthService.GetUser();

            if( currentProject != null && user != null && response.data.memberId === user._id) {
                ProjectService.SetCurrentProject({
                    id: currentProject.id,
                    name: currentProject.name,
                    permission: response.data.permission
                });
            }

            toast.success('Sucesso ao atualizar a permissão.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao atualizar a permissão.');

            if(members === null) {
                return;
            }

            members[memberIndex].permission = members[memberIndex].permission === Permission.MANAGER ? Permission.DEFAULT : Permission.MANAGER;
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiMember.Update({
                memberId,
                projectId,
                permission
            })
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const generateDropdownItems = (id: string, name: string) => {
        return [
            {
                key: 'delete',
                label: 'Deletar',
                onClick: () => { openConfirmDelete(id, name) }
            }
        ]
    }

    const onChangePermission = (event: ChangeEvent<HTMLSelectElement>, memberIndex: number) => {
        if(members === null) {
            return;
        }

        const { value } = event.currentTarget;
        members[memberIndex].permission = Permission.MANAGER.toString() === value ? Permission.MANAGER : Permission.DEFAULT;

        setMembers([...members]);

        const projectId = currentProject?.id || '';

        changePermission(members[memberIndex]._id, projectId, members[memberIndex].permission, memberIndex);
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let name: any = null;

        if(event.target.value !== '' && event.target.value) {
            name = event.target.value;
        }

        clearTimeDebounde();
        debounce(() => setFilterName(name), 500)();
    }

    const deleteMember = (id: string) => {
        closeConfirmDelete();
        setLoading(true);

        const projectId = currentProject?.id || '';

        const successCallback = () => {
            if(members === null) {
                return toast.error('Erro ao deletar o membro.');
            }

            const currentMembers = members.filter(e => e._id !== id);
            
            setMembers(currentMembers);

            toast.success('Sucesso ao deletar o membro.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar o membro.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiMember.Delete(id, projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const openConfirmDelete = (id: string, name: string) => {
        setConfirmDelete({id, name});
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    //#endregion

    //#region RENDER METHODS
    const renderInputPermission = (isOwner: boolean, permission: Permission, memberIndex: number) => {
        const isNotManager: boolean = currentProject === null || currentProject.permission === Permission.DEFAULT;

        return <div className={classes.tableInputContant}>
            <Form.Control as="select" 
                className={`${isOwner || isNotManager ? classes.disabled : ''}`}
                onChange={(event: ChangeEvent<HTMLSelectElement>) => onChangePermission(event, memberIndex)} 
                value={permission}
                disabled={isOwner || isNotManager}>
                <option value={Permission.DEFAULT}>Padrão</option>
                <option value={Permission.MANAGER}>Administrador</option>
            </Form.Control>
        </div>
        
    }

    const renderMembersRow = () => {
        if(members === null || members.length === 0) {
            return;
        }

        return members.map((member, i) => {
            const isNotManager: boolean = currentProject === null || currentProject.permission === Permission.DEFAULT;

            return <tr key={member._id}>
                <td>{i + 1}</td>
                <td>{member.name}</td>
                <td>{member.email}</td>
                <td className={classes.tableInput}>
                    {renderInputPermission(member.isOwner, member.permission, i)}
                </td>
                <td>
                    <div className={`${classes.tableOptions} ${member.isOwner || isNotManager ? classes.disabled : ''}`}>
                        <DropdownIcon 
                            items={generateDropdownItems(member._id, member.name)} 
                            disabled={member.isOwner || isNotManager}/>
                    </div>
                </td>
            </tr>
        });
    }

    const renderMembersTable = () => {
        if(loading !== true && (members === null || members.length === 0)) {
            return <div className={classes.noDataFound}><NoDataFound/></div>
        }

        return <div className={classes.scrollTable}>
            <Table className={classes.table}>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Permissão</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {renderMembersRow()}
                </tbody>
            </Table>
        </div>
    }

    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}><Loading/></div>;
    }

    const renderHeader = () => {
        return <div className={classes.headerOptions}>
            <div className={classes.headerOptionsLeft}>
                <Search placeholder="Busca por nome" onChange={onChangeSearch} disabled={loading}/>
            </div>
        </div>;
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}
            {renderHeader()}
            {renderMembersTable()}

            <ConfirmDelete 
                show={confirmDelete !== null}
                recordName={confirmDelete != null ? confirmDelete.name : ''} 
                recordId={confirmDelete!= null ? confirmDelete.id : ''} 
                handleClose={closeConfirmDelete} 
                handleConfirm={deleteMember}/>
        </div>
    );
}

export default withStyles(getStyles)(MembersList);