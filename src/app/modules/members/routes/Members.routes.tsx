import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import MembersList from '../containers/MembersList/MembersList.container';


export const MEMBERS_PATH = '/members';

const MembersRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={MEMBERS_PATH} component={MembersList} layoutType={LayoutType.NAVBAR_AND_MENU} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default MembersRoutes;