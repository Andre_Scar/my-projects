export const getStyles = {
    root: {
        paddingTop: '32px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        position: 'relative',
    },

    tabs: {
        padding: 0,
    }
};