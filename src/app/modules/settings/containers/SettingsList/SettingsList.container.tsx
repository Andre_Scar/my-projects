import React, { useEffect, useState } from 'react';
import withStyles from 'react-jss';
import { useLocation } from 'react-router-dom';
import { Tab, Tabs } from '../../../../components/Tabs';
import ActiviteCustomField from './components/ActivityCustomField/ActivityCustomField';
import ActivityTypes from './components/ActivityTypes/ActivityTypes';
import { getStyles } from './SettingsList.styles';
import { ISettingsListProps } from './SettingsList.types';

const SettingsList: React.FC<ISettingsListProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let loaction = useLocation();

    const [activeKey, setActiveKey] = useState<string>('activity-types');
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        let queryParameters = new URLSearchParams(loaction.search);
        const currentActiveKey = queryParameters.get('activeKey');

        setActiveKey(currentActiveKey || 'activity-types');
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const onSelect = (k: string | null) => {
        setActiveKey(k || 'activity-types');
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>

            <Tabs className={classes.tabs} transition={false} activeKey={activeKey} onSelect={onSelect}>

                <Tab eventKey="activity-types" title="Tipos de atividade" >
                    <ActivityTypes/>
                </Tab>

                <Tab eventKey="activite-custom-field" title="Campos personalizados">
                    <ActiviteCustomField/>
                </Tab>

            </Tabs>
        </div>
    );
}

export default withStyles(getStyles)(SettingsList);