import { WithStylesProps } from "react-jss";
import { getStyles } from "./SettingsList.styles";

export interface ISettingsListProps extends WithStylesProps<typeof getStyles> { }