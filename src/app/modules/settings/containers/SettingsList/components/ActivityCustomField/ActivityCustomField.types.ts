import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityCustomField.styles";

export interface IActivityCustomFieldProps extends WithStylesProps<typeof getStyles> {
}