import { AxiosResponse } from 'axios';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConfirmDelete from '../../../../../../components/ConfirmDelete/ConfirmDelete';
import { IConfirmDelete } from '../../../../../../components/ConfirmDelete/ConfirmDelete.types';
import { clearTimeDebounde, debounce } from '../../../../../../components/Debounce/Debounce';
import { DropdownIcon } from '../../../../../../components/DropdownIcon';
import { IDropdownItem } from '../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types';
import Link from '../../../../../../components/Link/Link';
import Loading from '../../../../../../components/Loading/Loading';
import NoDataFound from '../../../../../../components/NoDataFound/NoDataFound';
import Search from '../../../../../../components/Search/Search';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { IPageResult, IPaginationQuery, IReason } from '../../../../../../types/Common.types';
import { Permission } from '../../../../../members/types/Members.types';
import { ApiActivityCustomField } from '../../../../api/ActivityCustomField.api';
import { EDIT_ACTIVITY_CUSTOM_FIELD_PATH, NEW_ACTIVITY_CUSTOM_FIELD_PATH, VIEW_ACTIVITY_CUSTOM_FIELD_PATH } from '../../../../routes/Settings.routes';
import { IActivityCustomFieldList } from '../../../../types/Settings.types';
import { getStyles } from './ActivityCustomField.styles';
import { IActivityCustomFieldProps } from './ActivityCustomField.types';

const ActivityCustomField: React.FC<IActivityCustomFieldProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const currentProject = ProjectService.GetCurrentProject();

    const PAGE_SIZE = 10;
    const [page, setPage] = useState<number>(0);
    const [countCustomFields, setCountCustomFields] = useState<number>(0);

    const [customFields, setCustomFields] = useState<IActivityCustomFieldList[]>([]);

    const [loading, setLoading] = useState<boolean>(false);
    const [filterName, setFilterName] = useState<any>();

    const [confirmDelete, setConfirmDelete] = useState<IConfirmDelete | null>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        listCustomFields(0);
    }, [filterName]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const listCustomFields = (currentPage: number) => {
        setLoading(true);

        let filter: any = {
            '$or': [
                { project: ProjectService.GetCurrentProject()?.id },
                { project: null },
            ]
        };

        if(filterName !== null) {
            filter.name = filterName;
        }
    
        const query: IPaginationQuery = {
            page: currentPage,
            pageSize: PAGE_SIZE,
            filter
        }
    
        const successCallback = (response: AxiosResponse<IPageResult<IActivityCustomFieldList[]>>) => {
            setCustomFields(customFields && currentPage !== 0 ? customFields.concat(response.data.results) : response.data.results);
            setCountCustomFields(response.data.count);
        }
    
        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar lista de campos personalizados');
        }
    
        const finallyCallback = () => {
            setLoading(false);
        }
    
        ApiActivityCustomField.List(query)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const showMore = () => {
        const currentPage = page + 1;
        setPage(currentPage);

        listCustomFields(currentPage);
    }

    const newCustomField = () => {
        history.push(NEW_ACTIVITY_CUSTOM_FIELD_PATH);
    }

    const generateDropdownItems = (id: string, name: string) => {
        let options: IDropdownItem[] = [
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => {history.push(`${VIEW_ACTIVITY_CUSTOM_FIELD_PATH}/${id}`)}
            }
        ];

        if(currentProject !== null && currentProject.permission === Permission.MANAGER) {
            options = options.concat([
                {
                    key: 'edit',
                    label: 'Editar',
                    onClick: () => {history.push(`${EDIT_ACTIVITY_CUSTOM_FIELD_PATH}/${id}`)}
                },
                {
                    key: 'delete',
                    label: 'Deletar',
                    onClick: () => { openConfirmDelete(id, name) }
                }
            ]);
        }

        return options;
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let name: any = null;

        if(event.target.value !== '' && event.target.value) {
            name = {'$regex' : event.target.value, '$options' : 'i'};
        }

        clearTimeDebounde();
        debounce(() => setFilterName(name), 500)();
    }

    const deleteCustomField = (id: string) => {
        closeConfirmDelete();
        setLoading(true);

        const successCallback = () => {
            if(customFields === null) {
                return toast.error('Erro ao deletar o campo personalizado.');
            }

            const currentActivityTypes = customFields.filter(e => e._id !== id);
            
            setCountCustomFields(countCustomFields - 1);
            setCustomFields(currentActivityTypes);

            toast.success('Sucesso ao deletar o campo personalizado.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar o campo personalizado.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityCustomField.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const openConfirmDelete = (id: string, name: string) => {
        setConfirmDelete({id, name});
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    //#endregion

    //#region RENDER METHODS
    const renderHeader = () => {
        return <div className={classes.options}>
            <div className={classes.optionsLeft}>
                <Search placeholder="Busca por nome" onChange={onChangeSearch} disabled={loading}/>
            </div>
            <div className={classes.optionsRight}>
                <Button variant="primary" 
                    onClick={newCustomField}
                    disabled={currentProject === null || currentProject.permission === Permission.DEFAULT}>
                    Novo campo
                </Button>
            </div>
        </div>;
    }

    const renderRows = () => {
        return customFields.map((customField, i) => {
            return <tr key={customField._id}>
                <td>{i + 1}</td>
                <td>{customField.name}</td>
                <td>{customField.type}</td>
                <td>{customField.activityTypeName}</td>
                <td>{customField.createdByName}</td>
                <td>
                    <div className={classes.tableOptions}>
                        <DropdownIcon items={generateDropdownItems(customField._id, customField.name)}/>
                    </div>
                </td>
            </tr>
        });
    }

    const renderTable = () => {

        if(customFields === null || customFields.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound/></div>;
        }

        return <div className={classes.tableContant}>
            <div className={classes.scrollTable}>
                <Table className={classes.table}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Campo</th>
                            <th>Tipo</th>
                            <th>Tipo de atividade</th>
                            <th>Criado por</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderRows()}
                    </tbody>
                </Table>
            </div>
        </div>;
    }

    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}><Loading/></div>;
    }

    const renderShowMore = () => {
        if(customFields === null || customFields.length === 0 || customFields.length === countCustomFields || loading === true) {
            return;
        }

        return <div className={classes.showMoreContent}>
            <Link onClick={showMore}>Mostrar mais</Link>
        </div>
    }

    //#endregion

    return <div className={classes.root}>
        {renderLoading()}
        {renderHeader()}
        {renderTable()}
        {renderShowMore()}
        <ConfirmDelete 
            show={confirmDelete !== null}
            recordName={confirmDelete != null ? confirmDelete.name : ''} 
            recordId={confirmDelete!= null ? confirmDelete.id : ''} 
            handleClose={closeConfirmDelete} 
            handleConfirm={deleteCustomField}/>

    </div>
}

export default withStyles(getStyles)(ActivityCustomField);