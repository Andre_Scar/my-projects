import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityTypes.styles";

export interface IActivityTypesProps extends WithStylesProps<typeof getStyles> {
}