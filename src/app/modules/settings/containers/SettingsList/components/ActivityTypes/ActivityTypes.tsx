import { AxiosResponse } from 'axios';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConfirmDelete from '../../../../../../components/ConfirmDelete/ConfirmDelete';
import { IConfirmDelete } from '../../../../../../components/ConfirmDelete/ConfirmDelete.types';
import { clearTimeDebounde, debounce } from '../../../../../../components/Debounce/Debounce';
import { DropdownIcon } from '../../../../../../components/DropdownIcon';
import { IDropdownItem } from '../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types';
import Link from '../../../../../../components/Link/Link';
import Loading from '../../../../../../components/Loading/Loading';
import NoDataFound from '../../../../../../components/NoDataFound/NoDataFound';
import Search from '../../../../../../components/Search/Search';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { IPageResult, IPaginationQuery, IReason } from '../../../../../../types/Common.types';
import { Permission } from '../../../../../members/types/Members.types';
import { ApiActivityType } from '../../../../api/ActivityTypes.api';
import { EDIT_ACTIVITY_TYPE_PATH, NEW_ACTIVITY_TYPE_PATH, VIEW_ACTIVITY_TYPE_PATH } from '../../../../routes/Settings.routes';
import { IActivityTypeList } from '../../../../types/Settings.types';
import { getStyles } from './ActivityTypes.styles';
import { IActivityTypesProps } from './ActivityTypes.types';


const ActivityTypes: React.FC<IActivityTypesProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const currentProject = ProjectService.GetCurrentProject();

    const PAGE_SIZE = 10;
    const [page, setPage] = useState<number>(0);
    const [countActivityTypes, setCountActivityTypes] = useState<number>(0);

    const [activityTypes, setActivityTypes] = useState<IActivityTypeList[]>([]);

    const [loading, setLoading] = useState<boolean>(false);
    const [filterName, setFilterName] = useState<any>();

    const [confirmDelete, setConfirmDelete] = useState<IConfirmDelete | null>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        listActivityTypes(0);
    }, [filterName]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const listActivityTypes = (currentPage: number) => {
        setLoading(true);
    
        let filter: any = {
            '$or': [
                { project: currentProject ? currentProject.id : '' },
                { project: null },
            ]
        };

        if(filterName !== null) {
            filter.name = filterName;
        }

        const query: IPaginationQuery = {
            page: currentPage,
            pageSize: PAGE_SIZE,
            filter
        }
    
        const successCallback = (response: AxiosResponse<IPageResult<IActivityTypeList[]>>) => {
            setActivityTypes(activityTypes && currentPage !== 0 ? activityTypes.concat(response.data.results) : response.data.results);
            setCountActivityTypes(response.data.count);
        }
    
        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar lista de tipos de atividade.');
        }
    
        const finallyCallback = () => {
            setLoading(false);
        }
    
        ApiActivityType.List(query)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const showMore = () => {
        const currentPage = page + 1;
        setPage(currentPage);

        listActivityTypes(currentPage);
    }

    const newActivityType = () => {
        history.push(NEW_ACTIVITY_TYPE_PATH);
    }

    const generateDropdownItems = (id: string, nome: string) => {
        let options: IDropdownItem[] = [ 
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => {history.push(`${VIEW_ACTIVITY_TYPE_PATH}/${id}`)}
            }
        ];

        if(currentProject !== null && currentProject.permission === Permission.MANAGER) {
            options = options.concat([
                {
                    key: 'edit',
                    label: 'Editar',
                    onClick: () => {history.push(`${EDIT_ACTIVITY_TYPE_PATH}/${id}`)}
                },
                {
                    key: 'delete',
                    label: 'Deletar',
                    onClick: () => { openConfirmDelete(id, nome) }
                }
            ]);
        }

        return options;
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let name: any = null;

        if(event.target.value !== '' && event.target.value) {
            name = {'$regex' : event.target.value, '$options' : 'i'};
        }

        clearTimeDebounde();
        debounce(() => setFilterName(name), 500)();
    }

    const deleteActivityType = (id: string) => {
        closeConfirmDelete();
        setLoading(true);

        const successCallback = () => {
            if(activityTypes === null) {
                return toast.error('Erro ao deletar o tipo de atividade.');
            }

            const currentActivityTypes = activityTypes.filter(e => e._id !== id);
            
            setCountActivityTypes(countActivityTypes - 1);
            setActivityTypes(currentActivityTypes);

            toast.success('Sucesso ao deletar o tipo de atividade.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(reason.message || 'Erro ao deletar o tipo de atividade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityType.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const openConfirmDelete = (id: string, name: string) => {
        setConfirmDelete({id, name});
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    //#endregion

    //#region RENDER METHODS
    const renderHeader = () => {
        return <div className={classes.options}>
            <div className={classes.optionsLeft}>
                <Search placeholder="Busca por nome" onChange={onChangeSearch} disabled={loading}/>
            </div>
            <div className={classes.optionsRight}>
                <Button variant="primary" 
                    onClick={newActivityType}
                    disabled={currentProject === null || currentProject.permission === Permission.DEFAULT}>
                        Novo tipo
                </Button>
            </div>
        </div>;
    }

    const renderRows = () => {
        return activityTypes.map((activityType, i) => {
            return <tr key={activityType._id}>
                <td>{i + 1}</td>
                <td>{activityType.name}</td>
                <td>{activityType.description || '-'}</td>
                <td>{activityType.createdByName}</td>
                <td>
                    <div className={classes.tableOptions}>
                        <DropdownIcon items={generateDropdownItems(activityType._id, activityType.name)}/>
                    </div>
                </td>
            </tr>
        });
    }

    const renderTable = () => {
        if(activityTypes === null || activityTypes.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound/></div>;
        }

        return <div className={classes.tableContant}>
            <div className={classes.scrollTable}>
                <Table className={classes.table}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Descrição</th>
                            <th>Criado por</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderRows()}
                    </tbody>
                </Table>
            </div>
        </div>;
    }

    const renderLoading = () => {
        if(loading !== true) {
            return;
        }

        return <div className={classes.loading}><Loading/></div>;
    }

    const renderShowMore = () => {
        if(activityTypes === null || activityTypes.length === 0 || activityTypes.length === countActivityTypes || loading === true) {
            return;
        }

        return <div className={classes.showMoreContent}>
            <Link onClick={showMore}>Mostrar mais</Link>
        </div>
    }
    //#endregion

    return <div className={classes.root}>
        {renderLoading()}
        {renderHeader()}
        {renderTable()}
        {renderShowMore()}
        <ConfirmDelete 
                show={confirmDelete !== null}
                recordName={confirmDelete != null ? confirmDelete.name : ''} 
                recordId={confirmDelete!= null ? confirmDelete.id : ''} 
                handleClose={closeConfirmDelete} 
                handleConfirm={deleteActivityType}/>

    </div>
}

export default withStyles(getStyles)(ActivityTypes);