export const getStyles = {
    root: { 
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },

    title: {
        fontSize: '24px',
        fontWeight: 'bold',
    },

    contentNewType: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },

    tableContant: {
        padding: '16px 40px 24px 40px',
    },

    table: {
        marginBottom: 0
    },

    tableOptions: {
        width: '24px',
        height: '24px',
        cursor: 'pointer',
    },

    options: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingLeft: '40px',
        paddingRight: '40px',
        marginTop: '24px',
        marginBottom: '24px',  
    },

    optionsLeft: {
        width: '30%',
    },

    optionsRight: {
        width: '30%',
        display: 'flex',
        justifyContent: 'flex-end',
    },

    showMoreContent : {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: '24px'
    },

    loading: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFFE6',
        zIndex: 1,
    },

    noDataFound: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        marginLeft: '40px',
        marginRight: '40px',
        marginBottom: '40px',
    },

    scrollTable: {
    },

    '@media (max-width: 450px)': { 

        table: {
            minWidth: '650px',
        },

        tableContant: {
            height: '100%',
        },

        scrollTable: {
            overflow: 'auto',
            height: '100%',
        },

        options: {
            flexWrap: 'wrap',
            flexDirection: 'column-reverse'
        },

        optionsLeft: {
            width: '100%',
        },
    
        optionsRight: {
            marginBottom: '32px',
            width: '100%',
        },

    }
};