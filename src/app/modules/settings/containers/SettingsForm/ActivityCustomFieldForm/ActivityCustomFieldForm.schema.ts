import * as Yup from 'yup';
import { CustomFieldType, IActivityCustomFieldFormSchema } from './ActivityCustomFieldForm.types';

export const getActivityCustomFieldFormSchema = () => {
    return Yup.object().shape<IActivityCustomFieldFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        type: Yup
            .mixed<CustomFieldType>()
            .oneOf(Object.values(CustomFieldType))
            .required('O campo type é obrigatorio'),
        activityTypeId: Yup
            .string()
            .required('O campo tipos de atividade é obrigatorio')
    });
}