import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityCustomFieldFormGeneral.styles";

export interface IActivityCustomFieldFormGeneralProps extends WithStylesProps<typeof getStyles> {
    activityType: string;
}