import { AxiosResponse } from 'axios';
import { useFormikContext } from 'formik';
import React, { useEffect, useState } from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import { FormField, FormFieldTypes, FormModes, FormSelect } from '../../../../../../components/Form';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { ApiActivityType } from '../../../../api/ActivityTypes.api';
import { IActivityTypeList } from '../../../../types/Settings.types';
import { CustomFieldType } from '../ActivityCustomFieldForm.types';
import { getStyles } from './ActivityCustomFieldFormGeneral.styles';
import { IActivityCustomFieldFormGeneralProps } from './ActivityCustomFieldFormGeneral.types';
import { useFormMode } from '../../../../../../components/Form/hooks/FormMode';
import { IReason } from '../../../../../../types/Common.types';

const ActivityCustomFieldFormGeneral: React.FC<IActivityCustomFieldFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        activityType
    } = props;

    const { setFieldValue } = useFormikContext();
    const { mode } = useFormMode();

    const [activityTypes, setActivityTypes] = useState<IActivityTypeList[]>([]);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        loadActivityTypes();
    }, [activityType]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadActivityTypes = () => {

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        const successCallback = (response: AxiosResponse<IActivityTypeList[]>) => {
            const { data } = response;

            setActivityTypes(data);

            const activityTypeId = mode === FormModes.NEW ? data[0]._id : activityType;

            setFieldValue('activityTypeId', activityTypeId);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar tipos de atividade.');
        }

        const finallyCallback = () => {
        }

        ApiActivityType.ListAll(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="type" label="Tipo" disabled={mode === FormModes.EDIT}>
                            <option value={CustomFieldType.TEXT}>Texto</option>
                            <option value={CustomFieldType.NUMBER}>Número</option>
                            <option value={CustomFieldType.DATE}>Data</option>
                        </FormSelect>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="activityTypeId" label="Tipo de atividade" disabled={mode === FormModes.EDIT}>
                            {activityTypes.map(e => {
                                return <option value={e._id} key={e._id}>{e.name}</option>
                            })}
                        </FormSelect>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ActivityCustomFieldFormGeneral);