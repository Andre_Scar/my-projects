import { AxiosResponse } from 'axios';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import ActivityCustomFieldFormGeneral from './components/ActivityCustomFieldFormGeneral';
import { getActivityCustomFieldFormSchema } from './ActivityCustomFieldForm.schema';
import { getStyles } from './ActivityCustomFieldForm.styles';
import { CustomFieldType, IActivityCustomFieldFormProps, IActivityCustomFieldFormSchema } from './ActivityCustomFieldForm.types';
import { ICommonURL, IReason } from '../../../../../types/Common.types';
import { useFormMode } from '../../../../../components/Form/hooks/FormMode';
import { Form, FormModes } from '../../../../../components/Form';
import FormHeaderLayout from '../../../../../components/FormHeaderLayout/FormHeaderLayout';
import { SETTINGS_PATH } from '../../../routes/Settings.routes';
import { IActivityCustomFieldCreateOrUpdate } from '../../../types/Settings.types';
import { ApiActivityCustomField } from '../../../api/ActivityCustomField.api';
import { ProjectService } from '../../../../../services/currentProject/currentProject.service';
import Loading from '../../../../../components/Loading/Loading';

const ActivityCustomFieldForm: React.FC<IActivityCustomFieldFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    let { id } = useParams<ICommonURL>();

    const { mode } = useFormMode();

    const formId = 'ActivityCustomFieldFormGeneral';

    const [initialValues , setInitialValues] = useState<IActivityCustomFieldFormSchema>({
        name: '',
        type: CustomFieldType.TEXT,
        activityTypeId: ''
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode !== FormModes.NEW) {
            loadActivityCustomField();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadActivityCustomField = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IActivityCustomFieldCreateOrUpdate>) => {

            const { data } = response;

            const newInitialValues: IActivityCustomFieldFormSchema = {
                name: data.name,
                type: data.type,
                activityTypeId: data.activityTypeId
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar campo personalizado para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityCustomField.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const onSubmit = (values: IActivityCustomFieldFormSchema, actions: FormikHelpers<any>) => { 

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        if(mode === FormModes.NEW) {
            return save(values, projectId);
        }
        
        return update(values, projectId);
    }

    const onCancel = () => {
        history.push(SETTINGS_PATH);
    }

    const save = (values: IActivityCustomFieldFormSchema, projectId: string) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar campo personalizado.');
            history.push(`${SETTINGS_PATH}?activeKey=activite-custom-field`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao cadastrar campo personalizado.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityCustomField.Create({
            name: values.name,
            type: values.type,
            activityTypeId: values.activityTypeId,
            projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: IActivityCustomFieldFormSchema, projectId: string) => {
        setLoading(true);

        const activityCustomField = {
            _id: id,
            name: values.name,
            type: values.type,
            activityTypeId: values.activityTypeId,
            projectId
        }

        const successCallback = () => {
            toast.success('Sucesso ao editar campo personalizado.');
            history.push(`${SETTINGS_PATH}?activeKey=activite-custom-field`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar campo personalizado.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityCustomField.Update(id, activityCustomField)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Novo campo personalizado';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar campo personalizado';
        }

        return 'Visualizar campo personalizado';
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getActivityCustomFieldFormSchema()}>

                <ActivityCustomFieldFormGeneral activityType={initialValues.activityTypeId}/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ActivityCustomFieldForm);