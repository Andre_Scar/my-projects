import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityCustomFieldForm.styles";

export interface IActivityCustomFieldFormProps extends WithStylesProps<typeof getStyles> { }

export interface IActivityCustomFieldFormSchema {
    name: string;
    type: CustomFieldType;
    activityTypeId: string;
}

export enum CustomFieldType {
    TEXT = 'text',
    NUMBER = 'number',
    DATE = 'date'
}