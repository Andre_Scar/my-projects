import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityTypesFormGeneral.styles";

export interface IActivityTypesFormGeneralProps extends WithStylesProps<typeof getStyles> { }