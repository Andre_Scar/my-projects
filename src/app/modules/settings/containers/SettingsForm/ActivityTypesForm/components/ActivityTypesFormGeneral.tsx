import React from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { FormField, FormFieldTypes, FormTextarea } from '../../../../../../components/Form';
import { getStyles } from './ActivityTypesFormGeneral.styles';
import { IActivityTypesFormGeneralProps } from './ActivityTypesFormGeneral.types';

const ActivityTypesFormGeneral: React.FC<IActivityTypesFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormTextarea name="description" label="Descrição" rows={4}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ActivityTypesFormGeneral);