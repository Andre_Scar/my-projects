import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityTypesForm.styles";

export interface IActivityTypesFormProps extends WithStylesProps<typeof getStyles> { }

export interface IActivityTypesFormSchema {
    name: string;
    description?: string | null;
}
