import { AxiosResponse } from 'axios';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import ActivityTypesFormGeneral from './components/ActivityTypesFormGeneral';
import { getActivityTypesFormSchema } from './ActivityTypesForm.schema';
import { getStyles } from './ActivityTypesForm.styles';
import { IActivityTypesFormProps, IActivityTypesFormSchema } from './ActivityTypesForm.types';
import { ICommonURL, IReason } from '../../../../../types/Common.types';
import { useFormMode } from '../../../../../components/Form/hooks/FormMode';
import { Form, FormModes } from '../../../../../components/Form';
import FormHeaderLayout from '../../../../../components/FormHeaderLayout/FormHeaderLayout';
import { SETTINGS_PATH } from '../../../routes/Settings.routes';
import { ApiActivityType } from '../../../api/ActivityTypes.api';
import { IActivityTypeCreateOrUpdate } from '../../../types/Settings.types';
import { ProjectService } from '../../../../../services/currentProject/currentProject.service';
import Loading from '../../../../../components/Loading/Loading';

const ActivityTypesForm: React.FC<IActivityTypesFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    let { id } = useParams<ICommonURL>();

    const { mode } = useFormMode();

    const formId = 'ActivityTypeFormGeneral';

    const [initialValues , setInitialValues] = useState<IActivityTypesFormSchema>({
        name: '',
        description: null,
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.EDIT || mode === FormModes.VIEW) {
            loadActivityType();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadActivityType = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IActivityTypeCreateOrUpdate>) => {

            const { data } = response;

            const newInitialValues: IActivityTypesFormSchema = {
                name: data.name,
                description: data.description
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar tipo de atividade para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityType.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const onSubmit = (values: IActivityTypesFormSchema, actions: FormikHelpers<any>) => { 

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        if(mode === FormModes.NEW) {
            return save(values, projectId);
        }
        
        return update(values, projectId);
    }

    const onCancel = () => {
        history.push(SETTINGS_PATH);
    }

    const save = (values: IActivityTypesFormSchema, projectId: string) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar tipo de atividade.');
            history.push(`${SETTINGS_PATH}?activeKey=activity-types`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao cadastrar tipo de atividade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityType.Create({
            name: values.name,
            description: values.description,
            projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: IActivityTypesFormSchema, projectId: string) => {

        setLoading(true);

        const activityType = {
            _id: id,
            name: values.name,
            description: values.description,
            projectId
        }

        const successCallback = () => {
            toast.success('Sucesso ao editar tipo de atividade.');
            history.push(`${SETTINGS_PATH}?activeKey=activity-types`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar tipo de atividade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivityType.Update(id, activityType)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Novo tipo de atividade';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar tipo de atividade';
        }

        return 'Visualizar tipo de atividade';
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getActivityTypesFormSchema()}>

                <ActivityTypesFormGeneral/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ActivityTypesForm);