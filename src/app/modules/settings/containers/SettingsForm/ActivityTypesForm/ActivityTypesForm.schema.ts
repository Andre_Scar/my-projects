import * as Yup from 'yup';
import { IActivityTypesFormSchema } from './ActivityTypesForm.types';

export const getActivityTypesFormSchema = () => {
    return Yup.object().shape<IActivityTypesFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        description: Yup
            .string()
            .notRequired()
            .nullable()
    });
}