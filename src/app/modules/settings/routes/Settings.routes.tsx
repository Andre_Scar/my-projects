import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import SettingsList from '../containers/SettingsList/SettingsList.container';
import ActivityTypesForm from '../containers/SettingsForm/ActivityTypesForm/ActivityTypesForm.container';
import ActivityCustomFieldForm from '../containers/SettingsForm/ActivityCustomFieldForm/ActivityCustomFieldForm.container';


export const SETTINGS_PATH = '/settings';

export const NEW_ACTIVITY_TYPE_PATH = '/activity-type';
export const EDIT_ACTIVITY_TYPE_PATH = '/activity-type/edit';
export const VIEW_ACTIVITY_TYPE_PATH = '/activity-type/view';

export const NEW_ACTIVITY_CUSTOM_FIELD_PATH = '/activity-custom-field';
export const EDIT_ACTIVITY_CUSTOM_FIELD_PATH = '/activity-custom-field/edit';
export const VIEW_ACTIVITY_CUSTOM_FIELD_PATH = '/activity-custom-field/view';

const SettingsRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={SETTINGS_PATH} component={SettingsList} layoutType={LayoutType.NAVBAR_AND_MENU} isPrivateRoute={true}/>

                <Route exact={true} path={NEW_ACTIVITY_TYPE_PATH} component={ActivityTypesForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_ACTIVITY_TYPE_PATH}/:id`} component={ActivityTypesForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_ACTIVITY_TYPE_PATH}/:id`} component={ActivityTypesForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>

                <Route exact={true} path={NEW_ACTIVITY_CUSTOM_FIELD_PATH} component={ActivityCustomFieldForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_ACTIVITY_CUSTOM_FIELD_PATH}/:id`} component={ActivityCustomFieldForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_ACTIVITY_CUSTOM_FIELD_PATH}/:id`} component={ActivityCustomFieldForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default SettingsRoutes;