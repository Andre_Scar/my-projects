import { ICommon } from "../../../types/Common.types";
import { CustomFieldType } from "../containers/SettingsForm/ActivityCustomFieldForm/ActivityCustomFieldForm.types";


export interface IActivityTypeList {
    _id: string;
    name: string;
    description?: string;
    createdByName: string;
    projectId?: string;
}

export interface IActivityTypeCreateOrUpdate extends ICommon {
    name: string;
    description?: string | null;
    projectId: string;
}

export interface IActivityCustomFieldList {
    _id: string;
    name: string;
    type: CustomFieldType;
    activityTypeName: string;
    createdByName: string;
}

export interface IActivityCustomFieldCreateOrUpdate extends ICommon {
    name: string;
    type: CustomFieldType;
    activityTypeId: string;
    projectId: string;
}