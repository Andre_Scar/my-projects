import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IPageResult, IPaginationQuery } from '../../../types/Common.types';
import { IActivityCustomFieldCreateOrUpdate, IActivityCustomFieldList } from '../types/Settings.types';


export const AUTH_END_POINT: string = `activity-custom-field/v1`;


export class ApiActivityCustomField {

    static Create = (project: IActivityCustomFieldCreateOrUpdate) => {
        return api.post(`${AUTH_END_POINT}`, project);
    }

    static Update = (id: string, project: IActivityCustomFieldCreateOrUpdate) => {
        return api.put(`${AUTH_END_POINT}/${id}`, project);
    }

    static FindById = (id: string): Promise<AxiosResponse<IActivityCustomFieldCreateOrUpdate>> => {
        return api.get(`${AUTH_END_POINT}/${id}`);
    }

    static List = (query: IPaginationQuery): Promise<AxiosResponse<IPageResult<IActivityCustomFieldList[]>>> => {
        return api.post(`${AUTH_END_POINT}/list`, query);
    }

    static ListAll = (activityTypeId: string): Promise<AxiosResponse<IActivityCustomFieldList[]>> => {
        return api.post(`${AUTH_END_POINT}/list-all`, { activityTypeId });
    }

    static Delete = (id: string) => {
        return api.delete(`${AUTH_END_POINT}/${id}`);
    }
}