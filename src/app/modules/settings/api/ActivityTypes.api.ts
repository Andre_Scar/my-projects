import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IPageResult, IPaginationQuery } from '../../../types/Common.types';
import { IActivityTypeCreateOrUpdate, IActivityTypeList } from '../types/Settings.types';


export const AUTH_END_POINT: string = `activity-type/v1`;


export class ApiActivityType {

    static Create = (project: IActivityTypeCreateOrUpdate) => {
        return api.post(`${AUTH_END_POINT}`, project);
    }

    static Update = (id: string, project: IActivityTypeCreateOrUpdate) => {
        return api.put(`${AUTH_END_POINT}/${id}`, project);
    }

    static FindById = (id: string): Promise<AxiosResponse<IActivityTypeCreateOrUpdate>> => {
        return api.get(`${AUTH_END_POINT}/${id}`);
    }

    static List = (query: IPaginationQuery): Promise<AxiosResponse<IPageResult<IActivityTypeList[]>>> => {
        return api.post(`${AUTH_END_POINT}/list`, query);
    }

    static ListAll = (projectId: string): Promise<AxiosResponse<IActivityTypeList[]>> => {
        return api.post(`${AUTH_END_POINT}/list-all`, { projectId });
    }

    static Delete = (id: string) => {
        return api.delete(`${AUTH_END_POINT}/${id}`);
    }
}