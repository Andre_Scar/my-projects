import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IMyActivitiesByPriorityAndSprint, IMyActivitiesByStatusAndSprint, ITotalActivities, ITotalActivitiesByPriorityAndSprint, ITotalActivitiesBySprint, ITotalStoriesByPriority } from '../types/Dashboard.types';


export const AUTH_END_POINT: string = `dashboard/v1`;


export class ApiDashboard {

    static TotalActivities = (id: string): Promise<AxiosResponse<ITotalActivities>> => {
        return api.get(`${AUTH_END_POINT}/${id}/total-activities`);
    }

    static TotalStoriesByPriority = (id: string): Promise<AxiosResponse<ITotalStoriesByPriority>> => {
        return api.get(`${AUTH_END_POINT}/${id}/total-stories-by-priority`);
    }

    static TotalActivitiesBySprint = (id: string): Promise<AxiosResponse<ITotalActivitiesBySprint[]>> => {
        return api.get(`${AUTH_END_POINT}/${id}/total-activities-by-sprint`);
    }

    static MyActivitiesByStatusAndSprint = (id: string): Promise<AxiosResponse<IMyActivitiesByStatusAndSprint[]>> => {
        return api.get(`${AUTH_END_POINT}/${id}/my-activities-by-status-and-sprint`);
    }

    static MyActivitiesByPriorityAndSprint = (id: string): Promise<AxiosResponse<IMyActivitiesByPriorityAndSprint[]>> => {
        return api.get(`${AUTH_END_POINT}/${id}/my-activities-by-priority-and-sprint`);
    }

    static TotalActivitiesByPriorityAndSprint = (id: string): Promise<AxiosResponse<ITotalActivitiesByPriorityAndSprint[]>> => {
        return api.get(`${AUTH_END_POINT}/${id}/total-activities-by-priority-and-sprint`);
    }

}