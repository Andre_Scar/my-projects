import { WithStylesProps } from "react-jss";
import { getStyles } from "./Dashboard.styles";

export interface IDashboardProps extends WithStylesProps<typeof getStyles> { }