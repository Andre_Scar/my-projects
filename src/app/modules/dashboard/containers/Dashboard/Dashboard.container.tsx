import React from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import MyActivitiesByPriorityAndSprint from './components/MyActivitiesByPriorityAndSprint/MyActivitiesByPriorityAndSprint';
import MyActivitiesByStatusAndSprint from './components/MyActivitiesByStatusAndSprint/MyActivitiesByStatusAndSprint';
import TotalActivities from './components/TotalActivities/TotalActivities';
import TotalActivitiesByPriorityAndSprint from './components/TotalActivitiesByPriorityAndSprint/TotalActivitiesByPriorityAndSprint';
import TotalActivitiesBySprint from './components/TotalActivitiesBySprint/TotalActivitiesBySprint';
import TotalStoriesByPriority from './components/TotalStoriesByPriority/TotalStoriesByPriority';
import { getStyles } from './Dashboard.styles';
import { IDashboardProps } from './Dashboard.types';

const Dashboard: React.FC<IDashboardProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row xs={1} md={1} lg={2}>
                    <Col className={classes.chart}>
                        <TotalActivities/>
                    </Col>
                    <Col className={classes.chart}>
                        <TotalStoriesByPriority/>
                    </Col>
                    <Col className={classes.chart}>
                        <TotalActivitiesBySprint/>
                    </Col>
                    <Col className={classes.chart}>
                        <TotalActivitiesByPriorityAndSprint/>
                    </Col>
                    <Col className={classes.chart}>
                        <MyActivitiesByStatusAndSprint/>
                    </Col>
                    <Col className={classes.chart}>
                        <MyActivitiesByPriorityAndSprint/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(Dashboard);