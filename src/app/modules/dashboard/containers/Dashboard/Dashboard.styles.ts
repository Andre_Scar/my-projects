export const getStyles = {
    root: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: '40px 56px',
        overflow: 'auto',
    },

    chart: {
        display: 'flex',
        flexDirection: 'column',
    }
};