import { WithStylesProps } from "react-jss";
import { getStyles } from "./TotalActivitiesByPriorityAndSprint.styles";

export interface ITotalActivitiesByPriorityAndSprintProps extends WithStylesProps<typeof getStyles> {
}