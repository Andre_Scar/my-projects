import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import Loading from '../../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { ApiDashboard } from '../../../../api/Dashboard.api';
import { ITotalActivitiesByPriorityAndSprint } from '../../../../types/Dashboard.types';
import { getStyles } from './TotalActivitiesByPriorityAndSprint.styles';
import { ITotalActivitiesByPriorityAndSprintProps } from './TotalActivitiesByPriorityAndSprint.types';

const TotalActivitiesByPriorityAndSprint: React.FC<ITotalActivitiesByPriorityAndSprintProps> = (props) => {

	//#region CONSTS
	const {
		classes
	} = props;

	const [loading, setLoading] = useState<boolean>(false);

	const [data, setData] = useState<any>({});

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		scales: {
			yAxes: [
				{
					ticks: {
						beginAtZero: true,
					},
				},
			],
		},
	}
	//#endregion

	//#region LIFECYCLE
	useEffect(() => {
        load();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
	//#endregion

	//#region METHODS
	const load = () => {

		setLoading(true);

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        const successCallback = (response: AxiosResponse<ITotalActivitiesByPriorityAndSprint[]>) => {

            const { data } = response;

            createChart(data);
        }

        const failureCallback = () => {
            toast.error('Erro ao carregar total de atividades por prioridade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiDashboard.TotalActivitiesByPriorityAndSprint(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

	const createChart = (totalActivitiesBySprint: ITotalActivitiesByPriorityAndSprint[]) => {

		let series:any = {
			urgent: [],
			high: [],
			medium: [],
			low: []
		};

		let labels: string[] = [];
		totalActivitiesBySprint.forEach(e => {
			labels.push(e.sprintName);
			series.urgent.push(e.urgent);
			series.high.push(e.high);
			series.medium.push(e.medium);
			series.low.push(e.low);
		});

		setData({
			labels,
			datasets: [
				{
					label: 'Urgente',
					data: series.urgent,
					fill: false,
					backgroundColor: 'rgba(255, 99, 132, 0.2)',
					borderColor: 'rgba(255, 99, 132, 1)',
					borderWidth: 1
				},
				{
					label: 'Alto',
					data: series.high,
					fill: false,
					backgroundColor: 'rgba(255, 206, 86, 0.2)',
					borderColor: 'rgba(255, 206, 86, 1)',
					borderWidth: 1
				},
				{
					label: 'Médio',
					data: series.medium,
					fill: false,
					backgroundColor: 'rgba(54, 162, 235, 0.2)',
					borderColor: 'rgba(54, 162, 235, 1)',
					borderWidth: 1
				},
				{
					label: 'Baixo',
					data: series.low,
					fill: false,
					backgroundColor: 'rgba(75, 192, 192, 0.2)',
					borderColor: 'rgba(75, 192, 192, 1)',
					borderWidth: 1
				},
			],
		});
	}
	//#endregion

	//#region RENDER METHODS
	const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loadingContant}>
            <Loading/>
        </div>
    }
	//#endregion

	return <div className={classes.root}>
		{renderLoading()}

		<div className={classes.title}>Total de atividades por prioridade</div>
		<div className={classes.chartContainer}>
			<Bar data={data} options={options} />
		</div>
	</div>
}

export default withStyles(getStyles)(TotalActivitiesByPriorityAndSprint);