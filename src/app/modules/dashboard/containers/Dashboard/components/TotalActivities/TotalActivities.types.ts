import { WithStylesProps } from "react-jss";
import { getStyles } from "./TotalActivities.styles";

export interface ITotalActivitiesProps extends WithStylesProps<typeof getStyles> {
}