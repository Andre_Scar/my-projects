import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import Loading from '../../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { ApiDashboard } from '../../../../api/Dashboard.api';
import { ITotalActivities } from '../../../../types/Dashboard.types';
import { getStyles } from './TotalActivities.styles';
import { ITotalActivitiesProps } from './TotalActivities.types';

const TotalActivities: React.FC<ITotalActivitiesProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    const [loading, setLoading] = useState<boolean>(false);
    const [totalActivities, setTotalActivities] = useState<ITotalActivities>();

    const data = {
        labels: ['Minhas', 'Outros'],
        datasets: [
            {
                data: [totalActivities?.countMy || 0, totalActivities?.countOthers || 0],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1,
            },
        ],
    };

    const options = {
		responsive: true,
		maintainAspectRatio: false
	};
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        load();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const load = () => {

        setLoading(true);

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        const successCallback = (response: AxiosResponse<ITotalActivities>) => {

            const { data } = response;

            setTotalActivities(data);
        }

        const failureCallback = () => {
            toast.error('Erro ao carregar total de atividades.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiDashboard.TotalActivities(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loadingContant}>
            <Loading/>
        </div>
    }
    //#endregion

    return <div className={classes.root}>
        {renderLoading()}

        <div className={classes.title}>Total de atividades</div>
        <div className={classes.chartContainer}>
            <Pie data={data} options={options}/>
        </div>
    </div>
}

export default withStyles(getStyles)(TotalActivities);