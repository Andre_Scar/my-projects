import { WithStylesProps } from "react-jss";
import { getStyles } from "./MyActivitiesByPriorityAndSprint.styles";

export interface IMyActivitiesByPriorityAndSprintProps extends WithStylesProps<typeof getStyles> {
}