import { WithStylesProps } from "react-jss";
import { getStyles } from "./MyActivitiesByStatusAndSprint.styles";

export interface IMyActivitiesByStatusAndSprintProps extends WithStylesProps<typeof getStyles> {
}