import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import Loading from '../../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { ApiDashboard } from '../../../../api/Dashboard.api';
import { IMyActivitiesByStatusAndSprint } from '../../../../types/Dashboard.types';
import { getStyles } from './MyActivitiesByStatusAndSprint.styles';
import { IMyActivitiesByStatusAndSprintProps } from './MyActivitiesByStatusAndSprint.types';

const MyActivitiesByStatusAndSprint: React.FC<IMyActivitiesByStatusAndSprintProps> = (props) => {

	//#region CONSTS
	const {
		classes
	} = props;

	const [loading, setLoading] = useState<boolean>(false);
	const [data, setData] = useState<any>({});

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		scales: {
			yAxes: [
				{
					ticks: {
						beginAtZero: true,
					},
				},
			],
		},
	}
	//#endregion

	//#region LIFECYCLE
	useEffect(() => {
        load();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
	//#endregion

	//#region METHODS
	const load = () => {

		setLoading(true);

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        const successCallback = (response: AxiosResponse<IMyActivitiesByStatusAndSprint[]>) => {

            const { data } = response;

            createChart(data);
        }

        const failureCallback = () => {
            toast.error('Erro ao carregar minhas atividades por status.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiDashboard.MyActivitiesByStatusAndSprint(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

	const createChart = (totalActivitiesBySprint: IMyActivitiesByStatusAndSprint[]) => {

		let series:any = {
			done: [],
			inProgress: [],
			toDo: [],
			toVerify: []
		};

		let labels: string[] = [];
		totalActivitiesBySprint.forEach(e => {
			labels.push(e.sprintName);
			series.done.push(e.done);
			series.inProgress.push(e.inProgress);
			series.toDo.push(e.toDo);
			series.toVerify.push(e.toVerify);
		});

		setData({
			labels,
			datasets: [
				{
					label: 'Em Progresso',
					data: series.inProgress,
					fill: false,
					backgroundColor: '#007BFF',
					borderColor: '#007BFF'
				},
				{
					label: 'Verificar',
					data: series.toVerify,
					fill: false,
					backgroundColor: '#FFC107',
					borderColor: '#FFC107'
				},
				{
					label: 'Feito',
					data: series.done,
					fill: false,
					backgroundColor: '#28A745',
					borderColor: '#28A745'
				},
				{
					label: 'A fazer',
					data: series.toDo,
					fill: false,
					backgroundColor: '#17A2B8',
					borderColor: '#17A2B8'
				}
			],
		});
	}
	//#endregion

	//#region RENDER METHODS
	const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loadingContant}>
            <Loading/>
        </div>
    }
	//#endregion

	return <div className={classes.root}>
		{renderLoading()}

		<div className={classes.title}>Minhas atividades por status</div>
		<div className={classes.chartContainer}>
			<Line data={data} options={options} />
		</div>
	</div>
}

export default withStyles(getStyles)(MyActivitiesByStatusAndSprint);