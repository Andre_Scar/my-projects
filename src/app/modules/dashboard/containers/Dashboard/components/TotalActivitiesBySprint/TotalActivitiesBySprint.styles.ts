export const getStyles = {
    root: { 
        flex: 1,
        marginTop: '16px',
        marginBottom: '16px',
        position: 'relative',
        height: '800px',
    },

    title: {
        fontWeight: 'bold',
        marginBottom: '8px',
    },

    loadingContant: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        zIndex: 1,
        backgroundColor: '#FFFFFF',
        border: 'solid 1px #CCCCCC',
        borderRadius: '5px'
    },

    chartContainer: {
        height: '300px',
        width: '100%',
        position: 'relative',
    }
};