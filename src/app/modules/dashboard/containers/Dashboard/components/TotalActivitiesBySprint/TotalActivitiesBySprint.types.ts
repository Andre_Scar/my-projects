import { WithStylesProps } from "react-jss";
import { getStyles } from "./TotalActivitiesBySprint.styles";

export interface ITotalActivitiesBySprintProps extends WithStylesProps<typeof getStyles> {
}