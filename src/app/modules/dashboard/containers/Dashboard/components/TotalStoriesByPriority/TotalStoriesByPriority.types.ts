import { WithStylesProps } from "react-jss";
import { getStyles } from "./TotalStoriesByPriority.styles";

export interface ITotalStoriesByPriorityProps extends WithStylesProps<typeof getStyles> {
}