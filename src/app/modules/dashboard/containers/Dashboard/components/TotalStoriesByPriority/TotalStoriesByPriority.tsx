import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import Loading from '../../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { Priority } from '../../../../../board/types/Board.types';
import { ApiDashboard } from '../../../../api/Dashboard.api';
import { ITotalStoriesByPriority } from '../../../../types/Dashboard.types';
import { getStyles } from './TotalStoriesByPriority.styles';
import { ITotalStoriesByPriorityProps } from './TotalStoriesByPriority.types';

const TotalStoriesByPriority: React.FC<ITotalStoriesByPriorityProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    const [loading, setLoading] = useState<boolean>(false);
    const [totalStoriesByPriority, setTotalStoriesByPriority] = useState<ITotalStoriesByPriority>({
        urgent: 0,
        high: 0,
        medium: 0,
        low: 0
    });

    const data = {
        labels: ['Urgente', 'Alto', 'Médio', 'Baixo'],
        datasets: [
            {
                data: [
                    totalStoriesByPriority[Priority.URGENT], 
                    totalStoriesByPriority[Priority.HIGH],
                    totalStoriesByPriority[Priority.MEDIUM],
                    totalStoriesByPriority[Priority.LOW], 
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(75, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 206, 86, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1,
            },
        ],
    };

    const options = {
		responsive: true,
		maintainAspectRatio: false
	};
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        load();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const load = () => {

        setLoading(true);

        const currentProject = ProjectService.GetCurrentProject();
        const projectId = currentProject?.id || '';

        const successCallback = (response: AxiosResponse<ITotalStoriesByPriority>) => {

            const { data } = response;

            setTotalStoriesByPriority(data);
        }

        const failureCallback = () => {
            toast.error('Erro ao carregar total de histórias por prioridade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiDashboard.TotalStoriesByPriority(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loadingContant}>
            <Loading/>
        </div>
    }
    //#endregion

    return <div className={classes.root}>
        {renderLoading()}

        <div className={classes.title}>Total de histórias por prioridade</div>
        <div className={classes.chartContainer}>
            <Pie data={data} options={options}/>
        </div>
    </div>
}

export default withStyles(getStyles)(TotalStoriesByPriority);