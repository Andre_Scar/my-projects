import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import Dashboard from '../containers/Dashboard/Dashboard.container';


export const DASHBOARD_PATH = '/dashboard';

const DashboardRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={DASHBOARD_PATH} component={Dashboard} layoutType={LayoutType.NAVBAR_AND_MENU} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default DashboardRoutes;