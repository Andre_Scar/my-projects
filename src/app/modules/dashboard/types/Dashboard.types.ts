
export interface ITotalActivities {
    countMy: number;
    countOthers: number;
}

export interface ITotalStoriesByPriority {
    urgent: number;
    high: number;
    medium: number;
    low: number;
}

export interface ITotalActivitiesBySprint {
    sprintId: string;
    sprintName: string;
    toDo: number;
    inProgress: number;
    toVerify: number;
    done: number;
}

export interface IMyActivitiesByStatusAndSprint {
    sprintId: string;
    sprintName: string;
    toDo: number;
    inProgress: number;
    toVerify: number;
    done: number;
}

export interface IMyActivitiesByPriorityAndSprint {
    sprintId: string;
    sprintName: string;
    urgent: number;
    high: number;
    medium: number;
    low: number;
}

export interface ITotalActivitiesByPriorityAndSprint {
    sprintId: string;
    sprintName: string;
    urgent: number;
    high: number;
    medium: number;
    low: number;
}