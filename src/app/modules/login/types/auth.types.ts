import { ICommon } from "../../../types/Common.types";

export interface IUser extends ICommon {
    name?: string;
    email: string;
    password?: string;
    passwordResetToken?: string;
    passwordResetExpires?: Date;
    createdAt?: string;
}


export interface IAuthenticate extends ICommon{
    email: string;
    password: string;
}

export interface IAuthenticateResponse extends ICommon{
    user: IUser;
    token: string;
}


export interface IRegister extends ICommon {
    name: string;
    email: string;
    password: string;
}

export interface IRegisterResponse extends ICommon {
    user: IUser;
    token: string;
}

export interface IForgotPassword {
    email: string;
}

export interface IResetPassword {
    email: string;
    token: string; 
    password: string;
}

export interface IResetPasswordURL {
    token: string;
}