import { WithStylesProps } from "react-jss";
import { getStyles } from "./Login.styles";

export interface ILoginProps extends WithStylesProps<typeof getStyles> { }