export const getStyles = {
    root: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    content: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        maxWidth: '65%',
        minWidth: '900px',
        height: '530px',
    },

    cardForm: {
        display: 'flex',
        justifyContent: 'center',
        width: '450px',
        paddingLeft: '56px',
        paddingRight: '56px',
    },

    cardInfo: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        marginTop: '36px',
        marginBottom: '36px',
        padding: '40px 56px',
    },

    message: {
        marginTop: '24px',  
    },

    messageFooter: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',

        '& > p': {
            marginBottom: 0,
        }
    },

    '@media (max-width: 900px)': {

        content: {
            margin: '0',
            minWidth: '100%',
            height: '100%'
        },

        cardInfo: {
            display: 'none'
        }

    },

    '@media (max-width: 450px)': {

        cardForm: {
            width: '100%'
        },

    }
};