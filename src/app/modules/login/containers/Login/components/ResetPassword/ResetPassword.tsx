import React, { useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ResetPassword.styles';
import { IResetPasswordProps, IResetPasswordSchema } from './ResetPassword.types';
import { getResetPasswordSchema } from './ResetPassword.schema';
import { FormikHelpers } from 'formik';
import { Button } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { Form, FormField, FormFieldTypes, FormModes } from '../../../../../../components/Form';
import { IResetPassword, IResetPasswordURL } from '../../../../types/auth.types';
import { LOGIN_PATH } from '../../../../routes/Login.routes';
import { ApiAuth } from '../../../../api/Login.api';
import { toast } from 'react-toastify';

const ResetPassword: React.FC<IResetPasswordProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();
    let { token } = useParams<IResetPasswordURL>();

    const formId = 'ResetPassword';
    const initialValues = {
        email: '',
        password: ''
    };

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const onSubmit = (values: IResetPasswordSchema, actions: FormikHelpers<any>) => { 

        setLoading(true);

        const successCallback = () => {
            history.push(LOGIN_PATH);
            toast.success('Sucesso ao redefinir senha.');
        }

        const failureCallback = () => {
            toast.error('Erro ao redefinir senha.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        const resetPassword: IResetPassword = { 
            email: values.email,
            password: values.password,
            token: token
        };

        ApiAuth.ResetPassword(resetPassword)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const goToLogin = () => {
        history.push(LOGIN_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <div className={classes.title}>Redefinir senha</div>
            <Form 
                id={formId} 
                mode={FormModes.EDIT} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getResetPasswordSchema()}
                className={classes.form}>
                
                <div className={classes.body}>
                    <FormField name="email" label="E-mail" type={FormFieldTypes.EMAIL}/>

                    <FormField name="password" label="Senha" type={FormFieldTypes.PASSWORD}/>

                    <FormField name="confirmPassword" label="Confirmar senha" type={FormFieldTypes.PASSWORD}/>
                </div>

                <div className={classes.actions}>
                    <Button variant="secondary" onClick={goToLogin} disabled={loading}>
                        Cancelar
                    </Button>
                    <Button variant="primary" type="submit" form={formId} disabled={loading}>
                        {loading ? 'Carregando…' : 'Continuar'}
                    </Button>
                </div>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ResetPassword);