import * as Yup from 'yup';
import { IResetPasswordSchema } from './ResetPassword.types';

export const getResetPasswordSchema = () => {
    return Yup.object().shape<IResetPasswordSchema>({
        email: Yup
            .string()
            .required('O campo e-mail é obrigatorio')
            .email('Informe um email válido'),
        password: Yup
            .string()
            .required('O campo senha é obrigatorio')
            .min(6, 'A senha contem no minimo 6 caracteres'),
        confirmPassword: Yup
            .string()
            .oneOf([Yup.ref('password')], 'Passwords must match')
            .required('O campo confirmar senha é obrigatorio')
    });
}