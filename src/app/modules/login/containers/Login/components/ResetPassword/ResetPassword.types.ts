import { WithStylesProps } from "react-jss";
import { getStyles } from "./ResetPassword.styles";

export interface IResetPasswordProps extends WithStylesProps<typeof getStyles> { }

export interface IResetPasswordSchema {
    email: string;
    password: string;
    confirmPassword: string;
}