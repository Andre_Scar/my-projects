import React, { useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ForgotPassword.styles';
import { IForgotPasswordProps, IForgotPasswordSchema } from './ForgotPassword.types';
import { getForgotPasswordSchema } from './ForgotPassword.schema';
import { FormikHelpers } from 'formik';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Form, FormField, FormFieldTypes, FormModes } from '../../../../../../components/Form';
import { IForgotPassword } from '../../../../types/auth.types';
import { LOGIN_PATH } from '../../../../routes/Login.routes';
import { ApiAuth } from '../../../../api/Login.api';
import { toast } from 'react-toastify';

const ForgotPassword: React.FC<IForgotPasswordProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const formId = 'ForgotPassword';
    const initialValues = {
        email: '',
        password: ''
    };

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const onSubmit = (values: IForgotPasswordSchema, actions: FormikHelpers<any>) => { 

        setLoading(true);

        const successCallback = () => {
            history.push(LOGIN_PATH);
            toast.success('Sucesso ao enviar e-mail de recuperação de senha.');
        }

        const failureCallback = () => {
            toast.error('Erro ao enviar e-mail de recuperação de senha.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        const forgotPassword: IForgotPassword = { 
            email: values.email,
        };

        ApiAuth.ForgotPassword(forgotPassword)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const goToLogin = () => {
        history.push(LOGIN_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <div className={classes.title}>Recuperar senha</div>
            <Form 
                id={formId} 
                mode={FormModes.EDIT} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getForgotPasswordSchema()}
                className={classes.form}>
                
                <div className={classes.body}>
                    <FormField name="email" label="E-mail" type={FormFieldTypes.EMAIL}/>
                </div>

                <div className={classes.actions}>
                    <Button variant="secondary" onClick={goToLogin} disabled={loading}>
                        Cancelar
                    </Button>
                    <Button variant="primary" type="submit" form={formId} disabled={loading}>
                        {loading ? 'Carregando…' : 'Continuar'}
                    </Button>
                </div>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ForgotPassword);