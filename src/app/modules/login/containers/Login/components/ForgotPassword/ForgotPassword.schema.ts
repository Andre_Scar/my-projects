import * as Yup from 'yup';
import { IForgotPasswordSchema } from './ForgotPassword.types';

export const getForgotPasswordSchema = () => {
    return Yup.object().shape<IForgotPasswordSchema>({
        email: Yup
            .string()
            .required('O campo e-mail é obrigatorio')
            .email('Informe um email válido')
    });
}