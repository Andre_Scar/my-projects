import { WithStylesProps } from "react-jss";
import { getStyles } from "./ForgotPassword.styles";

export interface IForgotPasswordProps extends WithStylesProps<typeof getStyles> { }

export interface IForgotPasswordSchema {
    email: string;
}