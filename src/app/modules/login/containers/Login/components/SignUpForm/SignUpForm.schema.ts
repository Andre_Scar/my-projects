import * as Yup from 'yup';
import { ISignUpFormSchema } from './SignUpForm.types';

export const getSignUpFormSchema = () => {
    return Yup.object().shape<ISignUpFormSchema>({
        name: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        email: Yup
            .string()
            .required('O campo e-mail é obrigatorio')
            .email('Informe um email válido'),
        password: Yup
            .string()
            .required('O campo senha é obrigatorio')
            .min(6, 'A senha contem no minimo 6 caracteres'),
        confirmPassword: Yup
            .string()
            .oneOf([Yup.ref('password')], 'Passwords must match')
            .required('O campo confirmar senha é obrigatorio')
    });
}