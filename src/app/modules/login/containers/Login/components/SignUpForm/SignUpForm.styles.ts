export const getStyles = {

    root: { 
        flex: 1,
        paddingTop: '24px',
        paddingBottom: '24px',
        display: 'flex',
        flexDirection: 'column',
    },

    title: {
        fontWeight: '400',
        fontSize: '32px',
        marginBottom: '8px',
    },

    form: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,

        '& > form': {
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
        }
    },

    body: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,

        '& > .form-group': {
            marginBottom: 0
        }
    },

    actions: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: '8px',
        

        '& > button': {
            marginRight: '8px',
        },

        '& > button:last-child': {
            marginRight: 0,
        }
    }

};