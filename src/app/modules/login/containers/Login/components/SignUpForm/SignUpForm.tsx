import React, { useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './SignUpForm.styles';
import { ISignUpFormProps, ISignUpFormSchema } from './SignUpForm.types';
import { getSignUpFormSchema } from './SignUpForm.schema';
import { FormikHelpers } from 'formik';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Form, FormField, FormFieldTypes, FormModes } from '../../../../../../components/Form';
import { IRegister, IRegisterResponse } from '../../../../types/auth.types';
import { LOGIN_PATH } from '../../../../routes/Login.routes';
import { ApiAuth } from '../../../../api/Login.api';
import { toast } from 'react-toastify';
import { AxiosResponse } from 'axios';
import { AuthService } from '../../../../../../services/auth/auth.service';
import { PROJECTS_PATH } from '../../../../../projects/routes/Projects.routes';

const SignUpForm: React.FC<ISignUpFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const formId = 'SignUpForm';
    const initialValues = {
        email: '',
        password: ''
    };

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const onSubmit = (values: ISignUpFormSchema, actions: FormikHelpers<any>) => { 

        setLoading(true);

        const successCallback = (response: AxiosResponse<IRegisterResponse>) => {
            
            const { token, user } = response.data;
            
            AuthService.SetToken(token);
            AuthService.SetUser(user._id || '', user.name || '');

            history.push(PROJECTS_PATH);
        }

        const failureCallback = () => {
            toast.error('Erro ao cadastrar nova conta.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        const newUser: IRegister = { 
            name: values.name, 
            email: values.email, 
            password: values.password
        };

        ApiAuth.Register(newUser)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const goToLogin = () => {
        history.push(LOGIN_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <div className={classes.title}>Novo usuário</div>
            <Form 
                id={formId} 
                mode={FormModes.EDIT} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getSignUpFormSchema()}
                className={classes.form}>

                <div className={classes.body}>
                    <FormField name="name" label="Nome" type={FormFieldTypes.TEXT}/>

                    <FormField name="email" label="E-mail" type={FormFieldTypes.EMAIL}/>

                    <FormField name="password" label="Senha" type={FormFieldTypes.PASSWORD}/>

                    <FormField name="confirmPassword" label="Confirmar senha" type={FormFieldTypes.PASSWORD}/>
                </div>

                <div className={classes.actions}>
                    <Button variant="secondary" onClick={goToLogin} disabled={loading}>
                        Cancelar
                    </Button>
                    <Button variant="primary" type="submit" form={formId} disabled={loading}>
                        {loading ? 'Carregando…' : 'Continuar'}
                    </Button>
                </div>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(SignUpForm);