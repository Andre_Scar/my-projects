import { WithStylesProps } from "react-jss";
import { getStyles } from "./SignUpForm.styles";

export interface ISignUpFormProps extends WithStylesProps<typeof getStyles> { }

export interface ISignUpFormSchema {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}