import { WithStylesProps }  from 'react-jss';
import { getStyles } from './LoginForm.styles';

export interface ILoginFormProps extends WithStylesProps<typeof getStyles> { }

export interface ILoginFormSchema {
    email: string;
    password: string;
}