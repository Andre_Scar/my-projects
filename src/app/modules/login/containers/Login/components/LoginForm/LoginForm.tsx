import React, { useState } from 'react';
import { ILoginFormProps, ILoginFormSchema } from './LoginForm.types';
import { getStyles } from './LoginForm.styles';
import withStyles from 'react-jss';
import { Button } from 'react-bootstrap';
import { FormikHelpers } from 'formik/dist/types';
import { getLoginFormSchema } from './LoginForm.schema';
import { useHistory } from 'react-router-dom';
import { Form, FormField, FormFieldTypes, FormModes } from '../../../../../../components/Form';
import Logo from '../../../../../../components/Logo/Logo';
import Divider from '../../../../../../components/Divider/Divider';
import ExternalLoginButton from '../../../../../../components/ExternalLoginButton/ExternalLoginButton';
import { ExternalLoginType } from '../../../../../../components/ExternalLoginButton/ExternalLoginButton.types';
import Link from '../../../../../../components/Link/Link';
import { AuthService } from '../../../../../../services/auth/auth.service';
import { FORGOT_PASSWORD_PATH, SIGN_UP_PATH } from '../../../../routes/Login.routes';
import GoogleLogin from 'react-google-login';
import { toast } from 'react-toastify';
import { PROJECTS_PATH } from '../../../../../projects/routes/Projects.routes';

const LoginForm: React.FC<ILoginFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    const formId = 'FormLogin';
    const initialValues = {
        email: '',
        password: ''
    };

    const [loading, setLoading] = useState<boolean>(false);
    const [disabled, setDisabled] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const onSubmit = (values: ILoginFormSchema, actions: FormikHelpers<any>) => { 
        setLoading(true);

        const successCallback = () => {
            history.push(PROJECTS_PATH);
        }

        const failureCallback = (reason: any) => {

            const { response: { status, data } } = reason;
            
            if(status === 404) {
                return actions.setErrors({'email': data.error});
            }

            return actions.setErrors({'password': 'Senha inválida'});

        }

        const finallyCallback = () => {
            setLoading(false);
        }

        AuthService.Login({...values})
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const goToSignUp = () => {
        history.push(SIGN_UP_PATH);
    }

    const goToForgotPassword = () => {
        history.push(FORGOT_PASSWORD_PATH);
    }

    const onSuccess = async (googleData: any) => {

        if(!googleData.tokenId) {
            return;
        }

        setLoading(true);

        const successCallback = () => {
            history.push("/projects");
        }

        const failureCallback = (reason: any) => {
            if(reason && reason.response && reason.response.data) {
                toast.error(`${reason.response.data.error}.`);
            }

            toast.error('Erro ao realizar login.');
        }

        const finallyCallback = () => {
            setLoading(false);
            setDisabled(false);
        }

        AuthService.LoginGoogle(googleData.tokenId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onFailure = (error: any) => {
        toast.error('Erro ao realizar login.');
        setDisabled(false);
    }

    const onClickExternalLogin = (onClick: () => void): void => {
        setDisabled(true);
        onClick();
    }
    //#endregion

    //#region RENDER METHODS
    const renderExternalLoginButton = (onClick: () => void) => {
        return <ExternalLoginButton 
            onClick={() => onClickExternalLogin(onClick)} 
            label="Continue com Google" 
            type={ExternalLoginType.GOOGLE}
            disabled={loading || disabled}/>
    }
    //#endregion
    
    return ( 
        <div className={classes.root}>
            <Logo className={classes.logo} height={'45px'}/>

            <Form 
                id={formId} 
                mode={FormModes.EDIT} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getLoginFormSchema()}>

                <FormField name="email" label="E-mail" type={FormFieldTypes.EMAIL}/>

                <FormField name="password" label="Senha" type={FormFieldTypes.PASSWORD}/>
                
                <div className={classes.actions}>
                    <Link onClick={goToForgotPassword}>Esqueceu sua senha?</Link>
                    <Button variant="primary" type="submit" form={formId} disabled={loading || disabled}>
                        {loading ? 'Carregando…' : 'Entrar'}
                    </Button>
                </div>
            </Form>

            <Divider label={'Ou'}/>

            <div className={classes.externalLoginButton}>
                <GoogleLogin
                    clientId={process.env.REACT_APP_CLIENT_ID || ''}
                    render={renderProps => renderExternalLoginButton(renderProps.onClick)}
                    buttonText="Log in with Google"
                    onSuccess={onSuccess}
                    onFailure={onFailure}
                    cookiePolicy={'single_host_origin'}
                    disabled={loading || disabled}
                />
            </div>

            <div className={classes.signUp}>
                <Link onClick={goToSignUp}>Ainda não esta no My Projects? Crie uma conta</Link>
            </div>
        </div>
    );
}

export default withStyles(getStyles)(LoginForm);