export const getStyles = {
    
    root: {
        flex: 1,
        paddingTop: '40px',
        paddingBottom: '40px',
    },
    
    logo: {
        marginBottom: '32px',
        height: '45px'
    },

    actions: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    signUp: {
        display: 'flex',
        justifyContent: 'flex-start',
        marginTop: '24px',
    },

    externalLoginButton: {
        marginTop: '8px',
        display: 'flex',
        flexDirection: 'column'
    }
    
};