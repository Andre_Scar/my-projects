import * as Yup from 'yup';
import { ILoginFormSchema } from './LoginForm.types';

export const getLoginFormSchema = () => {
    return Yup.object().shape<ILoginFormSchema>({
        email: Yup
            .string()
            .required('O campo e-mail é obrigatorio')
            .email('Informe um email válido'),
        password: Yup
            .string()
            .required('O campo senha é obrigatorio')
            .min(6, 'A senha contem no minimo 6 caracteres'),
    });
}