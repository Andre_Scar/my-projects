import React from 'react';
import { useLocation } from "react-router-dom";
import withStyles from 'react-jss';
import DefoultCard from '../../../../components/DefoultCard/DefoultCard';
import { getStyles } from './Login.styles';
import { ILoginProps } from './Login.types';
import LoginForm from './components/LoginForm/LoginForm';
import SignUpForm from './components/SignUpForm/SignUpForm';
import { FORGOT_PASSWORD_PATH, RESET_PASSWORD_PATH, SIGN_UP_PATH } from '../../routes/Login.routes';
import ForgotPassword from './components/ForgotPassword/ForgotPassword';
import ResetPassword from './components/ResetPassword/ResetPassword';

const Login: React.FC<ILoginProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let location = useLocation();
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderForm = () => {
        if(location.pathname.indexOf(SIGN_UP_PATH) > -1) {
            return <SignUpForm/>;
        }

        if(location.pathname.indexOf(FORGOT_PASSWORD_PATH) > -1) {
            return <ForgotPassword/>;
        }

        if(location.pathname.indexOf(RESET_PASSWORD_PATH) > -1) {
            return <ResetPassword/>;
        }
        
        return <LoginForm/>;
    }
    //#endregion

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <DefoultCard className={classes.cardInfo}>
                    <h1>Bem-vindo!</h1>
                    <p className={classes.message}>
                        Este sistema foi desenvolvido como requisito parcial à obtenção do título de especialista em Desenvolvimento Web Full Stack.
                    </p>
                    <div className={classes.messageFooter}>
                        <p>PUC Minas</p>
                        <p>2021</p>
                    </div>
                </DefoultCard>
                <DefoultCard className={classes.cardForm}>
                    {renderForm()}
                </DefoultCard>
            </div>
        </div>
    );
}

export default withStyles(getStyles)(Login);