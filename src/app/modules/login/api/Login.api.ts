import { AxiosResponse } from 'axios';
import axios from "axios";
import { 
    IAuthenticate, 
    IAuthenticateResponse, 
    IForgotPassword, 
    IRegister, 
    IRegisterResponse, 
    IResetPassword
} from '../types/auth.types';

const api = axios.create({
    baseURL: process.env.REACT_APP_WS_URL
});

export const AUTH_END_POINT: string = `auth/v1`;


export class ApiAuth {

    static Authenticate = (authenticate: IAuthenticate): Promise<AxiosResponse<IAuthenticateResponse>> => {
        return api.post(`${AUTH_END_POINT}/authenticate`, authenticate);
    }
    
    static Register = (newUser: IRegister): Promise<AxiosResponse<IRegisterResponse>> => {
        return api.post(`${AUTH_END_POINT}/register`, newUser);
    }
    
    static ForgotPassword = (forgotPassword: IForgotPassword): Promise<AxiosResponse<any>> => {
        return api.post(`${AUTH_END_POINT}/forgot-password`, forgotPassword);
    }
    
    static ResetPassword = (resetPassword: IResetPassword): Promise<AxiosResponse<any>> => {
        return api.post(`${AUTH_END_POINT}/reset-password`, resetPassword);
    }

    static Google = (tokenId: string): Promise<AxiosResponse<any>> => {
        return api.post(`${AUTH_END_POINT}/google`, {tokenId});
    }

}