import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import Login from '../containers/Login/Login.container';


export const LOGIN_PATH = '/login';
export const SIGN_UP_PATH = '/sign-up';
export const FORGOT_PASSWORD_PATH = '/forgot-password';
export const RESET_PASSWORD_PATH = '/reset-password';


const LoginRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route path={LOGIN_PATH} component={Login} layoutType={LayoutType.BLANK}/>
                <Route path={SIGN_UP_PATH} component={Login} layoutType={LayoutType.BLANK}/>
                <Route path={FORGOT_PASSWORD_PATH} component={Login} layoutType={LayoutType.BLANK}/>
                <Route path={`${RESET_PASSWORD_PATH}/:token`} component={Login} layoutType={LayoutType.BLANK}/>
            </Switch>
        </>
    )
}

export default LoginRoutes;