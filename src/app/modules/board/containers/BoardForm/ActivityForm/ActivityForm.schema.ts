import * as Yup from 'yup';
import { ActivityStatus, Priority } from '../../../types/Board.types';
import { IActivityFormSchema } from './ActivityForm.types';

export const getActivityFormSchema = () => {
    return Yup.object().shape<IActivityFormSchema>({
        title: Yup
            .string()
            .required('O campo nome é obrigatorio'),
        description: Yup
            .string()
            .notRequired()
            .nullable(),
        responsibleId: Yup
            .string()
            .required('O campo responsável é obrigatorio'),
        typeId: Yup
            .string()
            .required('O campo tipo é obrigatorio'),
        status: Yup
            .mixed<ActivityStatus>()
            .oneOf(Object.values(ActivityStatus))
            .required('O campo status é obrigatorio'),
        priority: Yup
            .mixed<Priority>()
            .oneOf(Object.values(Priority))
            .required('O campo status é obrigatorio'),
        customFields: Yup
            .object()
            .nullable()
    });
}