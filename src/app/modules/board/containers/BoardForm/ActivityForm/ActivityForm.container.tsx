import { AxiosResponse } from 'axios';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import ActivityFormGeneral from './components/ActivityFormGeneral';
import { getActivityFormSchema } from './ActivityForm.schema';
import { getStyles } from './ActivityForm.styles';
import { IActivityFormProps, IActivityFormSchema } from './ActivityForm.types';
import { Form, FormModes } from '../../../../../components/Form';
import { ProjectService } from '../../../../../services/currentProject/currentProject.service';
import FormHeaderLayout from '../../../../../components/FormHeaderLayout/FormHeaderLayout';
import { useFormMode } from '../../../../../components/Form/hooks/FormMode';
import { ActivityStatus, IActivityCreateOrUpdate, IActivityURL, ICustomField, Priority } from '../../../types/Board.types';
import { BOARD_LIST_PATH } from '../../../routes/Board.routes';
import { ApiActivity } from '../../../api/Activity.api';
import { IReason } from '../../../../../types/Common.types';

const ActivityForm: React.FC<IActivityFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    let { id, storyId, sprintId } = useParams<IActivityURL>();

    const { mode } = useFormMode();

    const formId = 'ProjectFormGeneral';

    const [initialValues , setInitialValues] = useState<IActivityFormSchema>({
        title: '',
        description: undefined,
        responsibleId: '',
        typeId: '',
        status: ActivityStatus.TO_DO,
        priority: Priority.LOW,
        customFields: {}
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.EDIT || mode === FormModes.VIEW) {
            loadActivity();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadActivity = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IActivityCreateOrUpdate>) => {

            const { data } = response;
            
            const newInitialValues: IActivityFormSchema = {
                title: data.title,
                description: data.description,
                responsibleId: data.responsibleId,
                typeId: data.typeId,
                status: data.status,
                priority: data.priority,
                customFields: data.customFields
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar atividade para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivity.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const onSubmit = (values: IActivityFormSchema, actions: FormikHelpers<any>) => { 
        const currentProject = ProjectService.GetCurrentProject();

        const projectId = currentProject?.id || '';

        if(projectId === '') {
            return toast.error('Não foi possível encontrar o projeto.');
        }

        if(mode === FormModes.NEW) {
            return save(values, projectId);
        }
        
        return update(values, projectId);
    }

    const onCancel = () => {
        history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
    }

    const save = (values: IActivityFormSchema, projectId: string) => {

        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar atividade.');
            history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar atividade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }
        
        ApiActivity.Create({
            title: values.title,
            description: values.description,
            responsibleId: values.responsibleId,
            typeId: values.typeId,
            status: values.status,
            priority: values.priority,
            storyId: storyId,
            projectId: projectId,
            customFields: formatCustomFields(values.customFields)
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: IActivityFormSchema, projectId: string) => {

        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao editar atividade.');
            history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar atividade.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiActivity.Update(id, {
            _id: id,
            title: values.title,
            description: values.description,
            responsibleId: values.responsibleId,
            typeId: values.typeId,
            status: values.status,
            priority: values.priority,
            storyId: storyId,
            projectId: projectId,
            customFields: formatCustomFields(values.customFields)
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const formatCustomFields = (customFields: any) => {
        if(!customFields) {
            return [];
        }
        
        const keys = Object.keys(customFields);

        const results: ICustomField[] = keys.map(key => {

            if(typeof customFields[key] == 'number') {
                return {
                    activityCustomFieldId: key,
                    valueNumber: customFields[key]
                }
            }

            if(!isNaN(Date.parse(customFields[key]))) {
                return {
                    activityCustomFieldId: key,
                    valueDate: customFields[key]
                }
            }

            return {
                activityCustomFieldId: key,
                valueText: customFields[key]
            }

        });

        return results;
    } 
    //#endregion

    //#region RENDER METHODS
    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Nova atividade';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar atividade';
        }

        return 'Visualizar atividade';
    }
    //#endregion

    return (
        <div className={classes.root}>
            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getActivityFormSchema()}>

                <ActivityFormGeneral activityType={initialValues.typeId} loading={loading}/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(ActivityForm);