import { WithStylesProps } from "react-jss";
import { ActivityStatus, Priority } from "../../../types/Board.types";
import { getStyles } from "./ActivityForm.styles";

export interface IActivityFormProps extends WithStylesProps<typeof getStyles> { }


export interface IActivityFormSchema {
    title: string;
    description?: string | null;
    responsibleId: string;
    typeId: string;
    status: ActivityStatus;
    priority: Priority;
    customFields: any;
}