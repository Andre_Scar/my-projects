export const getStyles = {
    
    root: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    
    loading: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFFE6',
        zIndex: 1,
        top: 0,
        left: 0,
    }
};