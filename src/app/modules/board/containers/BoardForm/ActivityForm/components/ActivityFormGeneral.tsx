import { AxiosResponse } from 'axios';
import { useFormikContext } from 'formik';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { toast } from 'react-toastify';
import { FormField, FormFieldTypes, FormModes, FormSelect, FormTextarea } from '../../../../../../components/Form';
import { useFormMode } from '../../../../../../components/Form/hooks/FormMode';
import Loading from '../../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { IReason } from '../../../../../../types/Common.types';
import { ApiMember } from '../../../../../members/api/Members.api';
import { IMemberList } from '../../../../../members/types/Members.types';
import { ApiActivityCustomField } from '../../../../../settings/api/ActivityCustomField.api';
import { ApiActivityType } from '../../../../../settings/api/ActivityTypes.api';
import { CustomFieldType } from '../../../../../settings/containers/SettingsForm/ActivityCustomFieldForm/ActivityCustomFieldForm.types';
import { IActivityCustomFieldList, IActivityTypeList } from '../../../../../settings/types/Settings.types';
import { ActivityStatus, Priority } from '../../../../types/Board.types';
import { getStyles } from './ActivityFormGeneral.styles';
import { IActivityFormGeneralProps } from './ActivityFormGeneral.types';

const ActivityFormGeneral: React.FC<IActivityFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        activityType,
        loading
    } = props;

    const currentProject = ProjectService.GetCurrentProject();
    const projectId = currentProject?.id || '';

    const { setFieldValue } = useFormikContext();

    const { mode } = useFormMode();

    const [members, setMembers] = useState<IMemberList[]>([]);
    const [activityTypes, setActivityTypes] = useState<IActivityTypeList[]>([]);
    const [activityCustomFiels, setActivityCustomFiels] = useState<IActivityCustomFieldList[]>([]);

    const [loadingMembers, setLoadingMembers] = useState<boolean>(false);
    const [loadingActivityTypes, setLoadingActivityTypes] = useState<boolean>(false);
    const [loadingActivityCustomFiels, setLoadingActivityCustomFiels] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.NEW || activityType !== '') {
            loadMembers();
            loadActivityTypes();
        }
    }, [activityType]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadMembers = () => {
        setLoadingMembers(true);

        const successCallback = (response: AxiosResponse<IMemberList[]>) => {
            const { data } = response;

            if(data.length > 0 && mode === FormModes.NEW) {
                setFieldValue('responsibleId', data[0]._id);
            }

            setMembers(data);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar membros.');
        }

        const finallyCallback = () => {
            setLoadingMembers(false);
        }

        ApiMember.List({projectId: projectId})
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const loadActivityTypes = () => {
        setLoadingActivityTypes(true);

        const successCallback = (response: AxiosResponse<IActivityTypeList[]>) => {
            const { data } = response;

            setActivityTypes(data);

            if(data.length === 0) {
                return setActivityCustomFiels([]);
            }

            const activityTypeId = mode === FormModes.NEW ? data[0]._id : activityType;

            loadActivityCustomFiels(activityTypeId);
            setFieldValue('typeId', activityTypeId);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar tipos de atividade.');
        }

        const finallyCallback = () => {
            setLoadingActivityTypes(false);
        }

        ApiActivityType.ListAll(projectId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const loadActivityCustomFiels = (activityTypeId: string) => {
        setLoadingActivityCustomFiels(true);

        const successCallback = (response: AxiosResponse<IActivityCustomFieldList[]>) => {
            const { data } = response;
            setActivityCustomFiels(data);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar campos personalizados.');
        }

        const finallyCallback = () => {
            setLoadingActivityCustomFiels(false);
        }

        ApiActivityCustomField.ListAll(activityTypeId)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onChangeType = (event:ChangeEvent<HTMLSelectElement>) => {
        const activityTypeId = event.currentTarget.value;

        loadActivityCustomFiels(activityTypeId);

        setFieldValue('customFields', {});
    }

    const getFielType = (type: CustomFieldType) => {
        if(type === CustomFieldType.TEXT) {
            return FormFieldTypes.TEXT;
        }

        if(type === CustomFieldType.NUMBER) {
            return FormFieldTypes.NUMBER;
        }

        return FormFieldTypes.DATE;
    }
    //#endregion

    //#region RENDER METHODS
    const customFielsRender = () => {
        return activityCustomFiels.map(e => {
            return <Row>
                <Col xs sm md={8}>
                    <FormField key={e._id} name={`customFields[${e._id}]`} label={e.name} type={getFielType(e.type)}/>
                </Col>
            </Row>
        });
    }

    const renderLoading = () => {
        if(loading !== true && 
            loadingMembers !== true && 
            loadingActivityTypes !== true &&
            loadingActivityCustomFiels !== true
        ) {
            return;
        }

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormField name="title" label="Título" type={FormFieldTypes.TEXT}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormTextarea name="description" label="Descrição" rows={4}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="responsibleId" label="Responsável">
                            {members.map(e => {
                                return <option value={e._id}>{e.name}</option>
                            })}
                        </FormSelect>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="typeId" label="Tipo" onChange={onChangeType}>
                            {activityTypes.map(e => {
                                return <option value={e._id}>{e.name}</option>
                            })}
                        </FormSelect>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="status" label="Status">
                            <option value={ActivityStatus.TO_DO}>A Fazer</option>
                            <option value={ActivityStatus.IN_PROGRESS}>Em Progresso</option>
                            <option value={ActivityStatus.TO_VERIFY}>Verificar</option>
                            <option value={ActivityStatus.DONE}>Feito</option>
                        </FormSelect>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="priority" label="Prioridade">
                            <option value={Priority.URGENT}>Urgente</option>
                            <option value={Priority.HIGH}>Alto</option>
                            <option value={Priority.MEDIUM}>Médio</option>
                            <option value={Priority.LOW}>Baixo</option>
                        </FormSelect>
                    </Col>
                </Row>
                {customFielsRender()}
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(ActivityFormGeneral);