import { WithStylesProps } from "react-jss";
import { getStyles } from "./ActivityFormGeneral.styles";

export interface IActivityFormGeneralProps extends WithStylesProps<typeof getStyles> { 
    activityType: string;
    loading: boolean;
}