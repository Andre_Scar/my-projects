import * as Yup from 'yup';
import { Priority } from '../../../types/Board.types';
import { IStoryFormSchema } from './StoryForm.types';

export const getStoryFormSchema = () => {
    return Yup.object().shape<IStoryFormSchema>({
        description: Yup
            .string()
            .required('O campo história é obrigatorio'),
        priority: Yup
            .mixed<Priority>()
            .oneOf(Object.values(Priority))
            .required('O campo prioridade é obrigatorio')
    });
}