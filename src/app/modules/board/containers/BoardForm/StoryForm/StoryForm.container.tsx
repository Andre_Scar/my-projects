import { AxiosResponse } from 'axios';
import { FormikHelpers } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Form, FormModes } from '../../../../../components/Form';
import { useFormMode } from '../../../../../components/Form/hooks/FormMode';
import FormHeaderLayout from '../../../../../components/FormHeaderLayout/FormHeaderLayout';
import Loading from '../../../../../components/Loading/Loading';
import { ProjectService } from '../../../../../services/currentProject/currentProject.service';
import { IReason } from '../../../../../types/Common.types';
import { ApiStory } from '../../../api/Story.api';
import { BOARD_LIST_PATH } from '../../../routes/Board.routes';
import { IStoryCreateOrUpdate, IStoryURL, Priority } from '../../../types/Board.types';
import StoryFormGeneral from './components/StoryFormGeneral';
import { getStoryFormSchema } from './StoryForm.schema';
import { getStyles } from './StoryForm.styles';
import { IStoryFormProps, IStoryFormSchema } from './StoryForm.types';

const StoryForm: React.FC<IStoryFormProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let history = useHistory();

    let { id, sprintId } = useParams<IStoryURL>();

    const { mode } = useFormMode();

    const formId = 'ProjectFormGeneral';

    const [initialValues , setInitialValues] = useState<IStoryFormSchema>({
        description: '',
        priority: Priority.LOW
    });

    const [loading, setLoading] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(mode === FormModes.EDIT || mode === FormModes.VIEW) {
            loadSprint();
        }
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadSprint = () => {

        setLoading(true);

        const successCallback = (response: AxiosResponse<IStoryCreateOrUpdate>) => {

            const { data } = response;

            const newInitialValues: IStoryFormSchema = {
                description: data.description,
                priority: data.priority
            };

            setInitialValues(newInitialValues);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar história para edição.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiStory.FindById(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }


    const onSubmit = (values: IStoryFormSchema, actions: FormikHelpers<any>) => { 
        const currentProject = ProjectService.GetCurrentProject();

        const projectId = currentProject?.id || '';

        if(projectId === '') {
            return toast.error('Não foi possível encontrar o projeto.');
        }

        if(mode === FormModes.NEW) {
            return save(values, projectId);
        }
        
        return update(values, projectId);
    }

    const onCancel = () => {
        history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
    }

    const save = (values: IStoryFormSchema, projectId: string) => {
        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao cadastrar história.');
            history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao cadastrar história.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiStory.Create({
            description: values.description,
            priority: values.priority,
            sprintId: sprintId,
            projectId: projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }

    const update = (values: IStoryFormSchema, projectId: string) => {

        setLoading(true);

        const successCallback = () => {
            toast.success('Sucesso ao editar história.');
            history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao editar história.');
        }

        const finallyCallback = () => {
            setLoading(false);
        }

        ApiStory.Update(id, {
            _id: id,
            description: values.description,
            priority: values.priority,
            sprintId: sprintId,
            projectId: projectId
        })
        .then(successCallback, failureCallback)
        .finally(finallyCallback);
    }
    //#endregion

    //#region RENDER METHODS
    const renderLoading = () => {
        if(loading !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderFormLabel = () => {
        if(mode === FormModes.NEW) {
            return 'Nova história';
        }

        if(mode === FormModes.EDIT) {
            return 'Editar história';
        }

        return 'Visualizar história';
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            <FormHeaderLayout title={renderFormLabel()}>
                <Button variant="secondary" onClick={onCancel}>
                    {mode === FormModes.VIEW ? 'Voltar' : 'Cancelar'}
                </Button>
                {mode !== FormModes.VIEW && <Button variant="primary" type="submit" form={formId}>
                    Salvar
                </Button>}
            </FormHeaderLayout>

            <Form 
                id={formId} 
                mode={mode} 
                initialValues={initialValues} 
                onSubmit={onSubmit}
                validationSchema={getStoryFormSchema()}>

                <StoryFormGeneral/>
            </Form>
        </div>
    );
}

export default withStyles(getStyles)(StoryForm);