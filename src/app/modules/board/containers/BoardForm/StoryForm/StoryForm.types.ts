import { WithStylesProps } from "react-jss";
import { Priority } from "../../../types/Board.types";
import { getStyles } from "./StoryForm.styles";

export interface IStoryFormProps extends WithStylesProps<typeof getStyles> { }


export interface IStoryFormSchema {
    description: string;
    priority: Priority;
}