import React from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import withStyles from 'react-jss';
import { FormSelect, FormTextarea } from '../../../../../../components/Form';
import { Priority } from '../../../../types/Board.types';
import { getStyles } from './StoryFormGeneral.styles';
import { IStoryFormGeneralProps } from './StoryFormGeneral.types';

const StoryFormGeneral: React.FC<IStoryFormGeneralProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={classes.root}>
            <Container fluid>
                <Row>
                    <Col xs sm md={8}>
                        <FormTextarea name="description" label="História" rows={4}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm md={8}>
                        <FormSelect name="priority" label="Prioridade">
                            <option value={Priority.URGENT}>Urgente</option>
                            <option value={Priority.HIGH}>Alto</option>
                            <option value={Priority.MEDIUM}>Médio</option>
                            <option value={Priority.LOW}>Baixo</option>
                        </FormSelect>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default withStyles(getStyles)(StoryFormGeneral);