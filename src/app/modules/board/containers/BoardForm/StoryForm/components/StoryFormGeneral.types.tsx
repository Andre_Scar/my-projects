import { WithStylesProps } from "react-jss";
import { getStyles } from "./StoryFormGeneral.styles";

export interface IStoryFormGeneralProps extends WithStylesProps<typeof getStyles> { }