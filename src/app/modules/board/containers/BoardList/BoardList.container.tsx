import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import withStyles from 'react-jss';
import Search from '../../../../components/Search/Search';
import { getStyles } from './BoardList.styles';
import { IBoardListProps, ICustomConfirmDelete } from './BoardList.types';
import ActivityCard from './components/ActivityCard/ActivityCard';
import { useHistory, useLocation } from 'react-router-dom';
import { BOARD_LIST_PATH, NEW_STORY_PATH } from '../../routes/Board.routes';

import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { ActivityStatus, IActivityList, IStoriesWithActivities } from '../../types/Board.types';
import StoryCard from './components/StoryCard/StoryCard';
import { AxiosResponse } from 'axios';
import { ApiStory } from '../../api/Story.api';
import { toast } from 'react-toastify';
import { ApiSprint } from '../../../sprints/api/Sprints.api';
import { ISprintList } from '../../../sprints/types/Sprints.types';
import { IPageResult, IPaginationQuery, IReason } from '../../../../types/Common.types';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { ApiActivity } from '../../api/Activity.api';
import { InputGroup } from 'react-bootstrap';
import NoDataFound from '../../../../components/NoDataFound/NoDataFound';
import Loading from '../../../../components/Loading/Loading';
import { clearTimeDebounde, debounce } from '../../../../components/Debounce/Debounce';
import { Permission } from '../../../members/types/Members.types';
import ConfirmDelete from '../../../../components/ConfirmDelete/ConfirmDelete';


const BoardList: React.FC<IBoardListProps> = (props) => {

    //#region CONSTS
    const {
        classes
    } = props;

    let initialValues:any = {
        sprintId: null
    };

    let history = useHistory();
    let loaction = useLocation();

    const currentProject = ProjectService.GetCurrentProject();

    const [activitiesByStory, setActivitiesByStory] = useState<IStoriesWithActivities[]>([]);
    const [sprints, setSprints] = useState<ISprintList[]>([]);
    const [currentSprintId, setCurrentSprintId] = useState<string | null>(null);

    const [loadingSprints, setLoadingSprints] = useState<boolean>(false);
    const [loadingStories, setLoadingStories] = useState<boolean>(false);
    const [filterName, setFilterName] = useState<any>();

    const [confirmDelete, setConfirmDelete] = useState<ICustomConfirmDelete | null>(null);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        loadSprints();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if(currentSprintId) {
            loadStories(currentSprintId);
        }
    }, [filterName]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const loadSprints = () => {

        setLoadingSprints(true);

        let queryParameters = new URLSearchParams(loaction.search);
        const currentSprintId = queryParameters.get('sprintId');

        const query: IPaginationQuery = {
            page: 0,
            pageSize: 5,
            filter: { project: currentProject ? currentProject.id : '' }
        }

        const successCallback = (response: AxiosResponse<IPageResult<ISprintList[]>>) => {

            const { data } = response;

            if(data.count === 0) {
                return;
            }

            initialValues = {
                sprintId: currentSprintId || data.results[0]._id
            };

            history.push(`${BOARD_LIST_PATH}?sprintId=${initialValues.sprintId}`);

            setSprints(data.results);
            loadStories(initialValues.sprintId);
            setCurrentSprintId(initialValues.sprintId);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar sprints.');
        }

        const finallyCallback = () => {
            setLoadingSprints(false);
        }

        ApiSprint.List(query)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const loadStories = (sprintId:string) => {

        setLoadingStories(true);

        let filter: any = {sprint: sprintId};

        if(filterName !== null) {
            filter.name = filterName;
        }

        const successCallback = (response: AxiosResponse<IStoriesWithActivities[]>) => {

            const { data } = response;
            
            setActivitiesByStory(data);
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao carregar histórias.');
        }

        const finallyCallback = () => {
            setLoadingStories(false);
        }

        ApiStory.ListWithActivities(filter)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onChangeSprint = (event:ChangeEvent<HTMLSelectElement>) => {
        const sprintId = event.currentTarget.value;

        loadStories(sprintId);
        setCurrentSprintId(sprintId);
        history.push(`${BOARD_LIST_PATH}?sprintId=${sprintId}`);
    }

    const newStory = () => {
        history.push(`${NEW_STORY_PATH}/${currentSprintId}`);
    }

    const deleteStory = (id: string) => {
        setLoadingStories(true);
        closeConfirmDelete();

        const successCallback = () => {
            if(activitiesByStory === null) {
                return toast.error('Erro ao deletar a história.');
            }

            const currentActivitiesByStory = activitiesByStory.filter(e => e.story._id !== id);
            
            setActivitiesByStory(currentActivitiesByStory);

            toast.success('Sucesso ao deletar a história.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar a história.');
        }

        const finallyCallback = () => {
            setLoadingStories(false);
        }

        ApiStory.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const deleteActivity = (id: string, storyId: string, status: ActivityStatus) => {
        setLoadingStories(true);
        closeConfirmDelete();

        const successCallback = () => {
            if(activitiesByStory === null) {
                return toast.error('Erro ao deletar a atividade.');
            }

            const index = activitiesByStory.findIndex(e => e.story._id === storyId);
            
            activitiesByStory[index][status] = activitiesByStory[index][status].filter(e => e._id !== id);

            setActivitiesByStory([...activitiesByStory]);

            toast.success('Sucesso ao deletar a atividade.');
        }

        const failureCallback = (reason: IReason) => {
            toast.error(`${reason.message}.` || 'Erro ao deletar a atividade.');
        }

        const finallyCallback = () => {
            setLoadingStories(false);
        }

        ApiActivity.Delete(id)
            .then(successCallback, failureCallback)
            .finally(finallyCallback);
    }

    const onChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let name: any = null;

        if(event.target.value !== '' && event.target.value) {
            name = {'$regex' : event.target.value, '$options' : 'i'};
        }

        clearTimeDebounde();
        debounce(() => setFilterName(name), 500)();
    }
    //#endregion

    //#region RENDER METHODS
    const renderActivities = (activities: IActivityList[], storyId: string, sprintId: string, status: ActivityStatus) => {
        return activities.map(activity => {
            return <ActivityCard 
                key={activity._id}
                id={activity._id}
                count={activity.count}
                title={activity.title}
                priority={activity.priority}
                responsible={activity.responsibleName}
                storyId={storyId}
                sprintId={sprintId}
                openConfirmDelete={openConfirmDelete}
                status={status}/>
        });
    }

    const renderRows = () => {
        return activitiesByStory.map(item => {
            return <div key={item.story._id} className={classes.row}>
                <div className={classes.column}>
                    <StoryCard 
                        id={item.story._id} 
                        count={item.story.count}
                        description={item.story.description}
                        priority={item.story.priority}
                        createdBy={item.story.createdByName}
                        countToDo={item.toDo.length}
                        countInProgress={item.inProgress.length} 
                        countToVerify={item.toVerify.length}
                        countDone={item.done.length}
                        sprintId={item.story.sprintId}
                        openConfirmDelete={openConfirmDelete}/>
                </div>
                <div className={classes.column}>
                    {renderActivities(item.toDo, item.story._id, item.story.sprintId, ActivityStatus.TO_DO)}
                </div>
                <div className={classes.column}>
                    {renderActivities(item.inProgress, item.story._id, item.story.sprintId, ActivityStatus.IN_PROGRESS)}
                </div>
                <div className={classes.column}>
                    {renderActivities(item.toVerify, item.story._id, item.story.sprintId, ActivityStatus.TO_VERIFY)}
                </div>
                <div className={classes.column}>
                    {renderActivities(item.done, item.story._id, item.story.sprintId, ActivityStatus.DONE)}
                </div>
            </div>
        });
    }

    const renderActions = () => {

        return (
            <div className={classes.options}>
                <div className={classes.optionsLeft}>
                    <Search placeholder="Buscar atividade" onChange={onChangeSearch} disabled={loadingStories || loadingSprints}/>
                </div>
                <div className={classes.optionsRight}>
                    <InputGroup>
                        <InputGroup.Append>
                            <InputGroup.Text id="basic-addon2">Sprint</InputGroup.Text>
                        </InputGroup.Append>
                        <Form.Control as="select" 
                            value={currentSprintId || ''} 
                            onChange={(event: any) => onChangeSprint(event)} 
                            disabled={loadingStories || loadingSprints}>
                            {sprints.map(sprint => {
                                return <option value={sprint._id} key={sprint._id}>{sprint.name}</option>
                            })}
                        </Form.Control>
                    </InputGroup>
                </div>
            </div>
        );
    }

    const renderBoard = () => {
        return <div className={classes.contentScroll}>
            <div className={classes.content}>
                <div className={classes.header}>
                    <div className={classes.column}>
                        <div className={classes.columnName}>
                            História
                            <Button size="sm" 
                                variant="light" 
                                onClick={newStory} 
                                disabled={
                                    sprints.length === 0 || 
                                    (currentProject === null || 
                                    currentProject.permission === Permission.DEFAULT)
                                }>
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </div>
                    </div>
                    <div className={classes.column}>
                        <div className={classes.columnName}>
                            A Fazer
                        </div>
                    </div>
                    <div className={classes.column}>
                        <div className={classes.columnName}>
                            Em Progresso
                        </div>
                    </div>
                    <div className={classes.column}>
                        <div className={classes.columnName}>
                            Verificar
                        </div>
                    </div>
                    <div className={classes.column}>
                        <div className={classes.columnName}>
                            Feito
                        </div>
                    </div>
                </div>
                {renderBody()}
            </div>
        </div>;
    }

    const render = () => {

        if(sprints === null || sprints.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound message={'Necessário cadastrar uma sprint'}/></div>;
        }

        return renderBoard();
    }

    const renderLoading = () => {
        if(loadingSprints !== true && loadingStories !== true) return;

        return <div className={classes.loading}>
            <Loading/>
        </div>
    }

    const renderBody = () => {
        if(activitiesByStory === null || activitiesByStory.length === 0) {
            return <div className={classes.noDataFound}><NoDataFound/></div>;
        }

        return <div className={classes.body}>
            {renderRows()}
        </div>;
    }

    const openConfirmDelete = (id: string, name: string, storyId?: string, status?: ActivityStatus) => {
        setConfirmDelete({
            id,
            name,
            storyId,
            status
        });
    }

    const closeConfirmDelete = () => {
        setConfirmDelete(null);
    }

    const deleteStoryOrActivity = () => {
        if(confirmDelete === null) {
            return toast.error('Erro ao deletar o registro.');
        }

        if(confirmDelete.storyId && confirmDelete.status) {
            return deleteActivity(confirmDelete.id, confirmDelete.storyId, confirmDelete.status);
        }

        deleteStory(confirmDelete.id);
    }


    //#endregion

    return (
        <div className={classes.root}>
            {renderLoading()}

            {renderActions()}
            {render()}

            <ConfirmDelete 
                show={confirmDelete !== null}
                recordName={confirmDelete != null ? confirmDelete.name : ''} 
                recordId={confirmDelete!= null ? confirmDelete.id : ''} 
                handleClose={closeConfirmDelete} 
                handleConfirm={deleteStoryOrActivity}/>
        </div>
    );
}

export default withStyles(getStyles)(BoardList);