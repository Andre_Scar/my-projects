import { WithStylesProps } from "react-jss";
import { ActivityStatus, Priority } from "../../../../types/Board.types";
import { getStyles } from "./ActivityCard.styles";

export interface IActivityCardProps extends WithStylesProps<typeof getStyles> { 
    id: string;
    count: number;
    title: string;
    priority: Priority;
    responsible: string;
    storyId: string;
    sprintId: string;
    openConfirmDelete: (id: string, name: string, storyId: string, status: ActivityStatus) => void;
    status: ActivityStatus;
}