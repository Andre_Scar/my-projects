import React from 'react';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { DropdownIcon } from '../../../../../../components/DropdownIcon';
import { IDropdownItem } from '../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { Permission } from '../../../../../members/types/Members.types';
import { EDIT_ACTIVITY_PATH, VIEW_ACTIVITY_PATH } from '../../../../routes/Board.routes';
import { ActivityStatus, Priority } from '../../../../types/Board.types';
import { getStyles } from './ActivityCard.styles';
import { IActivityCardProps } from './ActivityCard.types';

const ActivityCard: React.FC<IActivityCardProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        id,
        count,
        title,
        priority,
        responsible,
        storyId,
        sprintId,
        openConfirmDelete,
        status
    } = props;

    let history = useHistory();

    const currentProject = ProjectService.GetCurrentProject();
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const formatId = (id: number) => {
        return `#${id.toString().padStart(4, '0')}`;
    }

    const generateDropdownItems = (id: string, count: number, storyId: string, sprintId: string, status: ActivityStatus) => {

        let options: IDropdownItem[] = [
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => { history.push(`${VIEW_ACTIVITY_PATH}/${storyId}/${sprintId}/${id}`) }
            },
            {
                key: 'edit',
                label: 'Editar',
                onClick: () => { history.push(`${EDIT_ACTIVITY_PATH}/${storyId}/${sprintId}/${id}`) }
            }
        ];

        if(currentProject !== null && currentProject.permission === Permission.MANAGER) {
            options.push({
                key: 'delete',
                label: 'Deletar',
                onClick: () => { openConfirmDelete(id, formatId(count), storyId, status) }
            });
        }

        return options;
    }
    //#endregion

    //#region RENDER METHODS
    const renderLabelPriority = () => {
        switch(priority)
        {
            case Priority.URGENT:
                return 'Urgente';
            case Priority.HIGH:
                return 'Alto';
            case Priority.MEDIUM:
                return 'Médio';
            case Priority.LOW:
                return 'Baixo';
        }
    }
    //#endregion

    return <div className={classes.root} key={id}>
        <div className={classes.header}>
            <div className={classes.id}>{formatId(count)}</div>
            <div className={classes.tagAndActions}>
                <div className={`${classes.tag} ${classes[priority]}`}>{renderLabelPriority()}</div>
                <div className={classes.buttonActions}><DropdownIcon items={generateDropdownItems(id, count, storyId, sprintId, status) || []}/></div>
            </div>
        </div>
        <div className={classes.title}>
            {title}
        </div>
        <div className={classes.responsible}>
            <div className={classes.responsibleLabel}>Responsável:</div> {responsible}
        </div>
    </div>
}

export default withStyles(getStyles)(ActivityCard);