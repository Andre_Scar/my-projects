import { WithStylesProps } from "react-jss";
import { Priority } from "../../../../types/Board.types";
import { getStyles } from "./StoryCard.styles";

export interface IStoryCardProps extends WithStylesProps<typeof getStyles> {
    id: string;
    count: number;
    description: string;
    priority: Priority;
    countToDo: number;
    countInProgress: number;
    countToVerify: number;
    countDone: number;
    createdBy: string;
    sprintId: string;
    openConfirmDelete: (id: string, name: string) => void;
}