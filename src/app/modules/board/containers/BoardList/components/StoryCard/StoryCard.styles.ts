export const getStyles = {
    root: { 
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: '8px',
        marginBottom: '8px',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16);',
    },

    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    
    id: {
        fontWeight: 'bold'
    },

    tagAndActions: {
        display: 'flex',
        flexDirection: 'row',
    },

    buttonActions: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '24px',
        cursor: 'pointer',
        marginLeft: '4px', 
    },

    description: {
        marginTop: '4px',
        marginBottom: '4px',
        maxHeight: '200px',
        overflow: 'auto',
    },

    createdBy: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },

    createdByLabel: {
        fontWeight: 'bold',
        marginRight: '4px',
        whiteSpace: 'nowrap',
    },

    footer: {
        marginTop: '4px'
    },

    tag: {
        width: '56px',
        fontWeight: 'bold',
        padding: '2px 4px',
        fontSize: '12px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '5px',
    },

    urgent: {
        color: '#721C24',
        backgroundColor: '#F8D7DA',
        border: '1px solid #F5C6CB',
    },

    high: {
        color: '#856404',
        backgroundColor: '#fff3cd',
        border: '1px solid #ffeeba',
    },

    medium: {
        color: '#004085',
        backgroundColor: '#cce5ff',
        border: '1px solid #b8daff',
    },

    low: {
        color: '#155724',
        backgroundColor: '#d4edda',
        border: '1px solid #c3e6cb',
    }
};