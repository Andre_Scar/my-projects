import React from 'react';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { DropdownIcon } from '../../../../../../components/DropdownIcon';
import { IDropdownItem } from '../../../../../../components/DropdownIcon/components/DropdownIcon/DropdownIcon.types';
import ProgressBarActivitiesStatus from '../../../../../../components/ProgressBarActivitiesStatus/ProgressBarActivitiesStatus';
import { ProjectService } from '../../../../../../services/currentProject/currentProject.service';
import { Permission } from '../../../../../members/types/Members.types';
import { EDIT_STORY_PATH, NEW_ACTIVITY_PATH, VIEW_STORY_PATH } from '../../../../routes/Board.routes';
import { Priority } from '../../../../types/Board.types';
import { getStyles } from './StoryCard.styles';
import { IStoryCardProps } from './StoryCard.types';

const StoryCard: React.FC<IStoryCardProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        id,
        count,
        description,
        priority,
        countToDo,
        countInProgress,
        countToVerify,
        countDone,
        createdBy,
        sprintId,
        openConfirmDelete
    } = props;

    let history = useHistory();

    const currentProject = ProjectService.GetCurrentProject();


    const countActivities = (countToDo + countInProgress + countToVerify + countDone) || 0;
    const percentageDoing = ((countInProgress / countActivities) * 100) || 0;
    const percentageDone = ((countDone / countActivities) * 100) || 0;
    const percentageVerify = ((countToVerify / countActivities) * 100) || 0;
    const percentageToDo = ((countToDo / countActivities) * 100) || 0;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const formatCount = (id: number) => {
        return `#${id.toString().padStart(4, '0')}`;
    }

    const generateDropdownItems = (id: string, count: number, sprintId: string) => {
        let options: IDropdownItem[] = [
            {
                key: 'newAtivity',
                label: 'Nova atividade',
                onClick: () => { history.push(`${NEW_ACTIVITY_PATH}/${id}/${sprintId}`) }
            },
            {
                key: 'view',
                label: 'Visualizar',
                onClick: () => { history.push(`${VIEW_STORY_PATH}/${sprintId}/${id}`) }
            }
        ];

        if(currentProject !== null && currentProject.permission === Permission.MANAGER) {
            options = options.concat([
                {
                    key: 'edit',
                    label: 'Editar',
                    onClick: () => { history.push(`${EDIT_STORY_PATH}/${sprintId}/${id}`) }
                },
                {
                    key: 'delete',
                    label: 'Deletar',
                    onClick: () => { openConfirmDelete(id, formatCount(count)) }
                }
            ]);
        }

        return options;
    }

    //#endregion

    //#region RENDER METHODS
    const renderLabelPriority = () => {
        switch(priority)
        {
            case Priority.URGENT:
                return 'Urgente';
            case Priority.HIGH:
                return 'Alto';
            case Priority.MEDIUM:
                return 'Médio';
            case Priority.LOW:
                return 'Baixo';
        }
    }
    //#endregion

    return <div className={classes.root} key={id}>
        <div className={classes.header}>
            <div className={classes.id}>{formatCount(count)}</div>
            <div className={classes.tagAndActions}>
                <div className={`${classes.tag} ${classes[priority]}`}>{renderLabelPriority()}</div>
                <div className={classes.buttonActions}><DropdownIcon items={generateDropdownItems(id, count, sprintId) || []}/></div>
            </div>
        </div>
        <div className={classes.description}>
            {description}
        </div>
        <div className={classes.createdBy}>
            <div className={classes.createdByLabel}>Criado por:</div> {createdBy}
        </div>
        <div className={classes.footer}>
            <ProgressBarActivitiesStatus 
                percentageDoing={percentageDoing} 
                percentageDone={percentageDone} 
                percentageVerify={percentageVerify} 
                percentageToDo={percentageToDo}/>
        </div>
    </div>
}

export default withStyles(getStyles)(StoryCard);