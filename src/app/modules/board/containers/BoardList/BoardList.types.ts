import { WithStylesProps } from "react-jss";
import { ActivityStatus } from "../../types/Board.types";
import { getStyles } from "./BoardList.styles";

export interface IBoardListProps extends WithStylesProps<typeof getStyles> { }

export interface ICustomConfirmDelete {
    name: string;
    id: string;
    storyId?: string, 
    status?: ActivityStatus
}