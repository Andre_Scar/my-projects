export const getStyles = {
    root: {
        padding: '32px 32px 0 32px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        position: 'relative',
    },

    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        minWidth: '1098px',
    },

    header: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#F8F9FA',
        fontWeight: 'bold'
    },

    body: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: '24px',
    },

    column: {
        flex: 0.2,
        display: 'flex',
        flexDirection: 'column',
        border: '1px solid #ECEEEF',
        padding: '8px',
        minWidth: '220px',
    },

    columnName: {
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    row: {
        display: 'flex',
        flexDirection: 'row',
    },

    options: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingLeft: '8px',
        paddingRight: '8px',
        marginBottom: '24px',  
    },

    optionsLeft: {
        width: '30%',
    },

    optionsRight: {
        width: '30%',
        display: 'flex',
        justifyContent: 'flex-end',
    },

    noDataFound: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },

    loading: {
        position: 'absolute',
        width: 'calc(100% - 64px)',
        height: 'calc(100% - 64px)',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFFE6',
        zIndex: 1,
    },

    contentScroll: {
        height: '100%',
        overflow: 'auto',
    },


    '@media (max-width: 450px)': { 

        content: {
            minWidth: '1325px',
        },

        contentScroll: {
            height: '100%',
            overflow: 'auto',
        },

        column: {
            minWidth: '265px',
        },

        options: {
            flexWrap: 'wrap',
            flexDirection: 'column-reverse'
        },

        optionsLeft: {
            width: '100%',
        },
    
        optionsRight: {
            marginBottom: '32px',
            width: '100%',
        },

    }
};