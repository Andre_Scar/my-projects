import { ICommon } from "../../../types/Common.types";

export enum ActivityStatus {
    TO_DO = 'toDo',
    IN_PROGRESS = 'inProgress',
    TO_VERIFY = 'toVerify',
    DONE = 'done'
}

export enum Priority {
    URGENT = 'urgent',
    HIGH = 'high',
    MEDIUM = 'medium',
    LOW = 'low'
}

export enum Type {
    FIX = 'fix',
	FEATURE = 'feature',
	CHANGE = 'change',
	REMOVE = 'remove'
}

export interface IStoryURL {
    id: string;
    sprintId: string;
}

export interface IActivityURL {
    id: string;
    storyId: string;
    sprintId: string;
}

export interface IStoryList {
    _id: string;
    count: number;
    description: string;
    priority: Priority;
    createdByName: string;
    sprintId: string;
}

export interface IStoryCreateOrUpdate extends ICommon {
    description: string;
    priority: Priority;
    sprintId: string;
    projectId: string;
}

export interface IActivityList {
    _id: string;
    count: number;
    title: string;
    priority: Priority;
    responsibleName: string;
}

export interface IActivityCreateOrUpdate extends ICommon {
    title: string;
    description?: string | null;
    responsibleId: string;
    typeId: string;
    status: ActivityStatus;
    priority: Priority;
    storyId: string;
    projectId: string;
    customFields: ICustomField[];
}

export interface ICustomField {
    activityCustomFieldId: string;
    valueText?: string | null;
    valueNumber?: number | null;
    valueDate?: string | null;
}

export interface IStoriesWithActivities {
    story: IStoryList;
    toDo: IActivityList[];
    inProgress: IActivityList[];
    toVerify: IActivityList[];
    done: IActivityList[];
}