import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IPageResult, IPaginationQuery } from '../../../types/Common.types';
import { IStoriesWithActivities, IStoryCreateOrUpdate, IStoryList } from '../types/Board.types';


export const STORY_END_POINT: string = `story/v1`;


export class ApiStory {

    static Create = (story: IStoryCreateOrUpdate) => {
        return api.post(`${STORY_END_POINT}`, story);
    }

    static Update = (id: string, story: IStoryCreateOrUpdate) => {
        return api.put(`${STORY_END_POINT}/${id}`, story);
    }

    static FindById = (id: string): Promise<AxiosResponse<IStoryCreateOrUpdate>> => {
        return api.get(`${STORY_END_POINT}/${id}`);
    }

    static List = (query: IPaginationQuery): Promise<AxiosResponse<IPageResult<IStoryList[]>>> => {
        return api.post(`${STORY_END_POINT}/list`, query);
    }

    static Delete = (id: string) => {
        return api.delete(`${STORY_END_POINT}/${id}`);
    }

    static ListWithActivities = (filter: any): Promise<AxiosResponse<IStoriesWithActivities[]>> => {
        return api.post(`${STORY_END_POINT}/list-with-activities`, filter);
    }
}