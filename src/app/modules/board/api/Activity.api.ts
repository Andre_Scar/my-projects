import { AxiosResponse } from 'axios';
import { api } from '../../../api/api';
import { IActivityCreateOrUpdate } from '../types/Board.types';


export const ACTIVITY_END_POINT: string = `activity/v1`;


export class ApiActivity {

    static Create = (activity: IActivityCreateOrUpdate) => {
        return api.post(`${ACTIVITY_END_POINT}`, activity);
    }

    static Update = (id: string, activity: IActivityCreateOrUpdate) => {
        return api.put(`${ACTIVITY_END_POINT}/${id}`, activity);
    }

    static FindById = (id: string): Promise<AxiosResponse<IActivityCreateOrUpdate>> => {
        return api.get(`${ACTIVITY_END_POINT}/${id}`);
    }

    static Delete = (id: string) => {
        return api.delete(`${ACTIVITY_END_POINT}/${id}`);
    }
}