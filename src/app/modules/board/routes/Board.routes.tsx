import React from 'react'
import { Switch } from 'react-router-dom';
import { LayoutType } from '../../../components/Layout/Layout.types';
import Route from '../../../components/Route/Route';
import BoardList from '../containers/BoardList/BoardList.container';
import ActivityForm from '../containers/BoardForm/ActivityForm/ActivityForm.container';
import StoryForm from '../containers/BoardForm/StoryForm/StoryForm.container';


export const BOARD_LIST_PATH = '/board-list';

export const NEW_ACTIVITY_PATH = '/activity/new';
export const EDIT_ACTIVITY_PATH = '/activity/edit';
export const VIEW_ACTIVITY_PATH = '/activity/view';

export const NEW_STORY_PATH = '/story/new';
export const EDIT_STORY_PATH = '/story/edit';
export const VIEW_STORY_PATH = '/story/view';

const BoardListRoutes: React.FC = () => {

    return (
        <>
            <Switch>
                <Route exact={true} path={BOARD_LIST_PATH} component={BoardList} layoutType={LayoutType.NAVBAR_AND_MENU} isPrivateRoute={true}/>

                <Route exact={true} path={`${NEW_ACTIVITY_PATH}/:storyId/:sprintId`} component={ActivityForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_ACTIVITY_PATH}/:storyId/:sprintId/:id`} component={ActivityForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_ACTIVITY_PATH}/:storyId/:sprintId/:id`} component={ActivityForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>

                <Route exact={true} path={`${NEW_STORY_PATH}/:sprintId`} component={StoryForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${EDIT_STORY_PATH}/:sprintId/:id`} component={StoryForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
                <Route exact={true} path={`${VIEW_STORY_PATH}/:sprintId/:id`} component={StoryForm} layoutType={LayoutType.ONLY_NAVBAR} isPrivateRoute={true}/>
            </Switch>
        </>
    )
}

export default BoardListRoutes;