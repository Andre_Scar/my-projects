import { Permission } from "../../modules/members/types/Members.types";

export interface ICurrentProject {
    id?: string;
    name?: string;
    permission: Permission
}