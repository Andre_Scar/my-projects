import { ICurrentProject } from "./currentProject.types";


export class ProjectService {

    private static CURRENT_PROJECT_KEY = 'MY_PROJECT:CURRENT_PROJECT';

    static SetCurrentProject(currentProject: ICurrentProject) {
        localStorage.setItem(this.CURRENT_PROJECT_KEY, JSON.stringify(currentProject));
    }

    static ClearCurrentProject() {
        localStorage.removeItem(this.CURRENT_PROJECT_KEY);
    }

    static GetCurrentProject(): ICurrentProject | null {
        let currentProject = localStorage.getItem(this.CURRENT_PROJECT_KEY);
        
        if(currentProject){
            return JSON.parse(currentProject);
        }

        return null;
    }
}