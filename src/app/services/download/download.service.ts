
export class DownloadService {

    static DownloadCSV(fileData: string, fileName: string) {
        if(!fileData || fileData === '' || !fileName || fileName === '') {
            return;
        }
        
        var blob = new Blob([fileData], {type: 'text/csv;charset=windows-1252;'});
        if (window && window.navigator.msSaveOrOpenBlob)
            window.navigator.msSaveBlob(blob, fileName);
        else
        {
            var link = window.document.createElement("a");
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            document.body.appendChild(link);
            link.click(); 
            document.body.removeChild(link);
        }
    }

}