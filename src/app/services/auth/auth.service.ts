import { ApiAuth } from "../../modules/login/api/Login.api";
import { IAuthenticate } from "../../modules/login/types/auth.types";
import { ILoggedUser } from "../../types/Common.types";
import { ProjectService } from "../currentProject/currentProject.service";



export class AuthService {

    private static TOKEN_KEY = 'MY_PROJECT:TOKEN_KEY';
    private static USER_KEY = 'MY_PROJECT:USER_KEY';

    static async Login(authenticate: IAuthenticate) {
        const result = await ApiAuth.Authenticate(authenticate);
    
        const { token, user } = result.data;

        this.SetToken(token);
        this.SetUser(user._id || '', user.name || '');
    }

    static async LoginGoogle(tokenId: string) {
        const result = await ApiAuth.Google(tokenId);
    
        const { token, user } = result.data;

        this.SetToken(token);
        this.SetUser(user._id || '', user.name || '');
    }

    static async Logout() {
        this.ClearToken();
        this.ClearUser();
        ProjectService.ClearCurrentProject();
    }

    static IsLoggedIn(): boolean {
        const token = this.GetToken();

        return token != null;
    }

    //#region Token 
    static SetToken(token: string) {
        localStorage.setItem(this.TOKEN_KEY, token);
    }

    static GetToken(): string | null {
        return localStorage.getItem(this.TOKEN_KEY);
    }

    static ClearToken() {
        localStorage.removeItem(this.TOKEN_KEY);
    }
    //#endregion

    //#region User 
    static SetUser(_id: string, name: string) {
        localStorage.setItem(this.USER_KEY, JSON.stringify({
            _id,
            name
        }));
    }

    static GetUser(): ILoggedUser | null {
        const user = localStorage.getItem(this.USER_KEY);

        return user != null ? JSON.parse(user) : null;
    }

    static ClearUser() {
        localStorage.removeItem(this.USER_KEY);
    }
    //#endregion
}
