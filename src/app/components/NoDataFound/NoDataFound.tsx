import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './NoDataFound.styles';
import { INoDataFoundProps } from './NoDataFound.types';


const NoDataFound: React.FC<INoDataFoundProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        message
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderMessage = () => {
        if(!message) {
            return 'Nenhum dado encontrado';
        }

        return message;
    }
    //#endregion

    return <div className={classes.root}>
        <div className={classes.label}>
            {renderMessage()}
        </div>
    </div>
}

export default withStyles(getStyles)(NoDataFound);