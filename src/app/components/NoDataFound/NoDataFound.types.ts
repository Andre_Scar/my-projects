import { WithStylesProps } from "react-jss";
import { getStyles } from "./NoDataFound.styles";

export interface INoDataFoundProps extends WithStylesProps<typeof getStyles> {
    message?: string;
}