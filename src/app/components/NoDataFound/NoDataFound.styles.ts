export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9E9E9E0D',
    },
    
    label: {
        fontSize: '24px',
        marginTop: '8px',
    }
};