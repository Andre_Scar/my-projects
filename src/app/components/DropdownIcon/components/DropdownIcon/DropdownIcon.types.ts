import { WithStylesProps } from "react-jss";
import { getStyles } from "./DropdownIcon.styles";

export interface IDropdownIconProps extends WithStylesProps<typeof getStyles> {
    items: IDropdownItem[];
    disabled?: boolean;
}

export interface IDropdownItem {
    key: string;
    label: string;
    onClick: () => void;
}