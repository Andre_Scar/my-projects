import React from 'react';
import { Dropdown } from 'react-bootstrap';
import withStyles from 'react-jss';
import CustomToggle from '../CustomToggle/CustomToggle';
import { getStyles } from './DropdownIcon.styles';
import { IDropdownIconProps } from './DropdownIcon.types';

const DropdownIcon: React.FC<IDropdownIconProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        items,
        disabled
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderDropdownItems = () => {
        return items.map(item => {
            return <Dropdown.Item className={classes.dropdownItem} key={item.key} onClick={item.onClick}>{item.label}</Dropdown.Item>
        });
    }

    //#endregion

    return <Dropdown className={`${classes.root} ${disabled ? classes.disabled : ''}`}>
        <Dropdown.Toggle split variant="success" as={CustomToggle}/>
    
        <Dropdown.Menu>
            {renderDropdownItems()}
        </Dropdown.Menu>
    </Dropdown>
}

export default withStyles(getStyles)(DropdownIcon);