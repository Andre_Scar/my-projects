export const getStyles = {
    root: { 
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    disabled: {
        pointerEvents: 'none',
        opacity: 0.6,
    },

    dropdownItem: {
        '&:active': {
            backgroundColor: '#DAE0E5',
            color: '#212529',
            userSelect: 'none',
        },

        '& > svg': {
            marginRight: '8px',
        }
    }
};