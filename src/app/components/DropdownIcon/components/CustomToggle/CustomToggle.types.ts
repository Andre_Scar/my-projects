import { WithStylesProps } from "react-jss";
import { getStyles } from "./CustomToggle.styles";

export interface ICustomToggleProps extends WithStylesProps<typeof getStyles> {
    onClick: (e: any) => void;
}

export type Ref = HTMLDivElement;