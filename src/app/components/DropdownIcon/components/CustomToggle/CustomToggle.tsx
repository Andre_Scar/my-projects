import React from 'react';
import { getStyles } from './CustomToggle.styles';
import { Ref, ICustomToggleProps } from './CustomToggle.types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import withStyles from 'react-jss';


const CustomToggle = React.forwardRef<Ref, ICustomToggleProps>((props, ref) => {

    //#region CONSTS
    const {
        classes,
        onClick
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root} ref={ref} onClick={(e) => {onClick(e);}}>
        <FontAwesomeIcon icon={faEllipsisV}/>
    </div>
});


export default withStyles(getStyles)(CustomToggle);
