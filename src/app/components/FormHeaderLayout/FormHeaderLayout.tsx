import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './FormHeaderLayout.styles';
import { IFormHeaderLayoutProps } from './FormHeaderLayout.types';


const FormHeaderLayout: React.FC<IFormHeaderLayoutProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        title,
        children
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root}>
        <div className={classes.title}>
            {title}
        </div>
        <div className={classes.actions}>
            {children}
        </div>
    </div>
}

export default withStyles(getStyles)(FormHeaderLayout);