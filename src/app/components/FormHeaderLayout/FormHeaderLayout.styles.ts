export const getStyles = {

    root: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: '32px',
    },

    title: {
        fontSize: '32px',
    },
    
    actions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',

        '& > button': {
            marginLeft: '8px',
        }
    },

    '@media (max-width: 450px)': { 

        root: {
            flexWrap: 'wrap'
        },

        title: {
            width: '100%',
        },
    
        actions: {
            marginTop: '32px',
            width: '100%',
        },

    }
};