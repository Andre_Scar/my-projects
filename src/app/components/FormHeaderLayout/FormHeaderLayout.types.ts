import { WithStylesProps } from "react-jss";
import { getStyles } from "./FormHeaderLayout.styles";

export interface IFormHeaderLayoutProps extends WithStylesProps<typeof getStyles> {
    title: string;
}