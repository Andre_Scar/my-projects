import { ChangeEvent, RefObject } from "react";
import { WithStylesProps } from "react-jss";
import { getStyles } from "./Search.styles";

export interface ISearchProps extends WithStylesProps<typeof getStyles> { 
    placeholder?: string;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
    disabled?: boolean;
    inputRef?:  RefObject<HTMLInputElement>;
}