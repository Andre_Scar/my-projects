import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Search.styles';
import { ISearchProps } from './Search.types';
import { InputGroup } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faSearch } from "@fortawesome/free-solid-svg-icons";

const Search: React.FC<ISearchProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        placeholder,
        onChange,
        disabled,
        inputRef
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root}>
        <InputGroup>
            <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-sm">
                    <FontAwesomeIcon icon={faSearch}/>
                </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl ref={inputRef} placeholder={placeholder} aria-describedby="inputGroup-sizing-sm" onChange={onChange} disabled={disabled}/>
        </InputGroup>
    </div>
}

export default withStyles(getStyles)(Search);