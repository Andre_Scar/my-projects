export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    icon: {
        marginRight: '16px',
        height: '100%'
    },
    
    name: {
        fontWeight: '400',
        userSelect: 'none',
        whiteSpace: 'nowrap'
    }
};