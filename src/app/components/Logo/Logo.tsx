import React from 'react';
import withStyles from 'react-jss';
import ImageViwer from '../ImageViewer/ImageViwer';
import { getStyles } from './Logo.styles';
import { ILogoProps } from './Logo.types';

import icon_my_projects from '../../assets/icon_my_projects.png';

const Logo: React.FC<ILogoProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        height,
        onclick
    } = props;

    const convertedHeight = parseFloat(height);
    const fontSize = convertedHeight - 7;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={`${classes.root} ${className}`}
        style={{height: height, cursor: onclick? 'pointer': 'auto'}} 
        onClick={onclick} >

        <div className={classes.icon}>
            <ImageViwer src={icon_my_projects} height={height}/>
        </div>
        <div className={`${classes.name} label`} style={{fontSize: fontSize}}>
            My Projects
        </div>
    </div>
}

export default withStyles(getStyles)(Logo);