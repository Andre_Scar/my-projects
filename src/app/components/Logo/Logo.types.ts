import { WithStylesProps } from "react-jss";
import { getStyles } from "./Logo.styles";

export interface ILogoProps extends WithStylesProps<typeof getStyles> {
    className?: string; 
    height: string;
    onclick?: () => void;
}