import { WithStylesProps } from "react-jss";
import { getStyles } from "./Divider.styles";

export interface IDividerProps extends WithStylesProps<typeof getStyles> {
    className?: string; 
    label?: string;
}