export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '4px',

        '& > hr': {
            flex: 'auto'
        }
    },

    label: {
        marginLeft: '8px',
        marginRight: '8px'
    }
};