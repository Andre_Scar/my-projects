import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Divider.styles';
import { IDividerProps } from './Divider.types';

const Divider: React.FC<IDividerProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        label
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return ( 
        <div className={`${classes.root} ${className}`}>
            <hr/>
            {
                label && <>
                    <span className={classes.label}>{label}</span>
                    <hr/>
                </>
            }
        </div>
    );
}

export default withStyles(getStyles)(Divider);