import React from 'react';
import { Tabs as  BTabs} from 'react-bootstrap';
import withStyles from 'react-jss';
import { getStyles } from './Tabs.styles';
import { ITabsProps } from './Tabs.types';

const Tabs: React.FC<ITabsProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        transition,
        children,
        activeKey,
        onSelect,
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS

    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={`${classes.root} ${className}`}>
        <BTabs transition={transition} activeKey={activeKey} onSelect={(k) => onSelect ? onSelect(k) : null }>
            {children}
        </BTabs>
    </div>;
}

export default withStyles(getStyles)(Tabs);