export const getStyles = {

    root: {

        width: '100%',
        maxWidth: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        paddingTop: '32px',

        '& > .nav-tabs': {
            paddingLeft: '32px',
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
        },

        '& > .nav-tabs > a': {
            whiteSpace: 'nowrap',
        },

        '& > .tab-content': {
            flex: 1,
            overflow: 'auto',
        },

        '& > .tab-content > div[role="tabpanel"]': {
            height: '100%',
        },
        
        '& > .nav-tabs[role="tablist"]': {
            border: 0,
        },

        '& > .nav-tabs[role="tablist"] > .nav-item': {
            border: 0,
            color: '#212529',
            userSelect: 'none',
        },

        '& > .nav-tabs[role="tablist"] > .nav-item:focus': {
            outline: 'none',
        },

        '& > .nav-tabs[role="tablist"] > .nav-item.active': {
            borderBottom: '2px solid',
            fontWeight: 'bold',
            zIndex: 0
        }
    },

    '@media (max-width: 450px)': { 

        root: {
            '& > .nav-tabs': {
                paddingLeft: 0,
                overflowX: 'auto',
                overflowY: 'hidden',
            },
        }

    }

};