import { WithStylesProps } from "react-jss";
import { getStyles } from "./Tabs.styles";

export interface ITabsProps extends WithStylesProps<typeof getStyles> { 
    className?: string;
    transition?: boolean;
    activeKey?: string;
    onSelect?: (k: string | null) => void;
}