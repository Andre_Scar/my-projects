export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        paddingLeft: '6px',
        paddingRight: '6px',
    },

    logo: {
        display: 'flex',
        background: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
        width: '24px',
        height: '24px',
        borderRadius: '5px',
        marginRight: '8px',
    },

    label: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: '24px',
    }
};