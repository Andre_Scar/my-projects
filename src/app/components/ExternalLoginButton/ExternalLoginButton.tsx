import React from 'react';
import { Button } from 'react-bootstrap';
import withStyles from 'react-jss';
import ImageViwer from '../ImageViewer/ImageViwer';
import { getStyles } from './ExternalLoginButton.styles';
import { ExternalLoginType, IExternalLoginButtonProps } from './ExternalLoginButton.types';

import google from '../../assets/google.png';

const ExternalLoginButton: React.FC<IExternalLoginButtonProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        label,
        type,
        onClick,
        disabled
    } = props;
    // const fontSize = heightNumber - (heightNumber * 0.25);

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderLogoType = () => {
        switch(type) {
            case ExternalLoginType.GOOGLE: 
                return <div className={classes.logo}>
                    <ImageViwer src={google} height="16px" width="16px"/>
                </div>
        }
    }

    const renderLabel = () => {
        return <div className={classes.label}>
            {label}
        </div>
    }
    //#endregion

    return <Button className={`${classes.root} ${className}`} variant="primary" onClick={onClick} disabled={disabled}>
        {renderLogoType()} 
        {renderLabel()}
    </Button>
}

export default withStyles(getStyles)(ExternalLoginButton);