import { WithStylesProps } from "react-jss";
import { getStyles } from "./ExternalLoginButton.styles";

export interface IExternalLoginButtonProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    label: string;
    type: ExternalLoginType;
    onClick?: () => void;
    disabled?: boolean;
}

export enum ExternalLoginType {
    GOOGLE = 'google'
}

export interface resultGoogle {
    tokenId: string;
}