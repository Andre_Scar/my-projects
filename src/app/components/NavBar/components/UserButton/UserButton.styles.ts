export const getStyles = {
    
    root: { 
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },

    userImage: {
        marginRight: '16px',
    },

    label: {
        userSelect: 'none',
        marginRight: '8px',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        width: '100px',
    },

    dropdownItem: {
        '&:active': {
            backgroundColor: '#DAE0E5',
            color: '#212529',
            userSelect: 'none',
        },

        '& > svg': {
            marginRight: '8px',
        }
    }
};