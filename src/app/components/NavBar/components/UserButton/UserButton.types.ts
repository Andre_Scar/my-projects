import { WithStylesProps } from "react-jss";
import { getStyles } from "./UserButton.styles";

export interface IUserButtonProps extends WithStylesProps<typeof getStyles> {
    label: string;
    imageSrc: string;
}
