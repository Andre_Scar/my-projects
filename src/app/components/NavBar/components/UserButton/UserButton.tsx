import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './UserButton.styles';
import { IUserButtonProps } from './UserButton.types';
import { Dropdown } from 'react-bootstrap';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAddressCard } from "@fortawesome/free-regular-svg-icons";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from '../../../../services/auth/auth.service';
import { useHistory } from 'react-router-dom';
import { LOGIN_PATH } from '../../../../modules/login/routes/Login.routes';
import { PROFILE_PATH } from '../../../../modules/profile/routes/Profile.routes';

const UserButton: React.FC<IUserButtonProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        label
    } = props;

    let history = useHistory();

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const logout = () => {
        AuthService.Logout();
        history.push(LOGIN_PATH);
    }

    const profile = () => {
        history.push(PROFILE_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <Dropdown alignRight>
            <Dropdown.Toggle variant="light" id="dropdown-basic" className={classes.root}>
                <div className={classes.label}>
                    {label}
                </div>
            </Dropdown.Toggle>
        
            <Dropdown.Menu>
                <Dropdown.Item onClick={profile} className={classes.dropdownItem}>
                    <FontAwesomeIcon icon={faAddressCard}/>
                    Perfil
                </Dropdown.Item>
                <Dropdown.Item onClick={logout} className={classes.dropdownItem}>
                    <FontAwesomeIcon icon={faSignOutAlt}/>
                    Sair
                </Dropdown.Item>
            </Dropdown.Menu>
      </Dropdown>
    );
}

export default withStyles(getStyles)(UserButton);