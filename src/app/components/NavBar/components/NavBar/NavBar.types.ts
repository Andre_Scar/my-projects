import { WithStylesProps } from "react-jss";
import { getStyles } from "./NavBar.styles";

export interface INavBarProps extends WithStylesProps<typeof getStyles> {
    hiddenLogo?: boolean;
    menuButton?: IMenuButton;
}

export interface IMenuButton {
    hidden?: boolean;
    onclick?: () => void;
}