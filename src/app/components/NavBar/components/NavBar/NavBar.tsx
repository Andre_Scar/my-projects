import React, { useEffect, useState } from 'react';
import { Button, Navbar } from 'react-bootstrap';
import withStyles from 'react-jss';
import { getStyles } from './NavBar.styles';
import { INavBarProps } from './NavBar.types';
import Logo from '../../../Logo/Logo';
import { useHistory } from 'react-router-dom';
import { OverlayTrigger } from 'react-bootstrap';
import { Tooltip } from 'react-bootstrap';
import { ProjectService } from '../../../../services/currentProject/currentProject.service';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import user from '../../../../assets/user.png';
import UserButton from '../UserButton/UserButton';
import { PROJECTS_PATH } from '../../../../modules/projects/routes/Projects.routes';

import { faBars, faExchangeAlt } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from '../../../../services/auth/auth.service';


const NavBar: React.FC<INavBarProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        hiddenLogo,
        menuButton
    } = props;

    let history = useHistory();
    
    const [username, setUsername] = useState<string>('');
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {

        const user = AuthService.GetUser();
        setUsername(user != null ? user.name : 'userName');
    }, []);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const goToProjects = () => {
        ProjectService.ClearCurrentProject();

        history.push(PROJECTS_PATH);
    }
    //#endregion

    //#region RENDER METHODS
    const renderProjectName = () => {
        let currentProject = ProjectService.GetCurrentProject()
        
        if(currentProject == null || !currentProject.name || history.location.pathname.indexOf(PROJECTS_PATH) > -1) return;

        return (
            <div className={`${classes.contentCenter} mobileContentCenter`}>
                <OverlayTrigger 
                    placement={'bottom'}
                    transition={false}
                    overlay={
                        <Tooltip id={`change-project`}>
                            Clique para selecionar outro projeto  
                        </Tooltip>
                    }>
                    <Button variant="light" onClick={goToProjects}>
                        {currentProject.name}

                        <FontAwesomeIcon className={classes.iconChangeProject} icon={faExchangeAlt}/>
                    </Button>
                </OverlayTrigger>
            </div>
            
        );
    }

    const renderLogo = () => {
        if(hiddenLogo) return;

        return <Logo className={classes.logo} onclick={goToProjects} height={'32px'}/>
    }

    const renderMenuButton = () => {
        if(menuButton && menuButton.hidden) return;

        return (
            <Button variant="light" onClick={menuButton?.onclick}>
                <FontAwesomeIcon icon={faBars} />
            </Button>
        );
    }
    //#endregion

    return (
        <Navbar className={classes.root} bg="light">
            <div className={classes.contentLeft}>
                {renderLogo()}
                {renderMenuButton()}
            </div>

            {renderProjectName()}

            <div className={`${classes.contentRight} mobileContentRight`}>
                <UserButton imageSrc={user} label={username}/>
            </div>
        </Navbar>
    );
}

export default withStyles(getStyles)(NavBar);