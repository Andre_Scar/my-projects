export const getStyles = {
    
    root: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: '56px',
        padding: '0 32px',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16);',
        zIndex: 2
    },

    contentLeft: {
        flex: 0.3,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
    },
    
    contentCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        fontSize: '20px',
        fontWeight: 400,
        userSelect: 'none',
    },

    contentRight: {
        flex: 0.3,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '100%',
        maxWidth: '100%',
    },

    userImage: {
        marginRight: '16px',
    },
    
    logo: {
    },

    iconChangeProject: {
        marginLeft: '8px',
    },

    '@media (max-width: 450px)': { 
        contentRight: {
            flex: 0.5,
        },

        contentLeft: {
            flex: 0.5,
            marginRight: '4px',
        },

        root: {
            '& .mobileContentCenter + .mobileContentRight': {
                display: 'none',
            }
        }
    },

    '@media (max-width: 380px)': { 
        logo: {
            '& > .label': {
                display: 'none',
            }
        },
    }
};