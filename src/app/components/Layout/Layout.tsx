import React, { useState } from 'react';
import withStyles from 'react-jss';
import { Menu } from '../Menu';
import { NavBar } from '../NavBar';
import { getStyles } from './Layout.styles';
import { ILayoutProps, LayoutType } from './Layout.types';


const Layout: React.FC<ILayoutProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        type,
        children
    } = props;

    const currentType = type || LayoutType.BLANK;
    const [hiddenMenu, setHiddenMenu] = useState<boolean>(false);
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const toggleMenu = () => {
        setHiddenMenu(!hiddenMenu);
    }
    //#endregion

    //#region RENDER METHODS
    const renderNavBar = () => {
        if(currentType !== LayoutType.ONLY_NAVBAR && currentType !== LayoutType.NAVBAR_AND_MENU) {
            return;
        }

        return <NavBar 
            hiddenLogo={currentType === LayoutType.NAVBAR_AND_MENU} 
            menuButton={{
                hidden: currentType !== LayoutType.NAVBAR_AND_MENU,
                onclick: toggleMenu
            }}/>;
    }

    const renderMenu = () => {
        return <Menu 
            hidden={
                (
                    currentType !== LayoutType.ONLY_MENU && 
                    currentType !== LayoutType.NAVBAR_AND_MENU
                ) || hiddenMenu
            }
            toggleMenu={() => toggleMenu()}/>;
    }

    const renderChildrenContainer = () => {
        if(currentType === LayoutType.BLANK) {
            return children;
        }

        return <div className={classes.childrenContainer}>
            {children}
        </div>;
    }

    const renderContainer = () => {
        if(currentType === LayoutType.BLANK) {
            return renderChildrenContainer();
        }

        return <div className={`${classes.container} test`}>
            {renderNavBar()}
            {renderChildrenContainer()}
        </div>;
    }
    //#endregion

    return (
        <div className={classes.root}>
            {renderMenu()}
            {renderContainer()}
        </div>
    );
}

export default withStyles(getStyles)(Layout);