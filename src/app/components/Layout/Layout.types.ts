import { WithStylesProps } from "react-jss";
import { getStyles } from "./Layout.styles";

export interface ILayoutProps extends WithStylesProps<typeof getStyles> {
    type?: LayoutType,
}

export enum LayoutType {
    BLANK = 'blank',
    ONLY_NAVBAR = 'onlyNavbar',
    ONLY_MENU = 'onlyMenu',
    NAVBAR_AND_MENU = 'navbarAndMenu'
}