export const getStyles = {
    
    root: { 
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',

        '& .menu + .test': {
            width: 'calc(100% - 254px)'
        }
    },

    childrenContainer: {
        display: 'flex',
        flexDirection: 'column',
        height: 'calc(100% - 56px)',
        position: 'relative',
    },

    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
};