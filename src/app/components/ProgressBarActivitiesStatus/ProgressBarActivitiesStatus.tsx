import React from 'react';
import { Tooltip } from 'react-bootstrap';
import { OverlayTrigger } from 'react-bootstrap';
import withStyles from 'react-jss';
import { getStyles } from './ProgressBarActivitiesStatus.styles';
import { IProgressBarActivitiesStatusProps } from './ProgressBarActivitiesStatus.types';

const ProgressBarActivitiesStatus: React.FC<IProgressBarActivitiesStatusProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        percentageDoing,
        percentageDone,
        percentageVerify,
        percentageToDo
    } = props;

    const status = [
        {
            statusLabel: `Em Progresso:`,
            percentageLabel: `${percentageDoing.toFixed(2)}% das atividades`,
            percentage: percentageDoing,
            color: '#007BFF'
        },
        {
            statusLabel: `Verificar:`,
            percentageLabel: `${percentageVerify.toFixed(2)}% das atividades`,
            percentage: percentageVerify,
            color: '#FFC107'
        },
        {
            statusLabel: `Feito:`,
            percentageLabel: `${percentageDone.toFixed(2)}% das atividades`,
            percentage: percentageDone,
            color: '#28A745'
        },
        {
            statusLabel: `A Fazer:`,
            percentageLabel: `${percentageToDo.toFixed(2)}% das atividades`,
            percentage: percentageToDo,
            color: '#17A2B8'
        }
    ]
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderProgressBarStatus = () => {
        return status.map(item => {
            if(item.percentage === 0){
                return <></>;
            }

            return <OverlayTrigger 
                transition={false}
                overlay={
                    <Tooltip id={`progress-bar-status`}>
                        <div>{item.statusLabel}</div> 
                        <div>{item.percentageLabel}</div> 
                    </Tooltip>
                }>
                <div className={classes.progressBar} style={{backgroundColor: item.color, width: `${item.percentage}%`}} />
            </OverlayTrigger>
        });
    }
    //#endregion

    return <div className={classes.root}>
        {renderProgressBarStatus()}
    </div>
}

export default withStyles(getStyles)(ProgressBarActivitiesStatus);