import { WithStylesProps } from "react-jss";
import { getStyles } from "./ProgressBarActivitiesStatus.styles";

export interface IProgressBarActivitiesStatusProps extends WithStylesProps<typeof getStyles> {
    percentageDoing: number;
    percentageDone: number;
    percentageVerify: number;
    percentageToDo: number;
}