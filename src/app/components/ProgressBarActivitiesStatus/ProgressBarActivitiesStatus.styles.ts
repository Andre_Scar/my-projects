export const getStyles = {
    root: { 
        height: '16px',
        with: '100%',
        backgroundColor: '#E9ECEF',
        display: 'flex',
        flexDirection: 'row',
        borderRadius: '5px'
    },

    progressBar: {
        height: '16px',
    }
};