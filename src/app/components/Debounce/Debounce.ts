let timer: NodeJS.Timeout

export function debounce<Params extends any[]>
    (func: (...args: Params) => any, timeout: number) : (...args: Params) => void {

    return (...args: Params) => {
        clearTimeout(timer)

        timer = setTimeout(() => {
            func(...args)
        }, timeout)
    }
}

export function clearTimeDebounde(){
    clearTimeout(timer)
}