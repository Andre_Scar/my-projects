export const getStyles = {
    
    root: { 
        display: 'flex',
        flexDirection: 'column',
        width: '254px',
        minWidth: '254px',
        height: '100%',
        backgroundColor: '#F8F9FA',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16);',
        zIndex: 100,
    },

    logo: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '56px',
    },

    options: {
        marginTop: '40px',  
    },

    toggleMenu: {
        display: 'none'
    },

    '@media (max-width: 450px)': { 

        root: { 
            position: 'absolute',
        },

        logo: {
            paddingLeft: '8px',
            paddingRight: '4px',
            justifyContent: 'space-between',
        },

        toggleMenu: {
            display: 'block'
        }
    }
};