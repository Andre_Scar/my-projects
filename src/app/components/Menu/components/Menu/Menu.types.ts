import { WithStylesProps } from "react-jss";
import { getStyles } from "./Menu.styles";

export interface IMenuProps extends WithStylesProps<typeof getStyles> { 
    hidden?: boolean;
    hiddenLogo?: boolean;
    toggleMenu: () => void;
}
