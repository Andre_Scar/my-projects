import React from 'react';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import Logo from '../../../Logo/Logo';
import Item from '../Item/Item';
import { getStyles } from './Menu.styles';
import { IMenuProps } from './Menu.types';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { BOARD_LIST_PATH } from '../../../../modules/board/routes/Board.routes';
import { SETTINGS_PATH } from '../../../../modules/settings/routes/Settings.routes';
import { SPRINTS_PATH } from '../../../../modules/sprints/routes/Sprints.routes';
import { DASHBOARD_PATH } from '../../../../modules/dashboard/routes/Dashboard.routes';

import { faTachometerAlt, faTable, faThList, faUsers, faCogs, faBars } from "@fortawesome/free-solid-svg-icons";
import { MEMBERS_PATH } from '../../../../modules/members/routes/Members.routes';
import { Button } from 'react-bootstrap';


const Menu: React.FC<IMenuProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        hidden,
        toggleMenu
    } = props;

    let history = useHistory();

    const menuOptions = [
        {
            icon: faTachometerAlt,
            label: 'Dashboard',
            path: DASHBOARD_PATH
        },
        {
            icon: faTable,
            label: 'Sprints',
            path: SPRINTS_PATH
        },
        {
            icon: faThList,
            label: 'Board',
            path: BOARD_LIST_PATH
        },
        {
            icon: faUsers,
            label: 'Membros',
            path: MEMBERS_PATH
        },
        {
            icon: faCogs,
            label: 'Configurações',
            path: SETTINGS_PATH
        }
    ];
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const goToProjects = () => {
        history.push("/projects");
    }
    //#endregion

    //#region RENDER METHODS
    const renderItems = () => {
        let items = [];

        for(var i = 0; i < menuOptions.length; i++) {
            const option = menuOptions[i];
            items.push(<Item key={i} label={option.label} icon={option.icon} path={option.path}/>)
        }

        return items;
    }

    const render = () => {
        if(hidden) return;

        return (
            <div className={`${classes.root} menu`}>
                <div className={classes.logo}>
                    <Logo onclick={goToProjects} height={'32px'}/>
                    <Button className={classes.toggleMenu} variant="light" onClick={toggleMenu}>
                        <FontAwesomeIcon icon={faBars} />
                    </Button>
                </div>
                <div className={classes.options}>
                    {renderItems()}
                </div>
            </div>
        );
    }
    //#endregion

    return <>{render()}</>;
}

export default withStyles(getStyles)(Menu);