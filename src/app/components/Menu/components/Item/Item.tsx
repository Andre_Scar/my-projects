import React from 'react';
import withStyles from 'react-jss';
import { useHistory } from 'react-router-dom';
import { getStyles } from './Item.styles';
import { IItemProps } from './Item.types';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Item: React.FC<IItemProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        icon,
        label,
        path
    } = props;

    let history = useHistory();
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const goTo = () => {
        history.push(path);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={`${classes.root} ${history.location.pathname.indexOf(path) > -1 ? classes.active : ''}`} onClick={goTo}>
            <div className={classes.icon}>
                <FontAwesomeIcon icon={icon} />
            </div>
            <div className={classes.label}>
                {label}
            </div>
        </div>
    );
}

export default withStyles(getStyles)(Item);