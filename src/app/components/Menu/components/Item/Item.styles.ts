export const getStyles = {
    
    root: { 
        height: '48px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: '16px',
        paddingRight: '16px',
        cursor: 'pointer',
        marginBottom: '1px',
        
        
        '&:hover': {
            backgroundColor: '#E2E6EA'
        }
    },

    icon: {
        marginRight: '16px',
        
    },

    label: {

    },

    active: {
        backgroundColor: '#E2E6EA',
        borderColor: '#DAE0E5',
    }

};