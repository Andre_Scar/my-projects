import { WithStylesProps } from "react-jss";
import { getStyles } from "./Item.styles";

export interface IItemProps extends WithStylesProps<typeof getStyles> { 
    icon: any;
    label: string;
    path: string;
}
