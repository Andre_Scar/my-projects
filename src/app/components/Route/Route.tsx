import React from 'react';
import { Route as ReactRoute, useHistory } from 'react-router-dom';
import withStyles from 'react-jss';
import { getStyles } from './Route.styles';
import { IRouteProps } from './Route.types';
import Layout from '../Layout/Layout';
import { AuthService } from '../../services/auth/auth.service';
import { LOGIN_PATH } from '../../modules/login/routes/Login.routes';


const Route: React.FC<IRouteProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        component,
        path,
        exact,
        layoutType,
        isPrivateRoute
    } = props;

    let history = useHistory();

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const privateRoute = () => {
        if(AuthService.IsLoggedIn() || !isPrivateRoute) {
            return <ReactRoute exact={exact} path={path} component={component}/>
        }

        history.push(LOGIN_PATH);
    }
    //#endregion

    return (
        <div className={classes.root}>
            <Layout type={layoutType}>
                {privateRoute()}
            </Layout>
        </div>
    );
}

export default withStyles(getStyles)(Route);