import { WithStylesProps } from "react-jss";
import { LayoutType } from "../Layout/Layout.types";
import { getStyles } from "./Route.styles";

export interface IRouteProps extends WithStylesProps<typeof getStyles> {
    component: any;
    path: string;
    exact?: boolean;
    layoutType?: LayoutType;
    isPrivateRoute?: boolean;
}