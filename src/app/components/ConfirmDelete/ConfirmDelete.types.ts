import { WithStylesProps } from "react-jss";
import { getStyles } from "./ConfirmDelete.styles";

export interface IConfirmDeleteProps extends WithStylesProps<typeof getStyles> {
    recordName: string;
    recordId: string;
    show?: boolean;
    handleClose: () => void;
    handleConfirm: (id: string) => void;
}

export interface IConfirmDelete {
    name: string;
    id: string;
}