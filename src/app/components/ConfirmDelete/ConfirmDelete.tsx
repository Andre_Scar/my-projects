import React, { ChangeEvent, useEffect, useState } from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ConfirmDelete.styles';
import { IConfirmDeleteProps } from './ConfirmDelete.types';
import { Button, Form, Modal } from 'react-bootstrap';

const ConfirmDelete: React.FC<IConfirmDeleteProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        recordName,
        recordId,
        show,
        handleClose,
        handleConfirm
    } = props;

    const CODE_MIN = 10000000;
    const CODE_MAX = 99999999;

    const [code, setCode] = useState<string>('');
    const [confirmDisabled, setConfirmDisabled] = useState<boolean>(true);
    //#endregion

    //#region LIFECYCLE
    useEffect(() => {
        if(show) {
            generateCode();
        }
    }, [show]);// eslint-disable-line react-hooks/exhaustive-deps
    //#endregion

    //#region METHODS
    const generateCode = () => {
        const newCode = Math.floor(Math.random() * (CODE_MAX - CODE_MIN) + CODE_MIN);
        setCode(newCode.toString(16).toUpperCase());
    }

    const onChangeCode = (event: ChangeEvent<HTMLInputElement>) => {

        if(event.target.value !== '' && event.target.value && event.target.value.toUpperCase() === code) {
            return setConfirmDisabled(false);
        }

        setConfirmDisabled(true);
        
    }
    //#endregion

    //#region RENDER METHODS
    const renderMessage = () => {
        return <div className={classes.message}>
            Para deletar o registro <b>{recordName}</b>, digite o código<b>: {code}</b>
        </div>
    }
    //#endregion

    return <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Confirmar deleção</Modal.Title>
        </Modal.Header>
        <Modal.Body className={classes.body}> 
            {renderMessage()}
            <Form.Group controlId="formBasicEmail">
                <Form.Control type="text" autoComplete={'off'} onChange={onChangeCode}/>
            </Form.Group>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="primary" onClick={() => handleConfirm(recordId)} disabled={confirmDisabled}>
                Confirmar
            </Button>
            <Button variant="secondary" onClick={handleClose}>
                Fechar
            </Button>
        </Modal.Footer>
    </Modal>
}

export default withStyles(getStyles)(ConfirmDelete);