import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Loading.styles';
import { ILoadingProps } from './Loading.types';
import * as animationData from './Loading.lottie';
import Lottie from 'react-lottie';

const Loading: React.FC<ILoadingProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        height,
        width,
        hiddenLabel,
        label,
        labelSize
    } = props;

    const defaultOptions = {
        loop: true,
        autoplay: true, 
        animationData: animationData.default
    };
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    const renderLabel = () => {

        if(hiddenLabel) return;

        return <div className={classes.label} style={{fontSize: labelSize || '24px'}}>
            {label ? labelSize : 'Carregando...'}
        </div>
    }

    //#endregion

    return <div className={classes.root}>
        <Lottie options={defaultOptions}
            height={height || 100}
            width={width || 100}/>
        {renderLabel()}
    </div>
}

export default withStyles(getStyles)(Loading);