export const getStyles = {
    root: { 
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    label: {
        fontSize: '24px',
        marginTop: '8px',
    }
};