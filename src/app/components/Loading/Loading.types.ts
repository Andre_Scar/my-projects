import { WithStylesProps } from "react-jss";
import { getStyles } from "./Loading.styles";

export interface ILoadingProps extends WithStylesProps<typeof getStyles> { 
    height?: number;
    width?: number;
    hiddenLabel?: boolean;
    label?: string;
    labelSize?: string;
}