import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './ImageViwer.styles';
import { CaptionPosition, IImageViwerProps, ImageViwerType } from './ImageViwer.types';

const ImageViwer: React.FC<IImageViwerProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        src,
        type,
        caption,
        captionPosition,
        height,
        width,
        alt,
        backgroundColor
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return (
        <div className={`${classes.root} ${classes[captionPosition || CaptionPosition.BOTTOM]} ${className}`} 
            style={{height: height, width: width || '100%'}}>

            <div className={`${classes.image} ${classes[type || ImageViwerType.SQUARE]}`} style={{backgroundColor: backgroundColor || ''}}>
                <img src={src} alt={alt} height={height}/>
            </div>

            {caption && <div className={classes.caption}></div>}
        </div>
    );
}

export default withStyles(getStyles)(ImageViwer);