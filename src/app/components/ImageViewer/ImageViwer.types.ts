import { WithStylesProps } from "react-jss";
import { getStyles } from "./ImageViwer.styles";

export interface IImageViwerProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    src: string;
    type?: ImageViwerType;
    caption?: string;
    captionPosition?: CaptionPosition;
    height?: string;
    width?: string;
    alt?: string;
    backgroundColor?: string;
}

export enum ImageViwerType {
    ROUND_BORER = 'roundBorder',
    SQUARE = 'square',
    CIRCLE = 'circle'
}

export enum CaptionPosition {
    BOTTOM = 'bottomCaption',
    SIDE = 'sideCaption'
}