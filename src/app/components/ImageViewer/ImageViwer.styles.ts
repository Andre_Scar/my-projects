export const getStyles = {
    root: { 
        display: 'flex',
        justifyContent: 'center',
    },

    caption: {
        
    },

    bottomCaption: {
        flexDirection: 'column',
    },

    sideCaption: {
        flexDirection: 'row',
    },

    image: {
        overflow: 'hidden',
        display: 'flex',
        justifyContent: 'center',
    },

    roundBorder: {
        width: '100%',
        borderRadius: '5px',
    },

    square: {
        width: '100%',
    },

    circle: {
        width: '100%',
        borderRadius: '50%',
    }
};