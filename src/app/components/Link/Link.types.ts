import { WithStylesProps } from "react-jss";
import { getStyles } from "./Link.styles";

export interface ILinkProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    onClick?: () => void;
}