import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Link.styles';
import { ILinkProps } from './Link.types';

const Link: React.FC<ILinkProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        className,
        onClick,
        children
    } = props;
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div onClick={onClick} className={`${classes.root} ${className}`}>
        {children}
    </div>
}

export default withStyles(getStyles)(Link);