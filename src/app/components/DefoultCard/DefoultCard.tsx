import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './DefoultCard.styles';
import { IDefoultCardProps } from './DefoultCard.types';

const DefoultCard: React.FC<IDefoultCardProps> = (props) => {

    //#region CONSTS
    const {
        className,
        classes,
        children
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={`${classes.root} ${className}`}>
        {children}
    </div>
}

export default withStyles(getStyles)(DefoultCard);