import { WithStylesProps } from "react-jss";
import { getStyles } from "./DefoultCard.styles";

export interface IDefoultCardProps extends WithStylesProps<typeof getStyles> {
    className?: string;
}