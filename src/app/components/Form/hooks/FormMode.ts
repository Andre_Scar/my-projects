import { FormModes } from "../components/Form/Form.types";


const getFormMode = () => {
    
    let url = document.URL;
    
    if(url.includes('edit')) {
        return FormModes.EDIT;
    }

    if(url.includes('view')) {
        return FormModes.VIEW;
    }

    return FormModes.NEW;
}

export const useFormMode = () => {

    const mode = getFormMode();

    return {
        mode,
        isEdit: mode === FormModes.EDIT,
        isView: mode === FormModes.VIEW,
        isNew: mode === FormModes.NEW,
    };
}