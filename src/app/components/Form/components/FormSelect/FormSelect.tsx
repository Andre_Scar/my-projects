import React, { ChangeEvent } from 'react';
import { Form } from 'react-bootstrap-formik';
import withStyles from 'react-jss';
import { getStyles } from './FormSelect.styles';
import { IFormSelectProps } from './FormSelect.types';


const FormSelect: React.FC<IFormSelectProps> = (props) => {

    //#region CONSTS
    const {
        className,
        classes,
        name,
        label,
        children,
        onChange,
        disabled
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    const onChangeSelect = (event:ChangeEvent<HTMLSelectElement>) => {
        if(!onChange)
        {
            return;
        }

        onChange(event);
    }
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <Form.Select className={`${classes.root} ${className}`} name={name} label={label} onChange={onChangeSelect} disabled={disabled}>
        {children}
    </Form.Select>
}

export default withStyles(getStyles)(FormSelect);