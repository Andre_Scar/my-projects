import { ChangeEvent } from "react";
import { WithStylesProps } from "react-jss";
import { getStyles } from "./FormSelect.styles";

export interface IFormSelectProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    name: string;
    label?: string;
    onChange?: (event: ChangeEvent<HTMLSelectElement>) => void;
    disabled?: boolean;
}
