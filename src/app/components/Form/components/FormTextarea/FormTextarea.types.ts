import { WithStylesProps } from "react-jss";
import { getStyles } from "./FormTextarea.styles";

export interface IFormTextareaProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    name: string;
    label?: string;
    rows?: number;
}
