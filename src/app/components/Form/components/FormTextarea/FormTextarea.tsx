import React from 'react';
import { Form } from 'react-bootstrap-formik';
import withStyles from 'react-jss';
import { getStyles } from './FormTextarea.styles';
import { IFormTextareaProps } from './FormTextarea.types';


const FormTextarea: React.FC<IFormTextareaProps> = (props) => {

    //#region CONSTS
    const {
        className,
        classes,
        name,
        label,
        rows
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <Form.Textarea className={`${classes.root} ${className}`} name={name} label={label} rows={rows}/>
}

export default withStyles(getStyles)(FormTextarea);