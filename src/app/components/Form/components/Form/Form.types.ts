import { FormikHelpers } from "formik/dist/types";
import { WithStylesProps } from "react-jss";
import { getStyles } from "./Form.styles";

export interface IFormProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    id: string;
    mode: FormModes;
    initialValues: any;
    validationSchema?: any;
    onSubmit: (values:any, actions: FormikHelpers<any>) => void;
}

export enum FormModes {
    EDIT = 'edit',
    VIEW = 'view',
    NEW = 'new'
}