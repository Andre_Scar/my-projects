import { Formik } from 'formik';
import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './Form.styles';
import { FormModes, IFormProps } from './Form.types';
import { Form as BForm  } from 'react-bootstrap';


const Form: React.FC<IFormProps> = (props) => {

    //#region CONSTS
    const {
        className,
        classes,
        id,
        mode,
        initialValues,
        validationSchema,
        onSubmit,
        children
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={`${classes.root} ${className}`}>
        <Formik
            initialValues={initialValues}
            enableReinitialize
            onSubmit={onSubmit}
            validationSchema={validationSchema}
            render={(formik) => (
                <BForm 
                    id={id} 
                    autoComplete="off"
                    className={`${classes.form} ${mode === FormModes.VIEW ? classes.disabled : ''}`} 
                    noValidate={true} 
                    onSubmit={(e) => {
                        e.preventDefault();
                        formik.handleSubmit();
                    }}>
                    {children}
                </BForm>
            )}
        />
    </div>
}

export default withStyles(getStyles)(Form);