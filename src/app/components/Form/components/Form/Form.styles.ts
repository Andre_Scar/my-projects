export const getStyles = {
    root: { 
    },

    disabled: {

        '& input': {
            pointerEvents: 'none',
        },

        '& textarea': {
            pointerEvents: 'none',
        },

        '& select': {
            pointerEvents: 'none',
        }
    },

    form: {

        '& > div': {
            marginBottom: 0,
        },

        '& .invalid-feedback': {
            display: 'block',
            height: '20px'
        }
    }
};