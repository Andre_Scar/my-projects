import React from 'react';
import { Form } from 'react-bootstrap-formik';
import withStyles from 'react-jss';
import { getStyles } from './FormField.styles';
import { IFormFieldProps } from './FormField.types';


const FormField: React.FC<IFormFieldProps> = (props) => {

    //#region CONSTS
    const {
        className,
        classes,
        name,
        label,
        type,
        min,
        max,
        disabled
    } = props;

    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <Form.Input 
        className={`${classes.root} ${className}`} 
        name={name} 
        label={label} 
        type={type} 
        min={min} 
        max={max}
        disabled={disabled}/>
}

export default withStyles(getStyles)(FormField);