import { WithStylesProps } from "react-jss";
import { getStyles } from "./FormField.styles";

export interface IFormFieldProps extends WithStylesProps<typeof getStyles> {
    className?: string;
    name: string;
    label?: string;
    type?: FormFieldTypes;
    min?: string;
    max?: string;
    disabled?: boolean;
}

export enum FormFieldTypes {
    TEXT = 'text',
    EMAIL = 'email',
    PASSWORD = 'password',
    NUMBER = 'number',
    DATE = 'date',
}