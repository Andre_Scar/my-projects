import { WithStylesProps } from "react-jss";
import { getStyles } from "./LottieIcon.styles";

export interface ILottieIconProps extends WithStylesProps<typeof getStyles> { 
    height?: number;
    width?: number;
    type?: LottieIconType;
}

export enum LottieIconType {
    OK = 'ok',
    ATTENTION = 'attention'
}