import React from 'react';
import withStyles from 'react-jss';
import { getStyles } from './LottieIcon.styles';
import { ILottieIconProps, LottieIconType } from './LottieIcon.types';
import * as animationDataOk from './Ok.lottie';
import * as animationDataAttention from './Attention.lottie';
import Lottie from 'react-lottie';

const LottieIcon: React.FC<ILottieIconProps> = (props) => {

    //#region CONSTS
    const {
        classes,
        height,
        width,
        type
    } = props;


    const defaultOptions = {
        loop: false,
        autoplay: true, 
        animationData: type === LottieIconType.OK ? animationDataOk.default : animationDataAttention.default
    };
    //#endregion

    //#region LIFECYCLE
    //#endregion

    //#region METHODS
    //#endregion

    //#region RENDER METHODS
    //#endregion

    return <div className={classes.root}>
        <Lottie options={defaultOptions}
            height={height || 100}
            width={width || 100}/>
    </div>
}

export default withStyles(getStyles)(LottieIcon);