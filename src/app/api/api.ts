import axios from "axios";
import { AuthService } from "../services/auth/auth.service";
import { ProjectService } from "../services/currentProject/currentProject.service";



export const createAxiosInstanceApi = () => {

    const axiosInstance = axios.create({
        baseURL: process.env.REACT_APP_WS_URL,
        withCredentials: false
    });
    
    axiosInstance.interceptors.request.use(async (config: any) => {
        const token = AuthService.GetToken();

        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
    });


    axiosInstance.interceptors.response.use(response => {

        return response;

    }, error => {

        if (
            error === undefined || 
            error.response === undefined || 
            error.response.status === 401 || 
            error.response.status === 503
        ) {
            AuthService.ClearToken();
            ProjectService.ClearCurrentProject();

            window.location.href = `${window.location.origin}/login`;
            
            return;
        };

        throw Error(error.response.data.error);
    });

    return axiosInstance;
}

export const api = createAxiosInstanceApi();