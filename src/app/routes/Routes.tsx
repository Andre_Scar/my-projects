import React from 'react';

import LoginRoutes from '../modules/login/routes/Login.routes';
import ProjectsRoutes from '../modules/projects/routes/Projects.routes';
import DashboardRoutes from '../modules/dashboard/routes/Dashboard.routes';
import SprintsRoutes from '../modules/sprints/routes/Sprints.routes';
import BoardListRoutes from '../modules/board/routes/Board.routes';
import SettingsRoutes from '../modules/settings/routes/Settings.routes';
import MembersRoutes from '../modules/members/routes/Members.routes';
import ProfileRoutes from '../modules/profile/routes/Profile.routes';
import { Redirect, Switch } from 'react-router-dom';

const Routes: React.FC = () => {

    return (
        <>
            <LoginRoutes/>
            <ProjectsRoutes/>
            <DashboardRoutes/>
            <SprintsRoutes/>
            <BoardListRoutes/>
            <MembersRoutes/>
            <SettingsRoutes/>
            <ProfileRoutes/>
            <Switch>
                <Redirect exact from="/" to="/projects" />
            </Switch>
        </>
    )

}

export default Routes;